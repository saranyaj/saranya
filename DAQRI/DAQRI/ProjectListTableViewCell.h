//
//  ProjectListHeaderView.h
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/21/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectListTableViewCell: UITableViewCell
@property (nonatomic,strong) IBOutlet UILabel *statusLabel;
@property (nonatomic,strong) IBOutlet UILabel *nameLabel;
@property (nonatomic,strong) IBOutlet UILabel *noOfStepsLabel;
@property (nonatomic,strong) IBOutlet UILabel *noOfTaskLabel;
@property (nonatomic,strong) IBOutlet UILabel *documentsLabel;
@property (strong, nonatomic) IBOutlet UIView *cellSplitView;
@property (strong, nonatomic) IBOutlet UILabel *stepsTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *taskTitleLabel;
@property (strong, nonatomic) IBOutlet UIButton *projectDetailPageButton;

@end
