//
//  ProjectListHeaderView.m
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/21/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import "ProjectListTableViewCell.h"

@implementation ProjectListTableViewCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
//
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        _statusLabel = [[UILabel alloc]initWithFrame:CGRectMake(24, 11, 50, 21)];
        _statusLabel.text = @"Status";
        [self labelProperties:_statusLabel];
        
        _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(105, 11, 46, 21)];
        _nameLabel.text = @"Name";
        [self labelProperties:_nameLabel];
        
        _noOfStepsLabel = [[UILabel alloc]initWithFrame:CGRectMake(249, 11, 166, 21)];
        _noOfStepsLabel.text = @"Steps Complete/Total";
        [self labelProperties:_noOfStepsLabel];
        
        _noOfTaskLabel = [[UILabel alloc]initWithFrame:CGRectMake(482, 11, 156, 21)];
        _noOfTaskLabel.text = @"Tasks Complete/Total";
        [self labelProperties:_noOfTaskLabel];
        
        _documentsLabel = [[UILabel alloc]initWithFrame:CGRectMake(681, 11, 88, 21)];
        _documentsLabel.text = @"Documents";
        [self labelProperties:_documentsLabel];
    }
    return self;
}


-(void)labelProperties:(UILabel *)label{
    label.textColor = [UIColor blackColor];
    [self addSubview:label];

}
@end
