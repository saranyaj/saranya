//
//  MasterTableViewController.m
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/19/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import "MasterTableViewController.h"
#import "DetailViewController.h"
#import "DAQRITableViewCell.h"

#define cellIdentifier @"StepsTaskCell"

@interface MasterTableViewController ()
@end

@implementation MasterTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!expandedSections)
    {
        expandedSections = [[NSMutableIndexSet alloc] init];
    }
    [self prepolpulateAllDatasForProject];
    
    self.view.backgroundColor = DAQRI_TABLEVIEW_BACKROUND_COLOR;
    [self.tableView setSeparatorColor:DAQRI_TABLEVIEW_SEPARATOR_COLOR];
    
    UIButton *barBt =[[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 20)];
    [barBt setImage:[UIImage imageNamed:@"icn_arrow1"] forState:UIControlStateNormal];
    [barBt setTitle:@"     Projects" forState:UIControlStateNormal];
    barBt.titleLabel.font =[UIFont fontWithName:PROXIMANOVA_BOLD size:17];
    [barBt addTarget:self action: @selector(projectsButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barItem =  [[UIBarButtonItem alloc]init];
    [barItem setCustomView:barBt];
    self.navigationItem.leftBarButtonItem = barItem;
    
    //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Projects" style:UIBarButtonItemStylePlain target:self action:@selector(projectsButtonPressed:)];

    
    //self.tableViewDataSource = [NSArray arrayWithObjects:@"Expert",@"Upload data",@"Clear Stored Data",@"More Info", nil];
    //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cellIdentifier];
    
    // Do any additional setup after loading the view.
}

-(void)projectsButtonPressed:(id)sender{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate showOneViewInSplitViewController];
}

-(void)prepolpulateAllDatasForProject{
    _sharedManger = [DAQRIWebRequestManager shareManager];
    if([_sharedManger.projects count]){
    ProjectData* projData = [_sharedManger.projects objectAtIndex:_sharedManger.selectedIndexPath.row];
    self.tableViewDataSource = projData.steps;
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.tableViewDataSource.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([expandedSections containsIndex:indexPath.section])
    {
        return 44;// return rows when expanded
    }
    return 65;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        if ([expandedSections containsIndex:section])
        {
            
            StepData* stepData = [self.tableViewDataSource objectAtIndex:section];
            int taskCount = stepData.tasks.count;
            return taskCount + 1;// return rows when expanded
        }
        return 1; // only top row showing
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //static NSString *CellIdentifier = @"Cell";
    
    DAQRITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[DAQRITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    // Configure the cell...
    cell.backgroundColor = [UIColor clearColor];
    
    StepData* stepData = [self.tableViewDataSource objectAtIndex:indexPath.section];
    cell.nameLabel.text = stepData.stepName;
    cell.stepsLabel.text = [NSString stringWithFormat:@"%d/%d",stepData.tasks.count,stepData.tasks.count];
    cell.expandButton.hidden = YES;
    [cell.rightImageDataViewer addTarget:self action:@selector(dataViewerButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    cell.rightImageDataViewer.tag = indexPath.row;

        if (!indexPath.row)
        {
            // first row
            cell.nameLabel.text = stepData.stepName;
            cell.taskLabel.text = @"";
            cell.leftImage.hidden = YES;
            cell.rightImageDataViewer.hidden = YES;
            [self setFontsAttributesToCell:cell];


        }else if (indexPath.row <= stepData.tasks.count)
        {
            // all other rows
            cell.taskLabel.text = [[stepData.tasks objectAtIndex:indexPath.row - 1] taskName];//@"Some Detail";
            cell.stepsLabel.text= @"";
            cell.nameLabel.text= @"";
            cell.leftImage.hidden = NO;
            cell.rightImageDataViewer.hidden = NO;
            [self setFontsAttributesToNestedCell:cell];

        }

    return cell;
}


-(void)setFontsAttributesToCell:(DAQRITableViewCell *)cell{
    cell.nameLabel.font = [UIFont fontWithName:PROXIMANOVAS_REGULAR size:20];
    cell.stepsLabel.font = [UIFont fontWithName:PROXIMANOVA_BOLD size:10];
    
    cell.nameLabel.textColor = [UIColor whiteColor];
    cell.stepsLabel.textColor = [UIColor whiteColor];
}


-(void)setFontsAttributesToNestedCell:(DAQRITableViewCell *)cell{
    cell.taskLabel.font = [UIFont fontWithName:PROXIMANOVAS_REGULAR size:17];
    cell.taskLabel.textColor = [UIColor orangeColor];//[UIColor colorWithRed:189.0/255.0 green:29.0/255.0 blue:44.0/255.0 alpha:0.9];
    
    CGRect frame = cell.leftImage.frame;
    frame.size.width = cell.leftImage.image.size.width;
    frame.size.height = cell.leftImage.image.size.height;
    cell.leftImage.frame =frame;

}

-(void)dataViewerButtonClicked:(id)sender{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate addingSplitViewForDataViewer:self.navigationController];
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

        if (!indexPath.row)
        {
            [self.tableView beginUpdates];
            
            // only first row toggles exapand/collapse
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            //clear previously expanded cell
            [self clearExpandedCell:tableView didSelectRowAtIndexPath:indexPath];
            
            NSInteger section = indexPath.section;
            BOOL currentlyExpanded = [expandedSections containsIndex:section];
            NSInteger rows;
            
            NSMutableArray *tmpArray = [NSMutableArray array];
            
            if (currentlyExpanded){
                rows = [self tableView:tableView numberOfRowsInSection:section];
                [expandedSections removeIndex:section];
            }
            else{
                [expandedSections addIndex:section];
                rows = [self tableView:tableView numberOfRowsInSection:section];
            }
            
            for (int i=1; i<rows; i++){
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i inSection:section];
                [tmpArray addObject:tmpIndexPath];
            }
            
            if (currentlyExpanded) {
                [tableView deleteRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
            }
            else{
                [tableView insertRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
            }
            [self.tableView endUpdates];
            
            StepData* stepData = [self.tableViewDataSource objectAtIndex:indexPath.section];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadData" object:stepData];
        }else{
            
            StepData* stepData = [self.tableViewDataSource objectAtIndex:indexPath.section];
            TaskData*  taskData = [stepData.tasks objectAtIndex:indexPath.row - 1];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadData" object:taskData];
            [self dataViewerButtonClicked:nil];
            _sharedManger.selectedStep = [stepData.stepsId integerValue];
            _sharedManger.selectedTask  = [taskData.taskId integerValue];
            _sharedManger.stepData = stepData;

        }
    
    
    
}

-(void)clearExpandedCell:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    NSInteger rows;
    NSInteger section = [expandedSections lastIndex];

    if(expandedSections.count &&(indexPath.section !=section)){
        rows = [self tableView:tableView numberOfRowsInSection:section];
        [expandedSections removeIndex:section];
    
        NSMutableArray *tmpArray = [NSMutableArray array];
        for (int i=1; i<rows; i++)
        {
            NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i inSection:section];
            [tmpArray addObject:tmpIndexPath];
        }
        [tableView deleteRowsAtIndexPaths:tmpArray
                         withRowAnimation:UITableViewRowAnimationTop];
        
    }
}

@end
