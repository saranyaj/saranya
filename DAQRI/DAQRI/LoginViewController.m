//
//  ViewController.m
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/16/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import "LoginViewController.h"

#import "MasterTableViewController.h"
#import "DetailViewController.h"

@interface LoginViewController ()
@property(nonatomic,retain)MasterTableViewController *masterScreen;
@property(nonatomic,retain)DetailViewController *detailScreen;
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)loginButtonPressed:(id)sender{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate addingSplitViewInHomePage:self];
}

@end
