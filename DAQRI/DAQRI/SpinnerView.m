//
//  SpinnerView.m
//  rc
//
//  Created by Saranya Jayaseelan on 5/7/14.
//  Copyright (c) 2014 ReliableCoders. All rights reserved.
//

#import "SpinnerView.h"

@implementation SpinnerView
@synthesize spinner;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //self.backgroundColor = [UIColor grayColor];
        
        self.layer.backgroundColor = [[UIColor colorWithWhite:0.0f alpha:0.5f] CGColor];

        spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        spinner.layer.cornerRadius = 05;
        spinner.opaque = NO;
        spinner.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.6f];
        spinner.center = self.center;
        spinner.color =  [UIColor whiteColor];
        [self addSubview:spinner];
    }
    return self;
}

-(void)startAnimation{
    [spinner startAnimating];
}

-(void)stopAnimation{
     [spinner stopAnimating];
    [self removeFromSuperview];

}


@end
