//
//  AppDelegate.m
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/16/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import "AppDelegate.h"


#import "DaqriCore/WebGLViewController.h"
#import "ProjectDataTypes.h"
#import "DaqriCore/DaqriCore.h"
#import "DaqriCore/Log.h"
#import "DaqriCore/DebugConsoleLog.h"
#import "DaqriCore/ContentManagerIOS.h"
#import "DaqriCore/TrackerManagerIOS.h"

#include "VuforiaPlugin/TrackerPlugin_Vuforia.h"
#include "InstantOnPlugin/TrackerPlugin_InstantOn.h"
#import "DAQRIWebGLSubClassViewController.h"



@interface AppDelegate ()
{
    TrackerPlugin*	 instantOnPlugin;
    TrackerPlugin*	 vuforiaPlugin;
}
@property (nonatomic, retain) DAQRIWebGLSubClassViewController*	webGLViewControllerSubClass;
@end

@implementation AppDelegate
- (void)EnableVuforia
{
    [DAQRI_TrackerManager enablePlugin:vuforiaPlugin];
}

- (void)EnableInstantOn
{
    [DAQRI_TrackerManager enablePlugin:instantOnPlugin];
}

- (bool)IsVuforiaEnabled
{
    return ([DAQRI_TrackerManager getCurrentTracker] == vuforiaPlugin);
}

-(void)navigationBarSettings{
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:80/255.0f green:103/255.0f blue:144/255.0f alpha:0.9]];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:PROXIMANOVA_BOLD size:17], NSFontAttributeName, nil]];
    [[UIBarButtonItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont fontWithName:PROXIMANOVA_BOLD size:17], NSFontAttributeName, nil] forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTintColor:[UIColor whiteColor]];

}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //custom navigation bar appearance
    [self navigationBarSettings];

    
    //initialize Globals for the urls
    [Globals initialize];
    [[DAQRI_ContentManager sharedManager] setGlobalsWithBaseURL:[Globals baseURL] enterpriseJSONURL:[Globals enterpriseJSONURL] projectBaseURL:[Globals projectBaseURL] taskBaseURL:[Globals taskBaseURL]];
    instantOnPlugin = new TrackerPlugin_InstantOn();
    vuforiaPlugin = new TrackerPlugin_Vuforia();
    
    
    TrackerPluginConfig vuforiaConfig;
    vuforiaConfig.useLocalDataSet = true;
    vuforiaConfig.localDataSetPath = "StonesAndChips.xml";
    vuforiaConfig.useCloudRecognition = true;
    vuforiaConfig.cloudAccessKey = "7cfe81bd5b9217377eac095c36844104c2797283";
    vuforiaConfig.cloudSecretKey = "32aa14bdd72c70b33087525666dcc92e0905e677";
    vuforiaConfig.maxTrackableTargets = 1;
    
    vuforiaPlugin->Config = vuforiaConfig;
    
    [self EnableVuforia];
    //	[self EnableInstantOn];
    [DAQRI_TrackerManager enableTracking];
    
    
    [[DAQRI_DebugConsoleLog sharedManager] initialize];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    self.webGLViewControllerSubClass = [storyboard instantiateViewControllerWithIdentifier:@"DAQRIWebGLSubClassViewController"];

    return YES;
}


#pragma mark Split View Controller Methods


//Adding SplitView  in Window
-(void)addingSplitViewInTaskPage:(id)sender taskData:(TaskData *)ltaskData{
    UINavigationController *nav = sender;
    self.appDelegateTaskData = ltaskData;
    
   UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    if(!_splitView)
        _splitView = [[UISplitViewController alloc] init];
    
    _masterScreen = [storyboard instantiateViewControllerWithIdentifier:@"MasterTableViewControllerNav"];
    
    _splitView.maximumPrimaryColumnWidth = 50;
    self.webGLViewControllerSubClass.ltaskData = self.appDelegateTaskData;
    
    _splitView.viewControllers = @[_masterScreen,self.webGLViewControllerSubClass];
    [nav pushViewController:_splitView animated:YES];
}

//Adding SplitView  in Window
-(void)addingSplitViewInStepTaskPage:(id)sender{
    UINavigationController *nav = sender;
    
    if(!_splitView)
    _splitView = [[UISplitViewController alloc] init];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    _masterScreen = [storyboard instantiateViewControllerWithIdentifier:@"MasterTableViewControllerNav"];
    _detailScreen = [storyboard instantiateViewControllerWithIdentifier:@"DetailViewControllerNav"];
    
    _splitView.maximumPrimaryColumnWidth = 320;
    
    _splitView.viewControllers = @[_masterScreen,_detailScreen];
    [nav pushViewController:_splitView animated:YES];
}


-(void)addingSplitViewInHomePage:(id)sender{
    if(!_splitView)
        _splitView = [[UISplitViewController alloc] init];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    _settingsScreen = [storyboard instantiateViewControllerWithIdentifier:@"SettingsViewControllerNav"];
    _projectListScreen = [storyboard instantiateViewControllerWithIdentifier:@"ProjectListViewControllerNav"];
    
    _splitView.maximumPrimaryColumnWidth = 320;
    
    _splitView.viewControllers = @[_settingsScreen,_projectListScreen];
    [sender presentViewController:_splitView animated:YES completion:nil];
}


//Showing Master and Detail View controller
-(void)showTwoViewInSplitViewController{
    _splitView.viewControllers = @[_settingsScreen,_projectListScreen];
}

//Showing Only Detail View controller
-(void)showOneViewInSplitViewController{
     _projectListScreen.view.frame = self.window.frame;
     _splitView.viewControllers = @[_projectListScreen];
}

//Adding SplitView  in Window
-(void)addingSplitViewForDataViewer:(id)sender{
    UINavigationController *nav = sender;
    
    if(!_splitView)
        _splitView = [[UISplitViewController alloc] init];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    _dataViewerMasterScreen = [storyboard instantiateViewControllerWithIdentifier:@"DataViewerMasterTableViewController"];
    UINavigationController *masterTableNav = [[UINavigationController alloc] initWithRootViewController:_dataViewerMasterScreen];
    
    _dataViewerDetailScreen = [storyboard instantiateViewControllerWithIdentifier:@"DataViewerDetailViewController"];
    UINavigationController *detailViewNav = [[UINavigationController alloc] initWithRootViewController:_dataViewerDetailScreen];

    _splitView.maximumPrimaryColumnWidth = 320;
    
    _splitView.viewControllers = @[masterTableNav,detailViewNav];
    [nav pushViewController:_splitView animated:YES];
}




- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self.webGLViewControllerSubClass appWillPause];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self.webGLViewControllerSubClass appWillResume];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)setSceneJSON:(NSString*)scenePath
{
    [self.webGLViewControllerSubClass setSceneJSON:scenePath];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}


@end
