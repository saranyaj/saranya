//
//  AppDelegate.h
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/16/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "DaqriCore/WebGLViewController.h"

#import "MasterTableViewController.h"
#import "DetailViewController.h"

#import "ProjectListViewController.h"
#import "SettingsViewController.h"
#import "ProjectDataTypes.h"

#import "DataViewerDetailViewController.h"
#import "DataViewerMasterTableViewController.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate,UISplitViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;


//SplitView Instance
@property (nonatomic,retain) UISplitViewController *splitView;

//MasterTable Instance
@property (nonatomic,retain) MasterTableViewController *masterScreen;

//MasterTable Instance of setting screen
@property (nonatomic,retain) SettingsViewController *settingsScreen;

//DetailScreen Instance of HomeScreen
@property (nonatomic,retain) ProjectListViewController *projectListScreen;

//DetailScreen Instance of Step/TaskScreen
@property (nonatomic,retain) DetailViewController *detailScreen;

//MasterTable Instance of Data Viewer screen
@property (nonatomic,retain) DataViewerDetailViewController *dataViewerMasterScreen;

//DetailScreen Instance of Data Viewer Detail screen
@property (nonatomic,retain) DataViewerMasterTableViewController *dataViewerDetailScreen;

@property (nonatomic, strong) TaskData *appDelegateTaskData;



//Adding SplitView  in Window
-(void)addingSplitViewInStepTaskPage:(id)sender;
-(void)addingSplitViewInHomePage:(id)sender;
-(void)addingSplitViewInTaskPage:(id)sender taskData:(TaskData *)ltaskData;
-(void)addingSplitViewForDataViewer:(id)sender;


//Showing Master and Detail View controller
-(void)showTwoViewInSplitViewController;

//Showing Only Detail View controller
-(void)showOneViewInSplitViewController;

- (void)EnableVuforia;
- (void)EnableInstantOn;
- (bool)IsVuforiaEnabled;

- (void)setSceneJSON:(NSString*)scenePath;
@end

