//  DataViewerMasterTableViewController.m
//  DAQRI
//
//  Created by Saranya Jayaseelan on 2/3/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import "DataViewerMasterTableViewController.h"
#import "DAQRITableViewCell.h"

#define cellIdentifier @"DataViewerCell"


@interface DataViewerMasterTableViewController ()

@end

@implementation DataViewerMasterTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Data viewer";
    self.tableview.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = DAQRI_TABLEVIEW_BACKROUND_COLOR;
    [self.tableview setSeparatorColor:DAQRI_TABLEVIEW_SEPARATOR_COLOR];
    
    self.segmentedControl.backgroundColor = [UIColor clearColor];
    self.segmentedControl.tintColor = [UIColor orangeColor];
    [self.segmentedControl setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor orangeColor], NSForegroundColorAttributeName, [UIFont fontWithName:PROXIMANOVA_BOLD size:17], NSFontAttributeName, nil] forState:UIControlStateNormal];
    [[self.segmentedControl layer] setBorderColor:[UIColor orangeColor].CGColor];
    [self.segmentedControl addTarget:self action:@selector(segmentButtonClicked:) forControlEvents: UIControlEventValueChanged];    // Do any additional setup after loading the view.
    
    self.tableViewDataSource = [NSMutableArray array];
}

-(void)segmentButtonClicked:(id)sender{
    
}

-(void)viewWillAppear:(BOOL)animated{
    DAQRIWebRequestManager *webManger = [DAQRIWebRequestManager shareManager];
    [self.tableViewDataSource removeAllObjects];
    
    for(int i = 0 ; i< webManger.stepData.tasks.count;i++){
        TaskData *taskData =  [webManger.stepData.tasks objectAtIndex:i];
        NSArray *stepDocAssets   = taskData.taskDocAssets;
        [self.tableViewDataSource addObjectsFromArray:stepDocAssets];
    }
    
//    DataBaseManager *datamanger = [DataBaseManager shareManager];
//    self.tableViewDataSource = [datamanger readDocAssetsFromTaskTableUsingStepId:webManger.selectedStep];
    [self.tableview reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.tableViewDataSource.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DAQRITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    cell.nameLabel.textColor = [UIColor whiteColor];
    cell.nameLabel.font = [UIFont fontWithName:PROXIMANOVAS_REGULAR size:20];
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    if(indexPath.row == 0){
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    
    if (cell == nil) {
        cell = [[DAQRITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSDictionary *dict = [self.tableViewDataSource objectAtIndex:indexPath.row];
    cell.nameLabel.text = dict[@"name"];
    
    NSString *ext = [cell.nameLabel.text pathExtension];
    if ([ext isEqualToString:@"doc"]) {
        cell.leftImage.image = [UIImage imageNamed:@"icn_doc1"];
    } else if ([ext isEqualToString:@"jpg"]) {
        cell.leftImage.image = [UIImage imageNamed:@"icn_image"];
    }else if ([ext isEqualToString:@"png"]) {
        cell.leftImage.image = [UIImage imageNamed:@"icn_image"];
    } else if ([ext isEqualToString:@"mp4"]) {
        cell.leftImage.image = [UIImage imageNamed:@"icn_video"];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [self.tableViewDataSource objectAtIndex:indexPath.row];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
