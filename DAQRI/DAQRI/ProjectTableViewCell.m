//
//  ProjectTableViewCell.m
//  DaqriWebGL
//
//  Created by Apar Suri on 1/1/15.
//  Copyright (c) 2015 Daqri. All rights reserved.
//

#import "ProjectTableViewCell.h"

@implementation ProjectTableViewCell

static float _expandedCellHeight;
static float _collapsedCellHeight;

+ (void) initialize
{
    _expandedCellHeight = 150.0f;
    _collapsedCellHeight = 44.0f;
}

- (id) initWithStyle:(UITableViewCellStyle) UITableViewCellStyleSubtitle reuseIdentifier:(NSString*)cellID
{
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellID];
    if ( self != nil )
    {
        buttonMoreInfoWidth = 30.0;
        buttonMoreInfoHeight = 30.0;
        topPadding = 20.0;
        leftPadding = 20.0;
        buttonMoreInfoLeftLocationFromRight = 50.0;
        labelDescriptionHeight = 50.0;
        labelDescriptionFontSize = 20.0;
        
        self.buttonMoreInfo = [UIButton buttonWithType: UIButtonTypeRoundedRect] ;
        self.imageExpand = [[UIImage imageNamed:@"More Info Icon"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.buttonMoreInfo.frame = CGRectMake(0, 0, buttonMoreInfoWidth, buttonMoreInfoHeight);
        [self.buttonMoreInfo setImage:self.imageExpand forState:UIControlStateNormal];
        self.imageCollapse = [[UIImage imageNamed:@"minus"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        [self.contentView addSubview:self.buttonMoreInfo];
        
        self.labelMoreInfo = [[UITextView alloc] initWithFrame:CGRectMake(leftPadding, self.frame.size.height + topPadding, self.frame.size.width - leftPadding, labelDescriptionHeight)];
        [self.labelMoreInfo setEditable:NO];
        [self.labelMoreInfo setScrollEnabled:YES];
        [self.labelMoreInfo setFont:[UIFont fontWithName:@"ProximaNova-Thin" size:labelDescriptionFontSize]];
    }
    return self;
}

- (void) layoutSubviews
{
    
    [super layoutSubviews];
    
    self.textLabel.frame = CGRectMake(leftPadding, topPadding, self.frame.size.width - leftPadding, self.frame.size.height - topPadding);
    [self.textLabel sizeToFit];
    self.buttonMoreInfo.center = CGPointMake(self.frame.size.width - buttonMoreInfoLeftLocationFromRight, topPadding);
    self.labelMoreInfo.frame = CGRectMake(self.labelMoreInfo.frame.origin.x, self.labelMoreInfo.frame.origin.y
                                          , self.frame.size.width, self.labelMoreInfo.frame.size.height);
    
}

+ (float) expandedCellHeight
{
    return _expandedCellHeight;
}

+ (float) collapsedCellHeight
{
    return _collapsedCellHeight;
}

@end
