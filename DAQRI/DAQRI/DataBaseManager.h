//
//  DataBaseManager.h
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/29/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "ProjectDataTypes.h"

@class DataBase;

@interface DataBaseManager : NSObject{
    sqlite3 *database;
    DataBase *db;
}
+ (DataBaseManager *) shareManager;

-(void)insertProjectDetails:(ProjectData *)projectData;
-(void)insertStepsDetails:(StepData *)stepData;
-(void)insertTaskDetails:(TaskData *)taskData;


-(NSMutableArray *)readFromProjectTable;
-(NSMutableArray *)readFromStepTable;
-(NSMutableArray *)readFromTaskTable;

-(NSMutableArray *)readDocAssetsFromTaskTableUsingStepId:(int)stepId;

@end
