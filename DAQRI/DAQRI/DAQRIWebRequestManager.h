//
//  DAQRIWebRequestManager.h
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/21/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "ProjectDataTypes.h"

@interface DAQRIWebRequestManager : NSObject

@property (nonatomic, retain) NSMutableDictionary* projectDownloads;
@property (nonatomic, retain) NSMutableArray* projects,*listOfProjects;
@property (nonatomic, retain) NSMutableArray* dataSource;
@property (nonatomic, retain) NSIndexPath* selectedIndexPath;
@property (nonatomic, assign) int selectedStep,selectedTask;
@property (nonatomic, retain) StepData *stepData;
@property (nonatomic, retain) TaskData *taskData;


+ (DAQRIWebRequestManager *) shareManager;
-(void)pulltheDataFrom4DLibraryForProjectList;

@end
