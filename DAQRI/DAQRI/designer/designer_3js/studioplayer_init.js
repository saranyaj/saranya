/**
 * init this sub part
 * 
 * @param  {THREE.Editor} editor - the three.js editor
 */
var studioplayer_init  = function(editor){
	var viewport	= editor.viewport

	var player	= new THREEx.StudioPlayer()
	editor.player	= player

	// set a default camera position
	player.camera.position.set(1,1,1).multiplyScalar(50)
	player.camera.lookAt(new THREE.Vector3(0,0,0))


	// plug player on the editor
	player.renderer		= viewport.renderer
	player.scene		= editor.scene
	player.renderingEnabled	= false
	player.controlsEnabled	= false

        //////////////////////////////////////////////////////////////////////////////////
        //	BehaviorContext
        //////////////////////////////////////////////////////////////////////////////////

	player.addBehaviorsComponent();

	player.init()

	editor.signals.requestAnimationFrame.add(function(){
		player.render()	
	})

	editor.toggleMode	= function(){
		var currentMode	= editor.getMode()
		if( currentMode === 'play' ){
			editor.setMode('edit')
		}else if( currentMode === 'edit' ){
			editor.setMode('play')			
		}
	}

	// TODO find a better API for that
	// TODO there is a need to refactor it with different scenes and different canvas
	editor.setMode	= function(value){
		if( value === 'play' )		switchToPlayMode()
		else if( value === 'edit' )	switchToEditMode()
		else				console.assert(false)
		return

		function switchToPlayMode(){
			// console.log('Switch editor to PLAY mode')
			undoRedo.enabled	= false

			viewport.renderingEnabled	= false
			viewport.controlsEnabled	= false

			// reset scissor - in case of multiple viewport
			var renderer	= viewport.renderer
			renderer.setViewport( 0, 0, renderer.domElement.width, renderer.domElement.height );
			renderer.enableScissorTest( false );

			player.renderingEnabled	= true
			player.controlsEnabled	= true

			player.start()
		}
		function switchToEditMode(){
			// console.log('Switch editor to EDIT mode')
			player.stop()

			player.renderingEnabled	= false
			player.controlsEnabled	= false

			viewport.renderingEnabled	= true
			viewport.controlsEnabled	= true

			viewport.renderer.domElement.focus()

			undoRedo.enabled	= true
		}
	}
	editor.getMode	= function(){
		return player.renderingEnabled === true ? 'play' : 'edit'
	}	
}