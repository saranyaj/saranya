//
//  DAQRIWebRequestManager.m
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/21/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import "DAQRIWebRequestManager.h"

#include "DaqriCore/ContentManagerIOS.h"
#include "DaqriCore/DownloadManager.h"

@implementation DAQRIWebRequestManager
+ (DAQRIWebRequestManager *) shareManager {
    
    static DAQRIWebRequestManager *shareManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareManager = [[self alloc] init];
    });
    return shareManager;
}

- (instancetype)init
{
    if (self = [super init]) {
        self.projects = [[NSMutableArray alloc] init];
        self.listOfProjects = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)pulltheDataFrom4DLibraryForProjectList{
    
    SEL enterpriseDownloadCompleted = @selector(enterpriseDownloadCompleted:);
    SEL enterpriseDownloadFailed = @selector(enterpriseDownloadFailed:);
    self.projects = [[NSMutableArray alloc] init];
    
    if ([Globals enterpriseJSONURL] == nil)
    {
        NSLog(@"Enterprise JSON URL is null, nothing will be loaded");
    }
    else
    {
        [[DAQRI_ContentManager sharedManager] getDataFromURL:[Globals enterpriseJSONURL]  withReceiver:self SuccessCallback:enterpriseDownloadCompleted AndFailureCallback:enterpriseDownloadFailed];
    }
}

- (void) enterpriseDownloadCompleted:(DAQRI_DownloadData*) data {
    NSLog(@"Enterprise download completed");
    
    //parse and populate
    _listOfProjects = [[DAQRI_ContentManager sharedManager] getProjectListFromJSON:data.fileData receiver:self success:nil failure:nil];
    SEL projectDownloadCompleted = @selector(downloadCompleted:);
    SEL projectDownloadFailed = @selector(downloadFailed:);
    DAQRI_ContentManager* contentManager = [DAQRI_ContentManager sharedManager];
    if (_listOfProjects != nil && _listOfProjects.count > 0) {
        for (int i = 0; i <_listOfProjects.count; ++i) {
            DAQRI_ProjectContent* projContent = [_listOfProjects objectAtIndex:i];
            if ([self.projectDownloads objectForKey:projContent.projectId] == nil) {
                [self.projectDownloads setObject:[NSNumber numberWithBool:false] forKey:projContent.projectId];
                // NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:projContent.projectJSONData options:kNilOptions error:nil];
                
            }else{
                
            }
        }
        
        if (contentManager.projectBaseURL == nil)
        {
            //There is no project url provided, return
            NSLog(@"Project base URL is empty, project data will not be downloaded");
            return;
        }
        
        for (int i = 0; i < _listOfProjects.count; ++i) {
            DAQRI_ProjectContent* projContent = [_listOfProjects objectAtIndex:i];
            NSString* projectLink = [NSString stringWithFormat:@"%@%@", [Globals projectBaseURL], projContent.projectId];
            //download the project json;
            [contentManager getDataFromURL:projectLink withReceiver:self SuccessCallback:projectDownloadCompleted  AndFailureCallback:projectDownloadFailed];
        }
    }
    else {
        NSLog(@"Enterprise project list is empty");
    }
    
}

- (void) enterpriseDownloadFailed:(DAQRI_DownloadData*) data {
}


- (void) downloadCompleted:(id) cacheObject {
    
    DAQRI_DownloadData* data = cacheObject;
    if (data != nil) {
        ProjectData* projData = [self parseProjectJSONWithData:data.fileData];
        
        if (self.projects == nil) {
            self.projects = [[NSMutableArray alloc] init];
        }
        [self.projects addObject:projData];
        
        if ([self.projectDownloads objectForKey:projData.projectId] != nil) {
            [self.projectDownloads setObject:[NSNumber numberWithBool:true] forKey:projData.projectId ];
        }
        
        if ([self checkIfAllProjectsDownloaded]) {
            
            if(_listOfProjects.count == _projects.count){
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadProjectTable" object:self.projects];

                    });
            }
            
            SEL projectsJsonDownloadSuccess = @selector(projectsJsonDownloadSuccess:);
            SEL projectsJsonDownloadFailed = @selector(projectsJsonDownloadFailed:);
            
            //After everything is downloaded and ready to go, start the enterprise download..
            [[DAQRI_ContentManager sharedManager] getDataFromURL:[Globals enterpriseJSONURL] withReceiver:self SuccessCallback:projectsJsonDownloadSuccess AndFailureCallback:projectsJsonDownloadFailed];
        }
    }
    else {
        NSLog(@"Download completed successfully but object is empty");
    }
}

- (void) downloadFailed:(id) obj {
}

- (BOOL) checkIfAllProjectsDownloaded {
    BOOL areAllProjectDownloadsComplete = true;
    for(id key in self.projectDownloads) {
        NSNumber* value = [self.projectDownloads objectForKey:key];
        if ([value boolValue] == false) {
            areAllProjectDownloadsComplete = false;
            break;
        }
    }
    
    return areAllProjectDownloadsComplete;
}


- (ProjectData*) parseProjectJSONWithData:(NSData*) jsonData {
    ProjectData* projData = [[ProjectData alloc] init];
    DataBaseManager *dataManger = [DataBaseManager shareManager];

    
    NSError *error = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:&error];
    NSDictionary* proj = [jsonDict objectForKey:@"project"];
    projData.projectName = [proj objectForKey:@"name"];
    projData.projectId = [proj objectForKey:@"id"];
    projData.projDescription = [proj objectForKey:@"description"];
    projData.projDocAssets   = [proj objectForKey:@"docs_assets"];

    projData.steps = [[NSMutableArray alloc] init];
    
    //steps
    NSArray* stepArray = [proj objectForKey:@"steps"];
    projData.no_of_Steps      =  stepArray.count;

    for (int i = 0; i < stepArray.count; ++i) {
        StepData* stepData = [[StepData alloc] init];
        stepData.stepName = [[stepArray objectAtIndex:i] objectForKey:@"name"];
        stepData.stepDescription = [[stepArray objectAtIndex:i] objectForKey:@"description"];
        stepData.stepDocAssets   = [[stepArray objectAtIndex:i] objectForKey:@"docs_assets"];
        stepData.steppointOfOriginName = [[stepArray objectAtIndex:i] objectForKey:@"target_asset_name"];
        stepData.steppointOfOriginNameURL = [[stepArray objectAtIndex:i] objectForKey:@"target_asset_thumb_url"];
       
        stepData.projectId = projData.projectId;
        stepData.stepsId   = [stepArray objectAtIndex:i][@"id"];

        
        //tasks
        NSArray* taskArray = [[stepArray objectAtIndex:i] objectForKey:@"tasks"];
        stepData.no_of_tasks      =  taskArray.count;

        stepData.tasks = [[NSMutableArray alloc] init];
        for (int j = 0; j < taskArray.count; ++j) {
            TaskData* taskData = [[TaskData alloc] init];
            taskData.taskName = [[taskArray objectAtIndex:j] objectForKey:@"name"];
            taskData.taskId = [[taskArray objectAtIndex:j] objectForKey:@"id"];
            taskData.taskDocAssets = [[taskArray objectAtIndex:j] objectForKey:@"threejsAssets"];
            taskData.taskDescription = [[taskArray objectAtIndex:j] objectForKey:@"description"];
            taskData.usesInstantOn = [[[stepArray objectAtIndex:i] objectForKey:@"instant_on"] boolValue];
            taskData.input_type =    [[taskArray objectAtIndex:j] objectForKey:@"input_type"];
            taskData.input_label =    [[taskArray objectAtIndex:j] objectForKey:@"input_label"];
            taskData.input_options =    [[taskArray objectAtIndex:j] objectForKey:@"input_options"];
            
            taskData.projectId = projData.projectId;
            taskData.stepsId   = stepData.stepsId;

            [stepData.tasks addObject:taskData];
            
            //adding to task table
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^{
                [dataManger insertTaskDetails:taskData];
            });

        }
        
        [projData.steps addObject:stepData];
        
        //adding to steps table
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^{
            [dataManger insertStepsDetails:stepData];
        });

    }
    
    //adding to project table
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^{
        [dataManger insertProjectDetails:projData];
    });
    
    return projData;
}

- (ProjectData*) parseProjectJSON:(NSString*) jsonPath {
    
    NSData* data = [[NSFileManager defaultManager] contentsAtPath:jsonPath];
    return [self parseProjectJSONWithData:data];
    
}

- (void) projectsJsonDownloadSuccess:(DAQRI_DownloadData*) projData {
    //NSLog(@"Enterprise json downloaded: %s", [projData.urlString UTF8String]);
    SEL enterpriseDownloadSuccess = @selector(enterpriseDownloadSuccess:);
    SEL enterpriseDownloadFailure = @selector(enterpriseDownloadFailure:);
    
    [[DAQRI_ContentManager sharedManager] getContentForProjects:projData.fileData Receiver:self SuccessCallback:enterpriseDownloadSuccess AndFailureCallback:enterpriseDownloadFailure];
}


- (void) projectsJsonDownloadFailed:(DAQRI_DownloadData*) projData {
    NSLog(@"Enterprise download failed: %s", [projData.urlString UTF8String]);
}

- (void) enterpriseDownloadSuccess:(DAQRI_ProjectContent*) projContent {
    NSLog(@"Enterprise data downloaded: %@", [projContent.enterpriseId stringValue]);
    
}

- (void) enterpriseDownloadFailure:(DAQRI_ProjectContent*) projContent {
    NSLog(@"Enterprise data Failure: %@", [projContent.enterpriseId stringValue]);
    
}

@end
