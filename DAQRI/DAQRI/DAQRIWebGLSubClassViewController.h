//
//  DAQRIWebGLSubClassViewController.h
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/30/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import "DaqriCore/WebGLViewController.h"


@interface DAQRIWebGLSubClassViewController : DAQRI_WebGLViewController

@property(nonatomic,retain) UIButton *previousTaskButton;
@property(nonatomic,retain) UIButton *nextTaskButton;
@property(nonatomic,retain) UIButton *cancelTaskButton;
@property(nonatomic,retain) UIWebView *childWebView;
@property(nonatomic,retain) UIView    *inputView;
@property(nonatomic, strong) TaskData  *ltaskData;
@property (weak, nonatomic) IBOutlet UIView *measurementView;
@property (nonatomic) UIImagePickerController *imagePickerController;


@end
