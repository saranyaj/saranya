//
//  DAQRITableViewCell.m
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/22/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import "DAQRITableViewCell.h"

@implementation DAQRITableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
