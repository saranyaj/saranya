//
//  DAQRITableViewCell.h
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/22/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAQRITableViewCell : UITableViewCell
@property (nonatomic,strong) IBOutlet UIImageView *leftImage;
@property (nonatomic,strong) IBOutlet UILabel *nameLabel;
@property (nonatomic,strong) IBOutlet UILabel *stepsLabel;
@property (nonatomic,strong) IBOutlet UILabel *taskLabel;
@property (nonatomic,strong) IBOutlet UIButton *expandButton;
@property (nonatomic,strong) IBOutlet UIButton *rightImageDataViewer;
@end
