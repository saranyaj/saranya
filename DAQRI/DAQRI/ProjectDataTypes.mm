//
//  ProjectDataTypes.m
//  DaqriWebGL
//
//  Created by Apar Suri on 12/9/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#import "ProjectDataTypes.h"
#import "DaqriCore/ContentManagerIOS.h"

@implementation ProjectData

@end

@implementation StepData

@end

@implementation TaskData

@end

@implementation Globals
static NSString* _enterpriseJSONURL;
static NSString* _enterpriseBaseURL;
static NSString* _projectBaseURL;
static NSString* _taskURL;
static NSString* _baseURL;
static bool _isUsingi4ds;

static NSString* i4dsBaseURL;
static NSString* i4dsEnterpriseURL;
static NSString* stagingBaseURL;
static NSString* stagingEnterpriseURL;

+ (void) initialize
{
    if (self == [Globals class]) {
        i4dsBaseURL = @"https://i4dsdemo.daqri.com/mobile/v1/i4ds/";
        i4dsEnterpriseURL = @"14?sort=created_at";
        stagingBaseURL = @"http://staging1.daqri.com/mobile/v1/i4ds/";
        stagingEnterpriseURL = @"11?sort=created_at";
        [Globals setAllURLs:i4dsBaseURL andEnterpriseURL:i4dsEnterpriseURL];
        _isUsingi4ds = true;
        [ProjectTableViewCell initialize];
    }
}

+ (void) setAllURLs:(NSString*) baseURL andEnterpriseURL:(NSString*) enterpriseURL
{
   //baseURL = @"http://staging1.daqri.com/mobile/v1/i4ds/";
    //@"https://i4dsdemo.daqri.com/mobile/v1/i4ds/";

    _baseURL = baseURL;
    
    // using enterprise/ after baseURL
    _enterpriseBaseURL = [NSString stringWithFormat:@"%@%@", _baseURL, @"enterprise/"];
    
    //https://i4dsdemo.daqri.com/mobile/v1/i4ds/enterprise/14?sort=created_at
    _enterpriseJSONURL = [NSString stringWithFormat:@"%@%@", _enterpriseBaseURL, enterpriseURL];//@"14?sort=created_at"];
    
    // using project/ after _baseURL
    //http://staging1.daqri.com/mobile/v1/i4ds/project/27
    _projectBaseURL = [NSString stringWithFormat:@"%@%@", _baseURL, @"project/"];
    
    //using task/ after _baseURL
    //https://i4dsdemo.daqri.com/mobile/v1/i4ds/task/14
    _taskURL = [NSString stringWithFormat:@"%@%@", _baseURL, @"task/"];
}

+ (NSString*) baseURL
{
    return _baseURL;
}

+ (NSString*) projectBaseURL
{
    return _projectBaseURL;
}

+ (NSString*) taskBaseURL
{
    return _taskURL;
}

+ (NSString*) enterpriseJSONURL
{
    return _enterpriseJSONURL;
}

+ (NSString*) enterpriseBaseURL
{
    return  _enterpriseBaseURL;
}

+ (BOOL) isUsingi4ds
{
    return _isUsingi4ds;
}

+ (void) setIsUsingi4ds:(BOOL) isUsingi4ds
{
    _isUsingi4ds = isUsingi4ds;
    
    if (_isUsingi4ds)
    {
        [Globals setAllURLs:i4dsBaseURL andEnterpriseURL:i4dsEnterpriseURL];
    }
    else
    {
        [Globals setAllURLs:stagingBaseURL andEnterpriseURL:stagingEnterpriseURL];
    }
    [[DAQRI_ContentManager sharedManager] setGlobalsWithBaseURL:[Globals baseURL] enterpriseJSONURL:[Globals enterpriseJSONURL] projectBaseURL:[Globals projectBaseURL] taskBaseURL:[Globals taskBaseURL]];
}

@end