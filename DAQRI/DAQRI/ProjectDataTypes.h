//
//  ProjectDataTypes.h
//  DaqriWebGL
//
//  Created by Apar Suri on 12/9/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProjectTableViewCell.h"

@interface ProjectData : NSObject {
}
@property (nonatomic, retain) NSString* projectName;
@property (nonatomic, retain) NSString* projectURL;
@property (nonatomic, retain) NSNumber* projectId;
@property (nonatomic, retain) NSString* projDescription;
@property (nonatomic, retain) NSMutableArray* steps;
@property (nonatomic, retain) NSMutableArray*projDocAssets;
@property (nonatomic, assign) int no_of_Steps;

@end

@interface StepData : NSObject {

}
@property (nonatomic, retain) NSString* stepName;
@property (nonatomic, retain) NSString* stepDescription;
@property (nonatomic, retain) NSMutableArray* tasks;
@property (nonatomic, retain) NSMutableArray* stepDocAssets;
@property (nonatomic, retain) NSString *steppointOfOriginName;
@property (nonatomic, retain) NSString *steppointOfOriginNameURL;
@property (nonatomic, retain) NSNumber* projectId;
@property (nonatomic, retain) NSNumber* stepsId;
@property (nonatomic, assign) int no_of_tasks;



@end


@interface TaskData : NSObject {

}
@property (nonatomic, retain) NSString* taskName;
@property (nonatomic, retain) NSNumber* taskId;
@property (nonatomic, retain) NSString* taskDescription;
@property (nonatomic) BOOL usesInstantOn;
@property (nonatomic, retain) NSMutableArray* taskDocAssets;
@property (nonatomic, retain) NSString* input_type;
@property (nonatomic, retain) NSString* input_label;
@property (nonatomic, retain) NSDictionary* input_options;
@property (nonatomic, retain) NSNumber* projectId;
@property (nonatomic, retain) NSNumber* stepsId;

@end

@interface Globals : NSObject  {
    
}
+ (NSString*) baseURL;
+ (NSString*) projectBaseURL;
+ (NSString*) taskBaseURL;
+ (NSString*) enterpriseJSONURL;
+ (NSString*) enterpriseBaseURL;
+ (void) initialize;

+ (BOOL) isUsingi4ds;
+ (void) setIsUsingi4ds:(BOOL) isUsingi4ds;

@end