//
//  ProjectTableViewCell.h
//  DaqriWebGL
//
//  Created by Apar Suri on 1/1/15.
//  Copyright (c) 2015 Daqri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectTableViewCell : UITableViewCell
{
    float buttonMoreInfoWidth;
    float buttonMoreInfoHeight;
    float topPadding;
    float leftPadding;
    float buttonMoreInfoLeftLocationFromRight;
    float labelDescriptionHeight;
    float labelDescriptionFontSize;
}
@property (nonatomic, retain) UIButton* buttonMoreInfo;
@property (nonatomic, retain) UITextView* labelMoreInfo;
@property (nonatomic, retain) UIImage* imageExpand;
@property (nonatomic, retain) UIImage* imageCollapse;

+ (float) expandedCellHeight;
+ (float) collapsedCellHeight;
+ (void) initialize;
@end
