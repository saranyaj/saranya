//
//  DataViewerDetailViewController.m
//  DAQRI
//
//  Created by Saranya Jayaseelan on 2/3/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import "DataViewerDetailViewController.h"

@interface DataViewerDetailViewController ()

@end

@implementation DataViewerDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:47.0/255.0 green:51.0/255.0 blue:56.0/255.0 alpha:0.9];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:PROJECT_SUMMARY style:UIBarButtonItemStylePlain target:self action:@selector(ProjectSummaryPressed:)];

    // Do any additional setup after loading the view.
}

-(void)ProjectSummaryPressed:(id)sender{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate addingSplitViewInStepTaskPage:self.navigationController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
