//
//  DARIWebViewController.h
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/29/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import <DaqriCore/DaqriCore.h>

@interface DARIWebViewController : DAQRI_WebGLViewController

@end
