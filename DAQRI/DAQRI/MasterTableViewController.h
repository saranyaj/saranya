//
//  MasterTableViewController.h
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/19/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DAQRIWebRequestManager;

@interface MasterTableViewController : UITableViewController<UISplitViewControllerDelegate>{
    NSMutableIndexSet *expandedSections;

}
@property (nonatomic,retain)  NSArray *tableViewDataSource;
@property (nonatomic,retain)  UISplitViewController *splitView;
@property (nonatomic,strong)  DAQRIWebRequestManager *sharedManger;
@property (nonatomic, assign) int selectedExpandablerow;
@property (nonatomic, assign) BOOL isCellExpanded;
@property (nonatomic, strong) UIView *expandableView;



@end
