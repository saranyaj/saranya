//1,0,0,0, 
//0,1,0,0, 
//0,0,1,1000, 
//0,0,0,1
var params = {
			    p1: 1,
				p2: 0,
				p3: 0,
				p4: 0,
				
				p5: 0,
				p6: 1,
				p7: 0,
				p8: 0,
				
			    p9: 0,
				p10: 0,
				p11: 1,
				p12: 100,
				
				p13: 0,
				p14: 0,
				p15: 0,
				p16: 1
			};

var debugLabel2 = document.createElement('div');
SetDebugLabel2();

function SetDebugLabel2()
{
	debugLabel2.style.position = 'absolute';
	//debugLabel.style.zIndex = 1;    // if you still don't see the label, try uncommenting this
	debugLabel2.style.width = 100;
	debugLabel2.style.height = 100;
	debugLabel2.style.backgroundColor = "blue";
	debugLabel2.innerHTML = "hi there! " + Math.PI;
	debugLabel2.style.top = 50 + 'px';
	debugLabel2.style.left = 10 + 'px';
	document.body.appendChild(debugLabel2);
}

var newPos = [ 0.073174,-0.981134,0.178945,-0.092728,  -0.994476,-0.058241,0.087329,-55.432877,  -0.075259,-0.184346,-0.979976,401.568298,  0.000000,0.000000,0.000000,1.000000 ];
var  newPos2 = [ 0.459531,0.654883,-0.599966,-3.435100,  0.753880,-0.644749,-0.126347,-5.323666,  -0.469570,-0.394242,-0.789986,-410.116638,  0.000000,0.000000,0.000000,1.000000 ];	

function SetupDatGUI() 
{
	debugLabel.innerHTML = "Insisde SetupDatGUI";
	var gui = new dat.GUI({
	    height : 5 * 32 - 1
	});
	
	var obj = { 
		add:function()
		{ 
			debugLabel2.innerHTML = "clicked"; 
			Scene4D.applyPoseToObject3d(newPos2, scope.threejs.editor.sceneRoot);
			//animate();
		}
	};
	gui.add(obj,'add');

	var setButton = { 
		set:function()
		{ 
			var newPosition = newPos;
			debugLabel2.innerHTML = "set"; 
			params.p1 = newPosition[0];
			params.p2 = newPosition[1];
			params.p3 = newPosition[2];
			params.p4 = newPosition[3];
			params.p5 = newPosition[4];
			params.p6 = newPosition[5];
			params.p7 = newPosition[6];
			params.p8 = newPosition[7];
			params.p9 = newPosition[8];
			params.p10 = newPosition[9];
			params.p11 = newPosition[10];
			params.p12 = newPosition[11];
			params.p13 = newPosition[12];
			params.p14 = newPosition[13];
			params.p15 = newPosition[14];
			params.p16 = newPosition[15];
			Scene4D.applyPoseToObject3d(newPosition, scope.threejs.editor.sceneRoot);
			//animate();
			
			for (var i in gui.__controllers) {
			    gui.__controllers[i].updateDisplay();
			}
		}
	};
	gui.add(setButton,'set');
	
	var p1 = gui.add(params, 'p1', -1, 1);
	p1.onChange( function(value) {
		debugLabel2.innerHTML = params.p1;
		applyPose();
	});
	
	var p2 = gui.add(params, 'p2', -1, 1);
	p2.onChange( function(value) {
		debugLabel2.innerHTML = params.p2;
		applyPose();
	});
	
	var p3 = gui.add(params, 'p3', -1, 1);
	p3.onChange( function(value) {
		debugLabel2.innerHTML = params.p3;
		applyPose();
	});
	
	var p4 = gui.add(params, 'p4', -1000, 1000);
	p4.onChange( function(value) {
		debugLabel2.innerHTML = params.p4;
		applyPose();
	});
	
	var p5 = gui.add(params, 'p5', -1, 1);
	p5.onChange( function(value) {
		debugLabel2.innerHTML = params.p5;
		applyPose();
	});
	
	var p6 = gui.add(params, 'p6', -1, 1);
	p6.onChange( function(value) {
		debugLabel2.innerHTML = params.p6;
		applyPose();
	});
	
	var p7 = gui.add(params, 'p7', -1, 1);
	p7.onChange( function(value) {
		debugLabel2.innerHTML = params.p7;
		applyPose();
	});
	
	var p8 = gui.add(params, 'p8', -1000, 1000);
	p8.onChange( function(value) {
		debugLabel2.innerHTML = params.p8;
		applyPose();
	});
	
	var p9 = gui.add(params, 'p9', -1, 1);
	p9.onChange( function(value) {
		debugLabel2.innerHTML = params.p9;
		applyPose();
	});
	
	var p10 = gui.add(params, 'p10', -1, 1);
	p10.onChange( function(value) {
		debugLabel2.innerHTML = params.p10;
		applyPose();
	});
	
	var p11 = gui.add(params, 'p11', -1, 1);
	p11.onChange( function(value) {
		debugLabel2.innerHTML = params.p11;
		applyPose();
	});
	
	var p12 = gui.add(params, 'p12', -1000, 1000);
	p12.onChange( function(value) {
		debugLabel2.innerHTML = params.p12;
		applyPose();
	    //debugLabel.innerHTML = params.p12;						
	});
	
	var p13 = gui.add(params, 'p13', -1, 1);
	p13.onChange( function(value) {
		debugLabel2.innerHTML = params.p13;
		applyPose();
	});
	
	var p14 = gui.add(params, 'p14', -1, 1);
	p14.onChange( function(value) {
		debugLabel2.innerHTML = params.p14;
		applyPose();
	});
	
	var p15 = gui.add(params, 'p15', -1, 1);
	p15.onChange( function(value) {
		debugLabel2.innerHTML = params.p15;
		applyPose();
	});
	
	var p16 = gui.add(params, 'p16', -1, 1);
	p16.onChange( function(value) {
		debugLabel2.innerHTML = params.p16;
		applyPose();
	});
	
}
			
function applyPose() {
	debugLabel.innerHTML = "applying pose";
	Scene4D.applyPoseToObject3d([params.p1, params.p2, params.p3, params.p4, params.p5, params.p6, params.p7, params.p8, params.p9, params.p10, params.p11, params.p12, params.p13, params.p14, params.p15, params.p16], scope.threejs.editor.sceneRoot);
		debugLabel.innerHTML = "applied pose";

	//debugLabel.innerHTML = DisplayMatrix44(planeMesh.matrix.elements);
	//animate();
	
}