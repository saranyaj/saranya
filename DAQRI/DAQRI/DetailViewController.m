//
//  DetailViewController.m
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/22/15.
//  Copyright (c) 2015 rc. All rights reserved.
//
#define cellIdentifier @"ProjectCell"


#import "DetailViewController.h"
#import "DAQRITableViewCell.h"



@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:47.0/255.0 green:51.0/255.0 blue:56.0/255.0 alpha:0.9];
    [self.tableView setSeparatorColor:DAQRI_TABLEVIEW_SEPARATOR_COLOR];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData:) name:@"ReloadData" object:nil];
    
    [self prepolpulateAllDatasForProject];
    [self setFontsAttributesToView];
    
     self.pointOfOriginView.hidden = YES;
    self.nextPageButton.hidden = YES;

    // Do any additional setup after loading the view.
}


-(void)viewWillAppear:(BOOL)animated{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:self.nameLabel.text style:UIBarButtonItemStylePlain target:self action:@selector(projectsButtonPressed:)];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.tableViewDataSource.count;
}


- (DAQRITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DAQRITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell == nil){
        cell = [[DAQRITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSDictionary *dict = [self.tableViewDataSource objectAtIndex:indexPath.row];
    cell.nameLabel.text = dict[@"name"];
    cell.nameLabel.font = [UIFont fontWithName:PROXIMANOVAS_REGULAR size:15];
    cell.nameLabel.textColor = [UIColor colorWithRed:167.0/255.0 green:169.0/255.0 blue:172.0/255.0 alpha:0.9];
    cell.backgroundColor =  [UIColor clearColor];
    cell.leftImage.image =[UIImage imageNamed:@"icn_doc1"];
    
    CGRect frame = cell.leftImage.frame;
    frame.size.width = cell.leftImage.image.size.width;
    frame.size.height = cell.leftImage.image.size.height;
    cell.leftImage.frame =frame;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //[self performSegueWithIdentifier:@"ProjectExpansionPage" sender:[self.tableViewDataSource objectAtIndex:indexPath.row]];
}



#pragma mark left and right bar butten item actions

- (IBAction)settingsButtonPressed:(id)sender {
    UIBarButtonItem *button = (UIBarButtonItem *)sender;
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([button.title isEqualToString:SETTINGS_TITLE]){
        [appDelegate showTwoViewInSplitViewController];
        [button setTitle:MAINMENU_TITLE];
        
    }else{
        [appDelegate showOneViewInSplitViewController];
        [button setTitle:SETTINGS_TITLE];
    }
        
}

-(void)projectsButtonPressed:(id)sender{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate showOneViewInSplitViewController];
}



 - (IBAction)nextPageButtonPressed:(id)sender {
     
     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

     const bool usesInstantOn = (YES == _ltaskData.usesInstantOn);
     const bool isUsingInstantOn = !([appDelegate IsVuforiaEnabled]);
     
     if (usesInstantOn != isUsingInstantOn)
     {
         if (usesInstantOn)
         {
             [appDelegate EnableInstantOn];
         }
         else
         {
             [appDelegate EnableVuforia];
         }
     }
     
     if ([Globals taskBaseURL] == nil)
     {
         //Daqri::Log::Error("Task Base URL is nil, no tasks will be downloaded");
     }
     else
     {
         NSString* taskPath = [NSString stringWithFormat:@"%@%@", [Globals taskBaseURL], _ltaskData.taskId];
        // Daqri::Log::Debug("Switching to AR with scene - %s", [taskPath UTF8String]);
         [appDelegate setSceneJSON:taskPath];
     }
     
     [appDelegate addingSplitViewInTaskPage:self.navigationController taskData:_ltaskData];


 }

#pragma mark Prepopulate items in View

-(void)prepolpulateAllDatasForProject{
    _sharedManger = [DAQRIWebRequestManager shareManager];
    
    if([_sharedManger.projects count]){
        ProjectData* projData = [_sharedManger.projects objectAtIndex:_sharedManger.selectedIndexPath.row];
        self.nameLabel.text         = projData.projectName;
        
        if(![projData.projDescription isKindOfClass:[NSNull class]])
            self.descriptionLabel.text  = projData.projDescription;
        else
            self.descriptionLabel.text = @"";
        
        self.stepsCountLabel.text        = [NSString stringWithFormat:@"%d/%d",projData.steps.count,projData.steps.count];
        
        
        self.tableViewDataSource = projData.projDocAssets;
    }
    
    [self adjustTopDetailViewFrames];
    [self adjustTableViewDynamically];
    
}

-(void)setFontsAttributesToView{
    
    self.nameLabel.textColor = [UIColor whiteColor];
    self.stepsTitleLabel.textColor = [UIColor whiteColor];
    self.tasksTitleLabel.textColor = [UIColor whiteColor];
    self.stepsCountLabel.textColor = [UIColor whiteColor];
    self.taskCountLabel.textColor = [UIColor whiteColor];
    self.documentTypeLabel.textColor = [UIColor whiteColor];
    
    self.stepsCountLabel.font = [UIFont fontWithName:PROXIMANOVAT_THIN size:34];
    self.taskCountLabel.font = [UIFont fontWithName:PROXIMANOVAT_THIN size:34];
    
    self.documentTypeLabel.font = [UIFont fontWithName:PROXIMANOVA_BOLD size:23];
    self.stepsTitleLabel.font = [UIFont fontWithName:PROXIMANOVA_BOLD size:14];
    self.tasksTitleLabel.font = [UIFont fontWithName:PROXIMANOVA_BOLD size:14];
    
    self.projectTitleAndDescriptionSplitView.backgroundColor  = [UIColor colorWithRed:63.0/255.0 green:67.0/255.0 blue:72.0/255.0 alpha:0.9];
    self.descriptionLabel.textColor = [UIColor colorWithRed:167.0/255.0 green:169.0/255.0 blue:172.0/255.0 alpha:0.9];
    self.splitviewBetweenDescriptionandPointOfOrigin.backgroundColor = self.projectTitleAndDescriptionSplitView.backgroundColor;
}

#pragma mark Dynaic Frames Adjustements

-(void)adjustTopDetailViewFrames{
    
    int y = 0;
    
    CGRect frame = self.descriptionLabel.frame;
    CGSize size = [self calculateDynamicHeightOfControls:self.descriptionLabel.text fontName:self.descriptionLabel.font size:frame.size];
    frame.size.height = size.height + 10;
    
    self.descriptionLabel.translatesAutoresizingMaskIntoConstraints = YES;
    self.descriptionLabel.frame = frame;
    
    y = frame.origin.y + frame.size.height; // updating y value
    
    if(!self.pointOfOriginView.hidden){
        
        frame = self.splitviewBetweenDescriptionandPointOfOrigin.frame;
        frame.origin.y = y + 5 ;
        self.splitviewBetweenDescriptionandPointOfOrigin.translatesAutoresizingMaskIntoConstraints = YES;
        self.splitviewBetweenDescriptionandPointOfOrigin.frame = frame;
        self.splitviewBetweenDescriptionandPointOfOrigin.hidden = NO;
        
        y += frame.size.height;
        
        frame = self.pointOfOriginView.frame;
        frame.origin.y  = y +15;
        self.pointOfOriginView.frame = frame;
        self.pointOfOriginView.translatesAutoresizingMaskIntoConstraints = YES;
        
    }else{
        self.splitviewBetweenDescriptionandPointOfOrigin.hidden = YES;
    }
    
}

-(CGSize)calculateDynamicHeightOfControls:(NSString *)text fontName:(UIFont *)font size:(CGSize) size{
    
    CGSize calculatedSize = [text boundingRectWithSize:size
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{
                                                         NSFontAttributeName : font
                                                         }
                                               context:nil].size;
    return calculatedSize;
    
}


-(void)reloadData:(NSNotification *)notificationValue{
    
    id dataType = [notificationValue object];
    
    if([dataType isKindOfClass:[StepData class]]){
        StepData *stepData = (StepData *)dataType;
        self.nameLabel.text         = stepData.stepName;
        self.tableViewDataSource    = stepData.stepDocAssets;
        
        if(![stepData.stepDescription isKindOfClass:[NSNull class]])
            self.descriptionLabel.text  = stepData.stepDescription;
        else
            self.descriptionLabel.text = @"";
        
        if(stepData.steppointOfOriginName){
            self.pointOfOriginView.hidden = NO;
            self.pointOfOriginImageName.text   = stepData.steppointOfOriginName;
        }
        self.nextPageButton.hidden = YES;

        
        
    }else{
        TaskData *taskData = (TaskData *)dataType;
        self.nameLabel.text         = taskData.taskName;
        self.tableViewDataSource    = taskData.taskDocAssets;
        self.pointOfOriginView.hidden = YES;
        self.nextPageButton.imageView.image = [UIImage imageNamed:@"bttn_start_task"];
        self.nextPageButton.hidden = NO;
        
        _ltaskData = taskData;
        
        if(![taskData.taskDescription isKindOfClass:[NSNull class]])
            self.descriptionLabel.text  = taskData.taskDescription;
        else
            self.descriptionLabel.text = @"";
        
    }
    
    self.descriptionLabel.font = [UIFont fontWithName:PROXIMANOVAS_REGULAR size:16];
    [self adjustTopDetailViewFrames];
    [self adjustTableViewDynamically];
    
}



-(void)adjustTableViewDynamically{
    
    if(self.tableViewDataSource.count){
        self.tableView.hidden = NO;
        self.documentTypeLabel.hidden = NO;
        [self.tableView reloadData];
    }
    else{
        self.tableView.hidden = YES;
        self.documentTypeLabel.hidden = YES;
    }
    
    self.tableView.translatesAutoresizingMaskIntoConstraints = YES;
    CGRect frame = self.tableView.frame;
    frame.size.height = self.tableView.rowHeight * self.tableViewDataSource.count;
    [self.tableView setFrame:frame];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
