//
//  DataViewermasterTableViewController.h
//  DAQRI
//
//  Created by Saranya Jayaseelan on 2/3/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataViewerMasterTableViewController : UIViewController
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (strong, nonatomic) IBOutlet UITableView *tableview;
@property (nonatomic,retain)  NSMutableArray *tableViewDataSource;

@end
