//
//  SettingsViewControllerTableViewController.h
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/26/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UITableViewController
@property (nonatomic,retain)  NSArray *tableViewDataSource;

@end
