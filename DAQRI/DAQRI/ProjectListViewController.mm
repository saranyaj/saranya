//
//  DetailViewController.m
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/19/15.
//  Copyright (c) 2015 rc. All rights reserved.
//
#define cellIdentifier @"ProjectListCell"


#import "ProjectListViewController.h"
#import  "MasterTableViewController.h"

#include "DaqriCore/ContentManagerIOS.h"
#include "DaqriCore/DownloadManager.h"
#import  "ProjectListTableViewCell.h"
#include "ProjectDataTypes.h"



@interface ProjectListViewController (){
SpinnerView *spinnerView;
    AppDelegate *appDelegate;
}
@end

@implementation ProjectListViewController

-(void)reloadTableView:(NSNotification *)notification{
    
    [spinnerView stopAnimation];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.tableViewDataSource = [notification object];
    [self.projectListTableview reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _sharedManger = [DAQRIWebRequestManager shareManager] ;
    self.view.backgroundColor = [UIColor colorWithRed:68/255.0f green:72/255.0f blue:78/255.0f alpha:0.9];
    self.projectListTableview.backgroundColor = [UIColor clearColor];


    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"bttn_settings1"] style:UIBarButtonItemStylePlain target:self action:@selector(settingsButtonPressed:)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:LOGOUT_TITLE style:UIBarButtonItemStylePlain target:self action:@selector(LogoutButtonPressed:)];
    
    // Do any additional setup after loading the view.
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableView:) name:@"ReloadProjectTable" object:nil];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^{
            [_sharedManger pulltheDataFrom4DLibraryForProjectList];
    });
   
}


-(void)viewWillAppear:(BOOL)animated{
    if(self.tableViewDataSource.count ==0)
    [self addSpinnerView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark left and right bar butten item actions

- (IBAction)settingsButtonPressed:(id)sender {

    if(appDelegate.splitView.viewControllers.count==1){
        [appDelegate showTwoViewInSplitViewController];

    }else{
        [appDelegate showOneViewInSplitViewController];
    }
    
    [self.projectListTableview reloadData];
}

- (IBAction)LogoutButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.tableViewDataSource.count;
}


- (ProjectListTableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProjectListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    if(cell == nil){
        cell = [[ProjectListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
   
    if (indexPath.row % 2) {
        cell.contentView.backgroundColor = [UIColor colorWithRed:178/255.0f green:189/255.0f blue:192/255.0f alpha:0.9];
    } else {
        cell.contentView.backgroundColor = [UIColor colorWithRed:47/255.0f green:51/255.0f blue:56/255.0f alpha:0.9];
    }
    cell.backgroundColor = cell.contentView.backgroundColor;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    ProjectData* projData = [self.tableViewDataSource objectAtIndex:indexPath.row];
    cell.nameLabel.text = projData.projectName;
    cell.noOfStepsLabel.text = [NSString stringWithFormat:@"%d/%d",projData.steps.count,projData.steps.count];
    cell.noOfTaskLabel.text = [NSString stringWithFormat:@"%d/%d",projData.steps.count,projData.steps.count];
    
    CGRect frame;
    if([appDelegate.splitView.viewControllers count] == 2){
        frame = CGRectMake(346, 73,cell.projectDetailPageButton.frame.size.width, cell.projectDetailPageButton.frame.size.height);
    }else{
        frame = CGRectMake(666, 73,cell.projectDetailPageButton.frame.size.width, cell.projectDetailPageButton.frame.size.height);
    }
    cell.projectDetailPageButton.translatesAutoresizingMaskIntoConstraints = YES;
    cell.projectDetailPageButton.frame = frame;
    cell.projectDetailPageButton.tag = indexPath.row;
    [cell.projectDetailPageButton addTarget:self action:@selector(continueButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [self setFontsAttributesToCell:cell];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _sharedManger.selectedIndexPath = indexPath;
    [appDelegate addingSplitViewInStepTaskPage:self.navigationController];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)setFontsAttributesToCell:(ProjectListTableViewCell *)cell{
    cell.nameLabel.font = [UIFont fontWithName:PROXIMANOVAT_THIN size:34];
    cell.noOfStepsLabel.font = [UIFont fontWithName:PROXIMANOVAT_THIN size:34];
    cell.noOfTaskLabel.font = [UIFont fontWithName:PROXIMANOVAT_THIN size:34];
    cell.stepsTitleLabel.font = [UIFont fontWithName:PROXIMANOVA_BOLD size:14];
    cell.taskTitleLabel.font = [UIFont fontWithName:PROXIMANOVA_BOLD size:14];

    
    cell.nameLabel.textColor = [UIColor whiteColor];
    cell.noOfStepsLabel.textColor = [UIColor whiteColor];
    cell.noOfTaskLabel.textColor = [UIColor whiteColor];
    cell.stepsTitleLabel.textColor = [UIColor whiteColor];
    cell.taskTitleLabel.textColor = [UIColor whiteColor];
    cell.cellSplitView.backgroundColor = [UIColor whiteColor];

}

-(void)continueButtonPressed:(id)sender{
    
    UIButton *btn = (UIButton *)sender;
    NSIndexPath *indexPath =[NSIndexPath indexPathForRow:btn.tag inSection:0];
    
    _sharedManger.selectedIndexPath = indexPath;
    [appDelegate addingSplitViewInStepTaskPage:self.navigationController];
    [self.projectListTableview deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark Spinner View methods

-(void)addSpinnerView{
    spinnerView = [[SpinnerView alloc]initWithFrame:self.view.frame];
    [self.view addSubview:spinnerView];
    [spinnerView startAnimation];
}

-(void) removeSpinnerView{
    [spinnerView stopAnimation];
    [spinnerView removeFromSuperview];
}

#pragma mark seque method
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [appDelegate addingSplitViewInStepTaskPage:self.navigationController];
}


@end
