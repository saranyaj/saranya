//
//  ViewController.h
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/16/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

-(IBAction)loginButtonPressed:(id)sender;
@end

