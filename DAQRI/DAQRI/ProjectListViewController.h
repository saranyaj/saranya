//
//  ProjectListViewController.h
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/19/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ProjectListViewController : UIViewController<UISplitViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UINavigationBar *navBar;
@property (strong, nonatomic) IBOutlet UITableView *projectListTableview;
@property (nonatomic,retain)  NSMutableArray *tableViewDataSource;

@property (nonatomic, retain) NSMutableDictionary* projectDownloads;
@property (nonatomic, retain) NSMutableArray* projects;
@property (nonatomic, retain) NSMutableArray* dataSource;
@property (strong, nonatomic) IBOutlet UIView *tableViewHeaderView;


@property (nonatomic, strong) DAQRIWebRequestManager *sharedManger;

//- (IBAction)settingsButtonPressed:(id)sender;

@end
