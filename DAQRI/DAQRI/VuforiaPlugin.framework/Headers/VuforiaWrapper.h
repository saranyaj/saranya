//
//  VuforiaWrapper.h
//  VuforiaPlugin
//
//  Created by Dusten Sobotta on 11/17/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#ifndef __VuforiaPlugin__VuforiaWrapper__
#define __VuforiaPlugin__VuforiaWrapper__

#include "DaqriCore/Types.h"

#include <stdio.h>
#include <vector>
#include <string>

#if PLATFORM_IOS
    #include <OpenGLES/ES2/gl.h>
#else
    #include <GLES2/gl2.h>
#endif

#include <QCAR/QCAR.h>
#include <QCAR/VideoBackgroundConfig.h>
#include <QCAR/Matrices.h>

#include "VuforiaRenderer.h"

using std::vector;
using std::string;

namespace QCAR
{
	class DataSet;
	class TrackerManager;
	class Tracker;
	class CameraDevice;
	class Renderer;
	class ImageTracker;
	class State;
	class TargetFinder;
}

namespace Daqri
{
	
class VuforiaWrapper
{

public:
	
	//INIT
	static VuforiaWrapper& GetInstance();
	bool Init();
	bool InitTrackers();
	bool InitCamera();
	bool InitTargetFinder(const string& accessKey, const string& secretKey);
	bool ReInitCamera();
	void DeInit();
	
	//START/STOP
	bool StartCamera();
	bool StopCamera();
	
	bool StartTrackers();
	bool StopTrackers();
	
	bool StartCloudRecognition();
	bool StopCloudRecognition();
	bool ClearCloudTrackables();
	
	bool StartExtendedTracking(Trackable& inTrackable) const;
	bool StopExtendedTracking(Trackable& inTrackable) const;
	bool ResetExtendedTracking();
	
	bool SetPersistentMode(const bool enabled);
	
	//CONFIGURATION
	void		SetMaxTrackableTargets(const int numTargets);
	void		SetSurfaceBoundsAndRotation(const int width, const int height, const Orientation rotation);
	Viewport2D	ConfigureVideoBackground(float viewWidth, float viewHeight, bool portrait);
	
	
	//CONTENT
	void Update(vector<Trackable>& trackables, GLuint framebuffer, GLuint colorRenderbuffer);
	
	bool LoadAndActivateDataSet(const string& path);
	
	Matrix44F	GetProjectionMatrix(float nearPlane, float farPlane) const;
    Viewport2D	GetViewPort() const;
	
private:
	
	VuforiaWrapper();
	VuforiaWrapper(VuforiaWrapper const&) = delete;
	VuforiaWrapper& operator=(const VuforiaWrapper&) = delete;
	
	//PRIVATE ACCESSORS
	QCAR::TrackerManager&			GetTrackerManager() const;
	QCAR::CameraDevice&				GetCameraDevice() const;
	QCAR::ImageTracker*				GetImageTracker() const;
	QCAR::TargetFinder*				GetTargetFinder() const;
	
	//PRIVATE HELPERS
	bool							DeInitCamera();
	QCAR::VideoBackgroundConfig		GenerateVideoBackgroundConfig(float viewWidth, float viewHeight, bool portrait) const;
	QCAR::Matrix44F					GetVuforiaProjectionMatrix(float nearPlane, float farPlane) const;
	Matrix44F						MatrixFromVuforiaMatrix(const QCAR::Matrix44F& vuforiaMatrix) const;

	QCAR::DataSet*					LoadVuforiaDataSet(const string& path);
	bool							ActivateVuforiaDataSet(QCAR::DataSet* dataSet);
	void							UpdateTrackablesWithState(vector<Trackable>& trackables, const QCAR::State& state);
	void							UpdateCloudRecognitionResults();
	
	VuforiaRenderer		vuforiaRenderer;
	QCAR::DataSet*		currentVuforiaDataSet;
	QCAR::Matrix44F		projectionMatrix;
    Viewport2D			viewport;
	bool				targetFinderInitialized;
	bool				cloudRecognitionEnabled;
	bool				imageTrackingEnabled;
	bool				vuforiaInitialized;
};

} //end namespace Daqri

#endif /* defined(__VuforiaPlugin__VuforiaWrapper__) */
