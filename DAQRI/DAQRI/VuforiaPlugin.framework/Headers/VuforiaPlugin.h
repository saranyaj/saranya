//
//  VuforiaPlugin.h
//  VuforiaPlugin
//
//  Created by Dusten Sobotta on 11/24/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

//! Project version number for VuforiaPlugin.
FOUNDATION_EXPORT double VuforiaPluginVersionNumber;

//! Project version string for VuforiaPlugin.
FOUNDATION_EXPORT const unsigned char VuforiaPluginVersionString[];
