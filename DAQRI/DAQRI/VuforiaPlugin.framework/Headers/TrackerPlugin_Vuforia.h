//
//  TrackerPlugin_Vuforia.h
//  VuforiaPlugin
//
//  Created by Dusten Sobotta on 12/6/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#ifndef __VuforiaPlugin__TrackerPlugin_Vuforia__
#define __VuforiaPlugin__TrackerPlugin_Vuforia__

#include <stdio.h>

#include "DaqriCore/Types.h"
#include "DaqriCore/TrackerPlugin.h"

namespace Daqri
{

class TrackerPlugin_Vuforia : public TrackerPlugin
{
public:
	TrackerPlugin_Vuforia();

	~TrackerPlugin_Vuforia() = default;

	const char* GetName() override { return "TrackerPlugin_Vuforia"; }

	bool Load() override;
	bool Start() override;
	bool StartTracking() override;
	
	bool StopTracking() override;
	bool Stop() override;
	bool UnLoad() override;

	void SetSurfaceSizeAndRotation(const int width, const int height, const Orientation orientation) override;
	
	//renders the video background, and updates the vector of trackables in the scene
	void Update(const GLuint frameBuffer, const GLuint renderBuffer) override;
	
	Matrix44F					GetProjectionMatrix() const override;
	Viewport2D					GetViewport() const override;
	const vector<Trackable>&	GetTrackables() const override;
	
private:
	bool loaded				= false;
	bool trackingEnabled	= false;
	bool cameraActive		= false;
};

} //end namespace Daqri

#endif /* defined(__VuforiaPlugin__TrackerPlugin_Vuforia__) */
