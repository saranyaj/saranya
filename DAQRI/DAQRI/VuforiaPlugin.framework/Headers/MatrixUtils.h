//
//  MatrixUtils.h
//  VuforiaPlugin
//
//  Created by Dusten Sobotta on 11/18/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

/*===============================================================================
 Copyright (c) 2012-2014 Qualcomm Connected Experiences, Inc. All Rights Reserved.
 
 Vuforia is a trademark of QUALCOMM Incorporated, registered in the United States
 and other countries. Trademarks of QUALCOMM Incorporated are used with permission.
 ===============================================================================*/

#ifndef __VuforiaPlugin__MatrixUtils__
#define __VuforiaPlugin__MatrixUtils__

#include <cstddef>

namespace Daqri
{
namespace Utils
{
	// Print a 4x4 matrix
	void printMatrix(const float* matrix);
	
	// Print GL error information
//	void checkGlError(const char* operation);
	
	// Set the rotation components of a 4x4 matrix
	void setRotationMatrix(float angle, float x, float y, float z, float *nMatrix);
	
	// Set the translation components of a 4x4 matrix
	void translatePoseMatrix(float x, float y, float z, float* nMatrix = nullptr);
	
	// Apply a rotation
	void rotatePoseMatrix(float angle, float x, float y, float z, float* nMatrix = nullptr);
	
	// Apply a scaling transformation
	void scalePoseMatrix(float x, float y, float z, float* nMatrix = nullptr);
	
	// Multiply the two matrices A and B and write the result to C
	void multiplyMatrix(float *matrixA, float *matrixB, float *matrixC);
	
	void setOrthoMatrix(float nLeft, float nRight, float nBottom, float nTop,
						float nNear, float nFar, float *nProjMatrix);
	
	void screenCoordToCameraCoord(int screenX, int screenY, int screenDX, int screenDY,
								  int screenWidth, int screenHeight, int cameraWidth, int cameraHeight,
								  int * cameraX, int* cameraY, int * cameraDX, int * cameraDY);
} //end namespace Utils

} //end namespace Daqri

#endif //defined(__VuforiaPlugin__MatrixUtils__)
