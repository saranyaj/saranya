//
//  VuforiaRendering.h
//  VuforiaPlugin
//
//  Created by Dusten Sobotta on 11/26/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#ifndef __VuforiaPlugin__VuforiaRendering__
#define __VuforiaPlugin__VuforiaRendering__

#include <stdio.h>
#include <string>

#if PLATFORM_IOS
    #include <OpenGLES/ES2/gl.h>
#else
    #include <GLES2/gl2.h>
#endif

#include <QCAR/Renderer.h>

#include "DaqriCore/Types.h"

using std::string;

namespace Daqri
{
class VuforiaRenderer
{
public:
	VuforiaRenderer() = default;
	VuforiaRenderer(VuforiaRenderer const&) = delete;
	VuforiaRenderer& operator=(const VuforiaRenderer&) = delete;
	
	void	SetOrthoMatrix(float nLeft, float nRight, float nBottom, float nTop,
							   float nNear, float nFar, float *nProjMatrix);
	GLuint	CompileShader(const string& vertShaderSource, const GLenum shaderType);
	GLuint	CompileShaders(const string& vertShaderSource, const string& fragShaderSource);
	void	InitShaders();
	void	DrawVideoBackground(QCAR::Renderer& renderer, const Viewport2D& viewport);
	void	CreateVideoBackgroundMesh();
	void	SetOrientation(const Orientation orientation);
	
private:

	// These values indicate how many rows and columns we want for our video background texture polygon
	static const int vbNumVertexCols = 2;
	static const int vbNumVertexRows = 2;
	
	// These are the variables for the vertices, coords and inidices
	static const int vbNumVertexValues=vbNumVertexCols*vbNumVertexRows*3;      // Each vertex has three values: X, Y, Z
	static const int vbNumTexCoord=vbNumVertexCols*vbNumVertexRows*2;          // Each texture coordinate has 2 values: U and V
	static const int vbNumIndices=(vbNumVertexCols-1)*(vbNumVertexRows-1)*6;   // Each square is composed of 2 triangles which in turn
	// have 3 vertices each, so we need 6 indices
	
	// These are the data containers for the vertices, texcoords and indices in the CPU
	float   vbOrthoQuadVertices     [vbNumVertexValues];
	float   vbOrthoQuadTexCoords    [vbNumTexCoord];
	GLbyte  vbOrthoQuadIndices      [vbNumIndices];
	
	// This will hold the data for the projection matrix passed to the vertex shader
	float   vbOrthoProjMatrix[16];

	const string BackgroundVertShader =
	"\
		attribute vec4 vertexPosition; \
		attribute vec2 vertexTexCoord; \
		\
		varying vec2 texCoord; \
		\
		uniform mat4 projectionMatrix; \
		\
		void main() \
		{ \
			gl_Position = projectionMatrix * vertexPosition; \
			texCoord = vertexTexCoord; \
		} \
	";
	
	const string BackgroundFragShader =
	"\
		precision mediump float; \
		varying vec2 texCoord; \
		uniform sampler2D texSampler2D; \
		void main () \
		{ \
			gl_FragColor = texture2D(texSampler2D, texCoord); \
		} \
	";
	
	// ----- Video background OpenGL data -----
	
	struct tagVideoBackgroundShader {
		// These handles are required to pass the values to the video background
		// shaders
		GLuint vbShaderProgramID;
		GLuint vbVertexPositionHandle;
		GLuint vbVertexTexCoordHandle;
		GLuint vbTexSampler2DHandle;
		GLuint vbProjectionMatrixHandle;
		
		// This flag indicates whether the mesh values have been initialized
		bool vbMeshInitialized;
		bool vbShaderInitialized;
	} videoBackgroundShader;
	
}; //end class VuforiaRenderer
} //end namespace Daqri

#endif /* defined(__VuforiaPlugin__VuforiaRendering__) */
