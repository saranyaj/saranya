//
//  TrackerPlugin_InstantOn.h
//  InstantOnPlugin
//
//  Created by Dusten Sobotta on 12/8/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#ifndef __InstantOnPlugin__TrackerPlugin_InstantOn__
#define __InstantOnPlugin__TrackerPlugin_InstantOn__

#include <stdio.h>

#include "DaqriCore/Types.h"
#include "DaqriCore/TrackerPlugin.h"

namespace Daqri
{
	
class TrackerPlugin_InstantOn : public TrackerPlugin
{
public:
	TrackerPlugin_InstantOn() = default;
	~TrackerPlugin_InstantOn() = default;
	
	const char* GetName() override { return "TrackerPlugin_InstantOn"; }
	
	bool Load() override;
	bool Start() override;
	bool StartTracking() override;
	bool StopTracking() override;
	bool Stop() override;
	bool UnLoad() override;
	
	void SetSurfaceSizeAndRotation(const int width, const int height, const Orientation orientation) override;
	
	//renders the video background, and updates the vector of trackables in the scene
	void Update(const GLuint frameBuffer, const GLuint renderBuffer) override;
	
	Matrix44F					GetProjectionMatrix() const override;
	Viewport2D					GetViewport() const override;
    
	const vector<Trackable>&	GetTrackables() const override;
};
	
} //end namespace Daqri

#endif /* defined(__InstantOnPlugin__TrackerPlugin_InstantOn__) */
