//
//  InstantOnPlugin.h
//  InstantOnPlugin
//
//  Created by Dusten Sobotta on 12/8/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

//! Project version number for InstantOnPlugin.
FOUNDATION_EXPORT double InstantOnPluginVersionNumber;

//! Project version string for InstantOnPlugin.
FOUNDATION_EXPORT const unsigned char InstantOnPluginVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <InstantOnPlugin/PublicHeader.h>


