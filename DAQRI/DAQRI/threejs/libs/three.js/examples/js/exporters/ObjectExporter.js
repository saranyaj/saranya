/**
 * @author mrdoob / http://mrdoob.com/
 */

THREE.ObjectExporter = function () {};

THREE.ObjectExporter.prototype = {

	constructor: THREE.ObjectExporter,

	parse: function ( object, externalSave ) {

		// externalSave is a boolean to determine if this is for the local storage (auto saving) - False, or the 4DS save where we don't want geometric data on models - True

		var output = {
			metadata: {
				version: 4.3,
				type: 'Object',
				generator: 'ObjectExporter'
			}
		};

		output.files = object.files	|| [];

		//

		var geometries = {};
		var geometryExporter = new THREE.GeometryExporter();
		var bufferGeometryExporter = new THREE.BufferGeometryExporter();

		var parseGeometry = function ( geometry, externalSave ) {

			if ( output.geometries === undefined ) {

				output.geometries = [];

			}

			if ( geometries[ geometry.uuid ] === undefined ) {

				var data = {};

				data.uuid = geometry.uuid;

				if ( geometry.name !== "" ) data.name = geometry.name;

				var handleParameters = function ( parameters ) {

					for ( var i = 0; i < parameters.length; i ++ ) {

						var parameter = parameters[ i ];

						if ( geometry.parameters[ parameter ] !== undefined ) {

							data[ parameter ] = geometry.parameters[ parameter ];

						}

					}

				};

				if ( geometry instanceof THREE.PlaneGeometry ) {

					data.type = 'PlaneGeometry';
					handleParameters( [ 'width', 'height', 'widthSegments', 'heightSegments' ] );

				} else if ( geometry instanceof THREE.BoxGeometry ) {

					data.type = 'BoxGeometry';
					handleParameters( [ 'width', 'height', 'depth', 'widthSegments', 'heightSegments', 'depthSegments' ] );

				} else if ( geometry instanceof THREEx.CalloutGeometry ) {

					data.type = 'CalloutGeometry';
					if( externalSave === true ){
						data.data = geometryExporter.parse( geometry );
					}
					handleParameters( ["width", "height", "radius", "arrowHeight", "arrowXRight", "arrowXCenter", "arrowXLeft", "amount", "bevelThickness", "bevelSize"] );

				} else if ( geometry instanceof THREE.CircleGeometry ) {

					data.type = 'CircleGeometry';
					handleParameters( [ 'radius', 'segments' ] );

				} else if ( geometry instanceof THREE.CylinderGeometry ) {

					data.type = 'CylinderGeometry';
					handleParameters( [ 'radiusTop', 'radiusBottom', 'height', 'radialSegments', 'heightSegments', 'openEnded' ] );

				} else if ( geometry instanceof THREE.SphereGeometry ) {

					data.type = 'SphereGeometry';
					handleParameters( [ 'radius', 'widthSegments', 'heightSegments', 'phiStart', 'phiLength', 'thetaStart', 'thetaLength' ] );

				} else if ( geometry instanceof THREE.IcosahedronGeometry ) {

					data.type = 'IcosahedronGeometry';
					handleParameters( [ 'radius', 'detail' ] );

				} else if ( geometry instanceof THREE.TorusGeometry ) {

					data.type = 'TorusGeometry';
					handleParameters( [ 'radius', 'tube', 'radialSegments', 'tubularSegments', 'arc' ] );

				} else if ( geometry instanceof THREE.TorusKnotGeometry ) {

					data.type = 'TorusKnotGeometry';
					handleParameters( [ 'radius', 'tube', 'radialSegments', 'tubularSegments', 'p', 'q', 'heightScale' ] );

				} else if ( geometry instanceof THREEx.TextGeometry ) {

					data.type = 'TextGeometry';
					if( externalSave === true ){
						data.data = geometryExporter.parse( geometry );
					}
					handleParameters( [ 'text', 'font', 'weight', 'size', 'height', 'bevelThickness', 'bevelSize', 'bevelEnabled', 'bevelSegments', 'curveSegments' ] );
					
				} else if ( geometry instanceof THREE.BufferGeometry ) {

					data.type = 'BufferGeometry';
					data.data = bufferGeometryExporter.parse( geometry );

					delete data.data.metadata;

				} else if ( geometry instanceof THREE.Geometry ) {

					if( !externalSave ) {
						data.data = geometryExporter.parse( geometry );
						delete data.data.metadata;
					}

					data.type = 'Geometry';					
				}

				geometries[ geometry.uuid ] = data;

				output.geometries.push( data );

			}

			return geometry.uuid;

		};

		//

		var materials = {};
		var materialExporter = new THREE.MaterialExporter();

		var parseMaterial = function ( material ) {

			if ( output.materials === undefined ) {

				output.materials = [];

			}

			if ( materials[ material.uuid ] === undefined ) {

				var data = materialExporter.parse( material );

				delete data.metadata;

				if( material.map ){
					data.map	= parseTexture( material.map )
				}

				materials[ material.uuid ] = data;

				output.materials.push( data );

			}

			return material.uuid;

		};
		//

		var textures = {};
		var textureExporter = new THREE.TextureExporter();

		// Verify which textures we actually need to save (not the ones we download)
		var textureCheck = {};
		if(object.files) {
			for(var fileCount = 0; fileCount < object.files.length; fileCount++) {
				if(object.files[fileCount].type == "Image") {
					textureCheck[object.files[fileCount].uuid] = true;
				}
				
			}
		}

		var parseTexture = function ( texture ) {

			if ( output.textures === undefined ) {

				output.textures = [];

			}

			if ( textures[ texture.uuid ] === undefined ) {
				var data = {};
				if ( textureCheck [texture.uuid] == true ) {
					data["uuid"] = texture.uuid;
				} else {
					data = textureExporter.parse( texture );
				}

				delete data.metadata;

				textures[ texture.uuid ] = data;

				output.textures.push( data );

			}

			return texture.uuid;

		};

		//

		var parseObject = function ( object , externalSave ) {

			var data = {};



			data.uuid = object.uuid;

			if ( object.name !== '' ) data.name = object.name;
			if ( JSON.stringify( object.userData ) !== '{}' ) data.userData = object.userData;
			if ( object.visible !== true ) data.visible = object.visible;

			if ( object instanceof THREE.Scene ) {

				data.type = 'Scene';

			} else if ( object instanceof THREE.PerspectiveCamera ) {

				data.type = 'PerspectiveCamera';
				data.fov = object.fov;
				data.aspect = object.aspect;
				data.near = object.near;
				data.far = object.far;

			} else if ( object instanceof THREE.OrthographicCamera ) {

				data.type = 'OrthographicCamera';
				data.left = object.left;
				data.right = object.right;
				data.top = object.top;
				data.bottom = object.bottom;
				data.near = object.near;
				data.far = object.far;

			} else if ( object instanceof THREE.AmbientLight ) {

				data.type = 'AmbientLight';
				data.color = object.color.getHex();

			} else if ( object instanceof THREE.DirectionalLight ) {

				data.type = 'DirectionalLight';
				data.color = object.color.getHex();
				data.intensity = object.intensity;
				if (object.target) data.targetUUID = object.target.uuid

			} else if ( object instanceof THREE.PointLight ) {

				data.type = 'PointLight';
				data.color = object.color.getHex();
				data.intensity = object.intensity;
				data.distance = object.distance;

			} else if ( object instanceof THREE.SpotLight ) {

				data.type = 'SpotLight';
				data.color = object.color.getHex();
				data.intensity = object.intensity;
				data.distance = object.distance;
				data.angle = object.angle;
				data.exponent = object.exponent;
				if (object.target) data.targetUUID = object.target.uuid

			} else if ( object instanceof THREE.HemisphereLight ) {

				data.type = 'HemisphereLight';
				data.color = object.color.getHex();
				data.groundColor = object.groundColor.getHex();

			} else if ( object instanceof THREEx.ArrowObject ) {

				data.type = 'ArrowObject';
				['length', 'axisRadius', 'hasHeadOrigin', 'hasHeadTarget', 'headLength', 'headRadius'].forEach(function(key){
					data[key]	= object.parameters[key]
				})
				data.color	= object.parameters.color.getHex()
				data.direction	= object.parameters.direction.toArray()
				if( externalSave === true ){
					data.geometry	= parseGeometry( object.geometry )
				}

			} else if ( object instanceof THREEx.BrushPlane ) {

				data.type	= 'BrushPlane';
				data.geometry	= parseGeometry( object.geometry )
				data.material	= parseMaterial( object.material )
				data.color	= object.parameters.color.getHex()
				data.size	= object.parameters.size

			} else if ( object instanceof THREEx.Callout ) {

				data.type	= 'Callout';
				data.geometry	= parseGeometry( object.geometry )
				data.material	= parseMaterial( object.material )
				data.text	= object.text
				
				var planeMesh	= object.children[0]
				var map	= planeMesh.material.map
				var texture	= textureExporter.parse(map)
				
				// get the base64 image from the dataURL
				var dataURL	= texture.dataURL
				var index	= dataURL.indexOf(',')
				var base64Text	= dataURL.slice(index+1)
				data.texture = base64Text				
				 
			} else if ( object instanceof THREEx.VideoObject ) {

				data.type	= 'VideoObject';
				object.toJson( data );

			} else if ( object instanceof THREEx.GalleryObject ) {

				data.type	= 'GalleryObject';
				object.toJson( data );

			} else if ( object instanceof THREEx.ImageObject ) {

				data.type	= 'ImageObject';
				data.url	= object.image.src

			} else if ( object.multipleMixerPlane instanceof THREEx.HtmlMultipleMixer.Plane ) {

				data.type	= 'HtmlMultipleMixerPlane';
				var mixerPlane	= object.multipleMixerPlane.planes[0]
				var domElement	= mixerPlane.domElement
				console.assert(domElement instanceof HTMLIFrameElement)
				console.assert(mixerPlane.initialUrl)
				data.url	= mixerPlane.initialUrl
				if( data.url === 'about:blank' ){
					data.htmlContent	= domElement.contentWindow.document.body.innerHTML 
				}

			} else if ( object instanceof THREE.Mesh ) {

				data.type = 'Mesh';
				data.geometry = parseGeometry( object.geometry , externalSave );
				data.material = parseMaterial( object.material );

			} else if ( object instanceof THREE.Sprite ) {

				data.type = 'Sprite';
				data.material = parseMaterial( object.material );

			} else {

				data.type = 'Object3D';

			}


			data.matrix = object.matrix.toArray();

			// handle threex.posrotscaanimation
			if ( object.posrotscaAnimation ){
				data.posrotscaAnimation	= object.posrotscaAnimation.toJson()
			}

			if ( object.touchParams ) {

				data.touchParams = object.touchParams.toJson()
				
			}


			if ( object.children.length > 0 ) {

				data.children = [];

				for ( var i = 0; i < object.children.length; i ++ ) {

					if( object.children[i].userData.selectableUUID !== undefined )	continue

					if( object.children[i].serialisable === false )	continue

					data.children.push( parseObject( object.children[ i ], externalSave ) );

				}

				if( data.children.length === 0 )	delete data.children

			}

			return data;

		}

		output.object = parseObject( object , externalSave );

		return output;

	}

}