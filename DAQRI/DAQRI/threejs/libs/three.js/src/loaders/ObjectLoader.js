/**
 * @author mrdoob / http://mrdoob.com/
 */

THREE.ObjectLoader = function ( manager ) {

	this.manager = ( manager !== undefined ) ? manager : THREE.DefaultLoadingManager;

};

THREE.ObjectLoader.prototype = {

	constructor: THREE.ObjectLoader,

	load: function ( url, onLoad, onProgress, onError ) {

		var scope = this;

		var loader = new THREE.XHRLoader( scope.manager );
		loader.setCrossOrigin( this.crossOrigin );
		loader.load( url, function ( text ) {

			onLoad( scope.parse( JSON.parse( text ) ) );

		} );

	},

	setCrossOrigin: function ( value ) {

		this.crossOrigin = value;

	},

	parse: function ( json ) {

		// parse Files assumes we have a list of files stored in the JSON
		var files = this.parseFiles(json.files);
		var videos = files.videos;
		var geometries = this.parseGeometries( json.geometries, files.geometries );
		var textures = this.parseTextures( json.textures, files.textures );
		var materials = this.parseMaterials( json.materials, textures );
		var object = this.parseObject( json.object, geometries, materials, textures, videos );

		//////////////////////////////////////////////////////////////////////////////////
		//		post process to setup target object				//
		//////////////////////////////////////////////////////////////////////////////////
		// TODO jme- it should not be here - it should be in editor code

		// post process to setup target for lights, THREEx.Arrow
		// TODO: remember the targetUUID and write it to the json
		var defaultTargetObjectsToDelete = [];
		object.traverse( function(child) {

			var targetUUID = child.targetUUID;
			if ( targetUUID === undefined ) return;

			object.traverse( function( otherChild ){
				if ( otherChild.uuid !== targetUUID || otherChild === child ) {
					return;
				}
				if ( child.target ) defaultTargetObjectsToDelete.push( child.target );
				child.target = otherChild;
			});
		});
		for (var i = defaultTargetObjectsToDelete.length-1; i >= 0; --i) {
			var objToDelete = defaultTargetObjectsToDelete[i];
			if (objToDelete.parent) objToDelete.parent.remove(objToDelete);
		}

		if(json.files !== undefined)
			object.files = json.files;
		else
			object.files =  [];

		return object;

	},

	parseFiles: function (filesJson) {
		var sceneAssets = {};
		sceneAssets.geometries = {};
		sceneAssets.textures = {};
		if( filesJson !== undefined) {

			var assetDownloads = THREEx.assetDownloads;

			for ( var fjCounter = 0, l = filesJson.length; fjCounter < l; fjCounter ++ ) {

				var file;
				var name;

				var requiredAssetDescription = filesJson[ fjCounter ];
				
				if(requiredAssetDescription === undefined) continue;
				
				var assetID = requiredAssetDescription.id;
				// We assume the structure of data to consist of an assetID, type, and an array of UUID's

				var assetDownloadState;
				if (assetDownloads !== undefined) {
					assetDownloadState = assetDownloads[assetID];
				}

				if (assetDownloadState !== undefined && assetDownloadState.fileData !== undefined ) {
					file = assetDownloadState.fileData;
					name = assetDownloadState.url.split("/").pop();
					console.log("found scene asset: " + name + " (asset ID = " + assetID + ")");
				}
				else {
					console.log("error! scene references asset " + assetID + " which failed to download");
					console.log("asset download status: ");
					console.log(assetDownloadState);
					continue;
				}

				switch ( requiredAssetDescription.type ) {
					
					case 'Model':

						var meshes = daqriFile.LoadModel( file, name );
						for(i = 0, l = meshes.length; i < l; i++) {
							meshes[i].geometry.uuid = requiredAssetDescription.uuids[i];
							sceneAssets.geometries[ requiredAssetDescription.uuids[i] ] = meshes[i].geometry;
						}
						break;
					
					case 'Image':
						var texture = file;
						texture.uuid = requiredAssetDescription.uuid;
						sceneAssets.textures[ requiredAssetDescription.uuid ] = texture;												
						break;

					// For Video the uuid is actually the uuid of the object the video is attached to. Videos currently dont have a separation from their object
					case 'Video': 
						var video = file;
						video.uuid = requiredAssetDescription.uuid;
						if(!sceneAssets.videos) {
							sceneAssets.videos = {};
						}
						sceneAssets.videos[ video.uuid ] = video;
						break;
					default:
						alert( 'Unsupported file format.' );
						break;
				}
			}
		}

		return sceneAssets;
	},

	parseGeometries: function ( json, geometries) {

		if( geometries === undefined) {
			geometries = {};
		}

		if ( json !== undefined ) {

			var geometryLoader = new THREE.JSONLoader();
			var bufferGeometryLoader = new THREE.BufferGeometryLoader();

			for ( var i = 0, l = json.length; i < l; i ++ ) {

				var geometry;
				var data = json[ i ];

				if( geometries[ data.uuid ] !== undefined || data === undefined ) {
					continue;
				}

				switch ( data.type ) {

					case 'PlaneGeometry':

						geometry = new THREE.PlaneGeometry(
							data.width,
							data.height,
							data.widthSegments,
							data.heightSegments
						);

						break;

					case 'BoxGeometry':
					case 'CubeGeometry': // DEPRECATED

						geometry = new THREE.BoxGeometry(
							data.width,
							data.height,
							data.depth,
							data.widthSegments,
							data.heightSegments,
							data.depthSegments
						);

						break;

					case 'CalloutGeometry':

						geometry = new THREEx.CalloutGeometry({
							width	: data.width,
							height	: data.height,
							radius	: data.radius,

							arrowHeight	: data.arrowHeight,
							arrowXRight	: data.arrowXRight,
							arrowXCenter	: data.arrowXCenter,
							arrowXLeft	: data.arrowXLeft,

							amount		: data.amount,
							bevelThickness	: data.bevelThickness,
							bevelSize	: data.bevelSize,
						})

						break;

					case 'CircleGeometry':

						geometry = new THREE.CircleGeometry(
							data.radius,
							data.segments
						);

						break;

					case 'CylinderGeometry':

						geometry = new THREE.CylinderGeometry(
							data.radiusTop,
							data.radiusBottom,
							data.height,
							data.radialSegments,
							data.heightSegments,
							data.openEnded
						);

						break;

					case 'SphereGeometry':

						geometry = new THREE.SphereGeometry(
							data.radius,
							data.widthSegments,
							data.heightSegments,
							data.phiStart,
							data.phiLength,
							data.thetaStart,
							data.thetaLength
						);

						break;

					case 'IcosahedronGeometry':

						geometry = new THREE.IcosahedronGeometry(
							data.radius,
							data.detail
						);

						break;

					case 'TorusGeometry':

						geometry = new THREE.TorusGeometry(
							data.radius,
							data.tube,
							data.radialSegments,
							data.tubularSegments,
							data.arc
						);

						break;

					case 'TorusKnotGeometry':

						geometry = new THREE.TorusKnotGeometry(
							data.radius,
							data.tube,
							data.radialSegments,
							data.tubularSegments,
							data.p,
							data.q,
							data.heightScale
						);

						break;

					case 'TextGeometry':

						geometry = new THREEx.TextGeometry(
							data.text,
							{
								font	: data.font,
								weight	: data.weight,
								size	: data.size,
								height	: data.height,
								bevelEnabled	: data.bevelEnabled,
								bevelThickness	: data.bevelThickness,
								bevelSize	: data.bevelSize,
								bevelSegments	: data.bevelSegments,
								curveSegments	: data.curveSegments
							}
						);

						break;

					case 'BufferGeometry':

						geometry = bufferGeometryLoader.parse( data.data );

						break;

					case 'Geometry':

						if( data.data !== undefined ) {
							geometry = geometryLoader.parse( data.data ).geometry;
						} else {
							geometry = undefined;
						}

						break;
				}				

				if( geometry !== undefined) {
					geometry.uuid = data.uuid;
					if ( data.name !== undefined ) geometry.name = data.name;
				}

				geometries[ data.uuid ] = geometry;		
			}
		}

		return geometries;
	},

	parseTextures: function ( json, textures ) {

		if( textures === undefined ) {
			textures = {};
		}

		if ( json !== undefined ) {
			var loader = new THREE.TextureLoader();

			for ( var i = 0, l = json.length; i < l; i ++ ) {

				var data = json[ i ];
				var texture;
				
				if( textures[ data.uuid ] !== undefined ) {
					continue;
				}

				texture = loader.parse( data );

				texture.uuid = data.uuid;

				if ( data.name !== undefined ) texture.name = data.name;

				textures[ data.uuid ] = texture;

			}

		}

		return textures;
	},

	parseMaterials: function ( json, textures ) {

		var materials = {};

		if ( json !== undefined ) {

			var loader = new THREE.MaterialLoader();

			for ( var i = 0, l = json.length; i < l; i ++ ) {

				var data = json[ i ];
				var material = loader.parse( data, textures );

				material.uuid = data.uuid;

				if ( data.name !== undefined ) material.name = data.name;

				materials[ data.uuid ] = material;

			}

		}

		return materials;

	},

	parseObject: function () {

		var matrix = new THREE.Matrix4();

		return function ( data, geometries, materials, textures, videos ) {

			var object;

			switch ( data.type ) {

				case 'Scene':

					object = new THREE.Scene();

					break;

				case 'PerspectiveCamera':

					object = new THREE.PerspectiveCamera( data.fov, data.aspect, data.near, data.far );

					break;

				case 'OrthographicCamera':

					object = new THREE.OrthographicCamera( data.left, data.right, data.top, data.bottom, data.near, data.far );

					break;

				case 'AmbientLight':

					object = new THREE.AmbientLight( data.color );

					break;

				case 'DirectionalLight':

					object = new THREE.DirectionalLight( data.color, data.intensity );
					object.targetUUID = data.targetUUID

					break;

				case 'PointLight':

					object = new THREE.PointLight( data.color, data.intensity, data.distance );

					break;

				case 'SpotLight':

					object = new THREE.SpotLight( data.color, data.intensity, data.distance, data.angle, data.exponent );
					object.targetUUID = data.targetUUID

					break;

				case 'HemisphereLight':

					object = new THREE.HemisphereLight( data.color, data.groundColor, data.intensity );

					break;

				case 'ArrowObject':

					object = new THREEx.ArrowObject()
					object.parameters.color.setHex( data.color )
					object.parameters.direction.fromArray( data.direction );
					['length', 'axisRadius', 'hasHeadOrigin', 'hasHeadTarget', 'headLength', 'headRadius'].forEach(function(key){
						object.parameters[key]	= data[key]
					})
					object.updateGeometry()
					break;

				case 'BrushPlane':


					var geometry = geometries[ data.geometry ];
					var material = materials[ data.material ];
					object = new THREEx.BrushPlane( geometry, material )
					// jme- if any of the children is selected, select mesh instead
					object.children.forEach(function(child){
						child.userData.selectableUUID	= object.uuid
					})

					break;

				case 'Callout':


					var geometry = geometries[ data.geometry ];
					var material = materials[ data.material ];
					object 		= new THREEx.Callout( geometry, material )
					object.text	= data.text
					object.textNeedsUpdate	= true
					object.update()

					// make children unselectable
					object.children.forEach(function(child){
						child.userData.selectableUUID	= data.uuid
					})

					break;

				case 'VideoObject':

					object		= new THREEx.VideoObject()
					object.fromJson ( data );
					if (!object.video) {
						object.video = {}
					}
					var rawVideo
					if( videos ) {
						rawVideo = videos[data.uuid]
					}
					if( rawVideo ) {
						object.load (rawVideo.src)
					}
					break;

				case 'GalleryObject':

					object = new THREEx.GalleryObject( data.height );
					object.fromJson ( data, textures );
					break;

				case 'ImageObject':



					object		= new THREEx.ImageObject()
					object.load(data.url)

					break;


				case 'HtmlMultipleMixerPlane':
					;(function(){
						var multipleMixerContext= editor.multipleMixerContext
						// create the domElement
						var initialUrl		= data.url
						var actualUrl		= THREEx.HtmlMixer.convertToViewerUrl(initialUrl)
						var iframeEl		= THREEx.HtmlMixerHelpers.createIframeDomElement(actualUrl)
						// create the multipleMixerPlane for it
						var multipleMixerPlane		= new THREEx.HtmlMultipleMixer.Plane(multipleMixerContext, iframeEl)
						object				= multipleMixerPlane.object3d
						object.multipleMixerPlane	= multipleMixerPlane
						multipleMixerPlane.planes.forEach(function(htmlMixerPlane){
							htmlMixerPlane.initialUrl	= initialUrl
						})


						// save html content if it is suitable
						if( data.url === 'about:blank' ){
							console.assert( data.htmlContent )
							var content	=  THREEx.HtmlMultipleMixer.defaultPrefix
							content		+= data.htmlContent
							content		+= THREEx.HtmlMultipleMixer.defaultSuffix 
							object.multipleMixerPlane.planes.forEach(function(mixerPlane){
								var iframeEl	= mixerPlane.domElement
								iframeEl.src	= 'about:blank'
								iframeEl.addEventListener('load', function onLoad(){
									var iframeDoc	= iframeEl.contentWindow.document
									iframeDoc.open()
									iframeDoc.write(content)
									iframeDoc.close()
									// console.log('loaded', iframeDoc.body.innerHTML )
									iframeEl.removeEventListener('load', onLoad)
								})
							})
						}
					})()					
					break;



				case 'Mesh':

					var geometry = geometries[ data.geometry ];
					var material = materials[ data.material ];

					if ( geometry === undefined ) {

						console.error( 'THREE.ObjectLoader: Undefined geometry ' + data.geometry );

					}

					if ( material === undefined ) {

						console.error( 'THREE.ObjectLoader: Undefined material ' + data.material );

					}

					if( geometry.morphTargets.length > 0 ){
						// jme- to support import of morph animation
						material.morphTargets	= true
						object = new THREE.MorphAnimMesh( geometry, material );
					}else{
						object = new THREE.Mesh( geometry, material );
					}


					break;

				case 'Sprite':

					var material = materials[ data.material ];

					if ( material === undefined ) {

						console.error( 'THREE.ObjectLoader: Undefined material ' + data.material );

					}

					object = new THREE.Sprite( material );

					break;

				default:

					object = new THREE.Object3D();

			}

			object.uuid = data.uuid;

			if ( data.name !== undefined ) object.name = data.name;
			if ( data.matrix !== undefined ) {

				matrix.fromArray( data.matrix );
				matrix.decompose( object.position, object.quaternion, object.scale );

			} else {

				if ( data.position !== undefined ) object.position.fromArray( data.position );
				if ( data.rotation !== undefined ) object.rotation.fromArray( data.rotation );
				if ( data.scale !== undefined ) object.scale.fromArray( data.scale );

			}

			if ( data.visible !== undefined ) object.visible = data.visible;
			if ( data.userData !== undefined ) object.userData = data.userData;


			// handle threex.posrotscaanimation
			if ( data.posrotscaAnimation ){
				object.posrotscaAnimation	= THREEx.PosrotscaAnimation.fromJson(object, data.posrotscaAnimation)
				object.posrotscaAnimation.set(object, 0)
			}

			if ( data.touchParams ) {
				object.touchParams = THREEx.TouchParams.fromJson( data.touchParams );
			}


			if ( data.children !== undefined ) {

				for ( var child in data.children ) {

					object.add( this.parseObject( data.children[ child ], geometries, materials, textures, videos ) );

				}
			}

			return object;

		}

	}()

};