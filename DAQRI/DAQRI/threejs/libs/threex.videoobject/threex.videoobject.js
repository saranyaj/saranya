var THREEx	= THREEx	|| {}


THREEx.VideoObject	= function(){

	this.status = '';
	this.fileID = undefined;
	this.fileName = null;

	this.startOptions = {
		option: 'AutoPlay'
	};

	this.touchOptions = {
		option: 'None',
		redirectType: '',
		redirectChapter: -1,
		redirectURL: ''
	};

	this.endOptions = {
		option: 'None',
		redirectType: '',
		redirectChapter: -1,
		redirectURL: ''
	};

	this.chromaSettings = {
		enable: false,
		color_hex: '#000000',
		threshold: 0.0,
		smoothness: 0.0
	};

	// build video element
	var video	= document.createElement('video');
	video.autoplay	= false
	video.autoReplay= true

	video.volume	= 0.2	// mute the video during developement

	// export video element
	this.video	= video

	// create the texture
	var texture	= new THREE.Texture( video );
	this.texture	= texture

	// build plane
	var geometry	= new THREE.PlaneGeometry(1, 1)
	var material	= new THREE.MeshBasicMaterial({
		side	: THREE.DoubleSide,
		map	: texture,
	})
	// call the inehrited contructor
	THREE.Mesh.call(this, geometry, material)


	//add an listener on loaded metadata
	video.addEventListener('loadeddata', function(){
		console.log('loadeddata')

		var object	= this

		delete object.__webglInit; // TODO: Remove hack (WebGLRenderer refactoring)

		object.geometry.dispose();

		// compute aspect 
		var videoW	= video.videoWidth
		var videoH	= video.videoHeight
		var aspect	= videoW/videoH

		// build plane
		var geometry	= new THREE.PlaneGeometry(aspect, 1)
		this.geometry	= geometry

		video.pause();
		object.status = 'loaded';
		if (object.onLoadedCallback) {
			object.onLoadedCallback();
		}

	}.bind(this), false);


	this.update	= function(){
		// trick to work around bugs in the video loop
		if( this.video.currentTime === this.video.duration ){
			this.video.src = this.video.src	
		}

		if( this.texture && this.video.readyState === this.video.HAVE_ENOUGH_DATA ){
			this.texture.needsUpdate	= true;
		}
	}


	this.load	= function(url){
		this.status = 'loading'
		video.crossOrigin = 'anonymous';
		video.src	= url;
	}


	this.destroy	= function(){
		video.pause()
	}

	this.toJson = function ( data ) {

		data.url	= this.video.src;
		data.volume	= this.video.volume;
		data.fileName = this.fileName;
		data.fileID = this.fileID;
		
		// These are used internally by the Unity video playback behavior
		data.identifier = this.video.identifier;
		data.materialForVideoIndex = this.video.materialForVideoIndex;
		data.useDefaultShader = true;
		data.autoReplay = this.video.autoReplay;

		data.startOptions = {
			option: this.startOptions.option
		};

		data.touchOptions = {
			option: this.touchOptions.option,
			redirectType: this.touchOptions.redirectType,
			redirectChapter: this.touchOptions.redirectChapter,
			redirectURL: this.touchOptions.redirectURL
		};

		data.endOptions = {
			option: this.endOptions.option,
			redirectType: this.endOptions.redirectType,
			redirectChapter: this.endOptions.redirectChapter,
			redirectURL: this.endOptions.redirectURL				
		};

		data.chromaSettings = {
			enable: this.chromaSettings.enable,
			color_hex: this.chromaSettings.color_hex,
			threshold: this.chromaSettings.threshold,
			smoothness: this.chromaSettings.smoothness
		};
	}

	this.fromJson = function ( data ) {

		this.video.volume	= data.volume;
		this.fileID = data.fileID;
		this.fileName = data.fileName;
		
		this.video.identifier = data.identifier;
		this.video.materialForVideoIndex = data.materialForVideoIndex;
		this.video.useDefaultShader = data.useDefaultShader;
		this.video.autoReplay = data.autoReplay;


		if ( data.startOptions ) {
			this.startOptions.option = data.startOptions.option;
		}

		if ( data.touchOptions ) {
			this.touchOptions.option = data.touchOptions.option;
			this.touchOptions.redirectType = data.touchOptions.redirectType;
			this.touchOptions.redirectChapter = data.touchOptions.redirectChapter;
			this.touchOptions.redirectURL = data.touchOptions.redirectURL;
		}

		if ( data.endOptions ) {
			this.endOptions.option = data.endOptions.option;
			this.endOptions.redirectType = data.endOptions.redirectType;
			this.endOptions.redirectChapter = data.endOptions.redirectChapter;
			this.endOptions.redirectURL = data.endOptions.redirectURL;
		}

		if ( data.chromaSettings ) {
			this.chromaSettings.enable = data.chromaSettings.enable;
			this.chromaSettings.color_hex = data.chromaSettings.color_hex;
			this.chromaSettings.threshold = data.chromaSettings.threshold;
			this.chromaSettings.smoothness = data.chromaSettings.smoothness;
		}
	}

}


THREEx.VideoObject.prototype = Object.create( THREE.Mesh.prototype )


