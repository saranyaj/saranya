var THREEx	= THREEx	|| {}

/**
 * @author WestLangley / http://github.com/WestLangley
 * @author zz85 / http://github.com/zz85
 * @author bhouston / http://exocortex.com
 * @author jeromeetienne / http://jetienne.com
 *
 * Creates an arrow for visualizing directions
 *
 * Parameters:
 *  dir - Vector3
 *  origin - Vector3
 *  length - Number
 *  color - color in hex value
 *  headLength - Number
 *  headRadius - Number
 */
THREEx.ArrowObject = function ( dir, origin, length, color, axisRadius, hasHead2, hasHead1, headLength, headRadius ) {
	// dir is assumed to be normalized

	// handle default paramters
	if( dir === undefined )			dir		= new THREE.Vector3(1,0,0)
	if( color === undefined )		color		= 'white';
	if( length === undefined )		length		= 1;
	if( axisRadius === undefined )		axisRadius	= 0.05;
	if( hasHead1 === undefined )	hasHead1	= true;
	if( hasHead2 === undefined )	hasHead2	= true;
	if( headLength === undefined )		headLength	= 0.2;
	if( headRadius === undefined )		headRadius	= 0.1;

	// copy parameters
	this.parameters	= {
		color		: new THREE.Color(color),
		direction	: dir.normalize(),
		length		: length,

		axisRadius	: axisRadius,

		hasHead1	: hasHead1,
		hasHead2	: hasHead2,

		headLength	: headLength,
		headRadius	: headRadius,
	}



	var geometry	= this._computeGeometry()
	var material	= new THREE.MeshPhongMaterial({
		color	: this.parameters.color
	})

	THREE.Mesh.call( this, geometry, material );

	this.parameters.color	= material.color
}

THREEx.ArrowObject.prototype = Object.create( THREE.Mesh.prototype );


//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.ArrowObject.prototype._computeGeometry	= function(){
	var params	= this.parameters
	var geometry	= new THREE.Geometry()

	// compute axisLength
	var axisLength	= params.length 
				- ( params.hasHead1 ? params.headLength : 0 )
				- ( params.hasHead2 ? params.headLength : 0 )

	// handle axis
	var axisGeometry	= new THREE.CylinderGeometry(params.axisRadius, params.axisRadius, axisLength);
	geometry.merge(axisGeometry)

	// handle head1
	if( params.hasHead1 ){
		var head1Geometry = new THREE.CylinderGeometry( 0, params.headRadius, params.headLength);
		head1Geometry.applyMatrix( new THREE.Matrix4().makeRotationZ( Math.PI ) );
		head1Geometry.applyMatrix( new THREE.Matrix4().makeTranslation( 0, -params.headLength/2, 0 ) );
		head1Geometry.applyMatrix( new THREE.Matrix4().makeTranslation( 0, -axisLength/2, 0 ) );
		geometry.merge(head1Geometry)
	}

	// handle head2
	if( params.hasHead2 ){
		var head2Geometry = new THREE.CylinderGeometry( 0, params.headRadius, params.headLength);
		head2Geometry.applyMatrix( new THREE.Matrix4().makeTranslation( 0, +params.headLength/2, 0 ) );
		head2Geometry.applyMatrix( new THREE.Matrix4().makeTranslation( 0, +axisLength/2, 0 ) );
		geometry.merge(head2Geometry)
	}

	// recenter the geometry depending on params.hasHead1 and params.hasHead2
	var deltaY	= ( params.hasHead1 ? +params.headLength/2 : 0 )
				+ ( params.hasHead2 ? -params.headLength/2 : 0 )
	geometry.applyMatrix( new THREE.Matrix4().makeTranslation( 0, deltaY, 0 ) );

	//////////////////////////////////////////////////////////////////////////////////
	//		Make rotation with direction								//
	//////////////////////////////////////////////////////////////////////////////////

	;(function(){
		var dir		= this.parameters.direction.normalize();
		var quaternion	= new THREE.Quaternion()

		// dir is assumed to be normalized
		if ( dir.y > 0.99999 ) {
			quaternion.set( 0, 0, 0, 1 );
		} else if ( dir.y < - 0.99999 ) {
			quaternion.set( 1, 0, 0, 0 );
		} else {
			var axis	= new THREE.Vector3();
			axis.set( dir.z, 0, -dir.x ).normalize();

			var radians = Math.acos( dir.y );
			quaternion.setFromAxisAngle( axis, radians );
		}

		geometry.applyMatrix( new THREE.Matrix4().makeRotationFromQuaternion(quaternion) )
	}.bind(this))()



	//////////////////////////////////////////////////////////////////////////////////
	//		Comment								//
	//////////////////////////////////////////////////////////////////////////////////

	// return the merge geometry
	return geometry
}

THREEx.ArrowObject.prototype.updateGeometry = function(){
	delete this.__webglInit; // TODO: Remove hack (WebGLRenderer refactoring)

	this.geometry.dispose();

	this.geometry	= this._computeGeometry();
}

//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.ArrowObject.prototype.setLength = function ( length ) {
	this.parameters.length	= length
	this.updateGeometry()
}

THREEx.ArrowObject.prototype.setDirection = function(direction) {
	this.parameters.direction	= direction
	this.updateGeometry()
}





