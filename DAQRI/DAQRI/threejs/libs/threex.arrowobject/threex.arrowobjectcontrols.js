var THREEx	= THREEx	|| {}

/**
 * THREEx.ArrowObjectControls with a .target as THREE.Object3D
 */
THREEx.ArrowObjectControls	= function(arrowObject){

	THREE.Object3D.call( this );

	this.target	= new THREE.Object3D()

	this.setFromObject	= function(){
		var params	= arrowObject.parameters
		var delta	= params.direction.clone().multiplyScalar(params.length/2)
		this.position.copy(arrowObject.position).sub(delta)
		this.target.position.copy(arrowObject.position).add(delta)		
	}

	this.setFromObject();

	this.update	= function(){
		var delta	= this.target.position.clone()
			.sub(this.position)
			.divide(this.scale)

		var length	= delta.length()
		arrowObject.setLength(length)

		var direction	= delta.normalize()
		arrowObject.setDirection(direction)

		var center	= new THREE.Vector3()
		center.copy(this.position).add(this.target.position).multiplyScalar(1/2)
		arrowObject.position.copy(center)
	}
}

THREEx.ArrowObjectControls.prototype = Object.create( THREE.Object3D.prototype );
