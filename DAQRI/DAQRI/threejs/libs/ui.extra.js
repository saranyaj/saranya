/**
 * @fileOverview Extra stuff added to ui.js 
 * @author Jerome Etienne jerome.etienne@gmail.com
 */

//////////////////////////////////////////////////////////////////////////////////
//		UI.HorizontalRule						//
//////////////////////////////////////////////////////////////////////////////////
// HorizontalRule

UI.Icon = function (url) {

	UI.Element.call( this );

	var dom		= document.createElement( 'img' );
	dom.className	= 'Icon';
	dom.src		= url;

	this.dom	= dom;

	return this;

};

UI.Icon.prototype = Object.create( UI.Element.prototype );