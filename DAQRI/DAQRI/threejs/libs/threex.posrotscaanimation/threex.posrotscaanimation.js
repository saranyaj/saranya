var THREEx	= THREEx	|| {}


THREEx.PosrotscaAnimation	= function(object3d){
	this._steps		= []
	this.mode		= 'idle'	// 'playing' 'recording' or 'idle'
	this.stepDuration	= 1
	this.loop		= true
	this.pingpong		= false
	this.object3d		= object3d !== undefined ? object3d : null
}

//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.PosrotscaAnimation.prototype.nSteps = function() {
	return this._steps.length
};

THREEx.PosrotscaAnimation.prototype.isPlayable = function() {
	return this._steps.length >= 2 ? true : false
};

THREEx.PosrotscaAnimation.prototype.reset = function() {
	this._steps	= []
}

THREEx.PosrotscaAnimation.prototype.set = function(object3d, progress){
	console.assert(progress >= 0 && progress <= 1.0)
	console.assert(this._steps.length >= 2)

	// handle special case of progress === 1.0
	if( progress === 1.0 ){
		var posrotscaTmp= this._steps[this._steps.length-1]
		var posrotsca	= new THREEx.Posrotsca(object3d)
		posrotsca.lerpVector(posrotscaTmp, posrotscaTmp, 0.0)
		return
	}

	// compute keyIdx and alpha
	var floatIdx	= progress * (this._steps.length-1)
	var keyIdx	= Math.floor(floatIdx)
	console.assert( keyIdx < this._steps.length-1 )
	var alpha	= floatIdx - keyIdx

	// set the object3d position
	var posrotsca	= new THREEx.Posrotsca(object3d)
	var posrotscaSrc= this._steps[keyIdx]
	var posrotscaDst= this._steps[keyIdx+1]

	posrotsca.lerpVector(posrotscaSrc, posrotscaDst, alpha)
}


//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.PosrotscaAnimation.prototype.toJson = function() {
	var output	= {
		stepDuration	: this.stepDuration,
		loop		: this.loop,
		pingpong	: this.pingpong,
		steps		: [],
	}
	this._steps.forEach(function(posrotsca){
		output.steps.push(posrotsca.toJson())
	})
	return output
}

THREEx.PosrotscaAnimation.fromJson = function(object3d, json) {
	var posrotscaAnimation	= new THREEx.PosrotscaAnimation(object3d)
	posrotscaAnimation.stepDuration	= json.stepDuration
	posrotscaAnimation.loop		= json.loop
	posrotscaAnimation.pingpong	= json.pingpong
	posrotscaAnimation.record()
	json.steps.forEach(function(jsonItem){
		var posrotsca	= THREEx.Posrotsca.fromJson(jsonItem)
		posrotscaAnimation.addStep(posrotsca)
	})
	posrotscaAnimation.stopRecording()
	return posrotscaAnimation
}

//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.PosrotscaAnimation.prototype.duration = function() {
	return this._steps.length * this.stepDuration
}

THREEx.PosrotscaAnimation.prototype.isRecording = function() {
	return this.mode === 'recording'
}

THREEx.PosrotscaAnimation.prototype.isPlaying = function() {
	return this.mode === 'playing'
}

THREEx.PosrotscaAnimation.prototype.isIdle = function() {
	return this.mode === 'idle'
}

//////////////////////////////////////////////////////////////////////////////////
//		Recoding							//
//////////////////////////////////////////////////////////////////////////////////

THREEx.PosrotscaAnimation.prototype.record = function() {
	console.assert( this.mode === 'idle' )
	
	this.mode	= 'recording'
	this.reset()
}

THREEx.PosrotscaAnimation.prototype.addStep = function(posrotsca) {
	console.assert(posrotsca instanceof THREEx.Posrotsca)
	console.assert(this.mode === 'recording')

	this._steps.push(posrotsca)
};


THREEx.PosrotscaAnimation.prototype.stopRecording = function() {
	console.assert(this.mode === 'recording')

	this.mode	= 'idle'
};


//////////////////////////////////////////////////////////////////////////////////
//		Playing								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.PosrotscaAnimation.prototype.play = function() {
	console.assert( this.mode === 'idle' )
	console.assert( this.isPlayable() === true )
	this.mode	= 'playing'
	this.startTime	= Date.now()/1000
};

THREEx.PosrotscaAnimation.prototype.stopPlaying = function() {
	console.assert(this.mode === 'playing')

	this.mode	= 'idle'
};

THREEx.PosrotscaAnimation.prototype.update = function() {
	if( this.mode !== 'playing' )	return

	// compute time since playing
	var currentTime	= Date.now()/1000
	var deltaTime	= currentTime - this.startTime

	// compute animation progress based on deltaTime
	var progress	= deltaTime / this.duration()
	if( progress > 1.0 ){
		if( this.loop === false ){
			var progress	= 1.0
		}else if( this.pingpong === false ){
			var progress	= progress % 1.0	
		}else{
			// handle the loop+pingpong case
			if( Math.floor(progress) % 2 === 1 ){
				progress	= 1 - (progress % 1.0)
			}else{
				var progress	= progress % 1.0
			}
		}
	}

	// apply animation to the object itself
	this.set(this.object3d, progress)
};