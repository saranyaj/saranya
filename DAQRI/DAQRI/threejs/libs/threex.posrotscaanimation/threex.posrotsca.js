var THREEx	= THREEx	|| {}

THREEx.Posrotsca	= function(parameters){
	parameters	= parameters	|| {}
	this.position	= parameters.position !== undefined ? parameters.position : new THREE.Vector3(0,0,0)
	this.scale	= parameters.scale !== undefined ? parameters.scale : new THREE.Vector3(1,1,1)
	this.quaternion	= parameters.quaternion !== undefined ? parameters.quaternion : new THREE.Quaternion()
}
//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.Posrotsca.fromObject3d	= function(object3d){
	var parameters	= {
		position	: object3d.position.clone(),	
		quaternion	: object3d.quaternion.clone(),
		scale		: object3d.scale.clone(),
	}
	return new THREEx.Posrotsca(parameters)
}


THREEx.Posrotsca.prototype.toJson	= function(){
	var output	= {
		position	: this.position.toArray(),	
		quaternion	: this.quaternion.toArray(),
		scale		: this.scale.toArray(),
	}
	return output
}

THREEx.Posrotsca.fromJson	= function( json ){
	var parameters	= {
		position	: new THREE.Vector3().fromArray(json.position),	
		quaternion	: new THREE.Quaternion().fromArray(json.quaternion),
		scale		: new THREE.Vector3().fromArray(json.scale),
	}
	return new THREEx.Posrotsca(parameters)
}

//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////
THREEx.Posrotsca.prototype.lerpVector	= function(a, b, alpha){
	this.position.copy(a.position).lerp(b.position, alpha)
	this.quaternion.copy(a.quaternion).slerp(b.quaternion, alpha)
	this.scale.copy(a.scale).lerp(b.scale, alpha)
}

THREEx.Posrotsca.prototype.copy	= function(other){
	this.position.copy(other.position)
	this.quaternion.copy(other.quaternion)
	this.scale.copy(other.scale)
}

THREEx.Posrotsca.prototype.clone	= function(){
	return new THREEx.Posrotsca({
		position	: this.position.clone(),
		quaternion	: this.quaternion.clone(),
		scale		: this.scale.clone(),
	})
}


