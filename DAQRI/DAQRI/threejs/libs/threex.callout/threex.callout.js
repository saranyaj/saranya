var THREEx	= THREEx	|| {}

THREEx.Callout	= function(geometry, material){
	// call the inehrited contructor
	THREE.Mesh.call(this, geometry, material)

	this.text	= 'Hello World'
	this.textNeedsUpdate	= true

	this.dynamicTexture	= null
	this.computeDynamicTexture	= function(){
/**
 * * compute aspect based on geometry parameters
 * * keep the same aspect in dynamicTexture
 */

		// remove all children
	 	this.children.slice(0).forEach(function(child){
	 		this.remove(child)
	 	}.bind(this))

		this.dynamicTexture	= new THREEx.DynamicTexture(512,512)
		this.dynamicTexture.context.font	= "bolder 70px Verdana";

		// FIXME: it goes beyond the callout geometry. check corners
		var planeGeometry	= new THREE.PlaneGeometry(geometry.parameters.width,geometry.parameters.height)
		var planeMaterial	= new THREE.MeshPhongMaterial({
			map		: this.dynamicTexture.texture,
			transparent	: true,
		})
		var planeMesh		= new THREE.Mesh(planeGeometry, planeMaterial)
		planeMesh.position.z	= geometry.parameters.amount + geometry.parameters.bevelThickness + 0.1
		this.add( planeMesh )

		planeMesh.userData.selectableUUID	= this.uuid

	 	// draw the text on the new dynamicTexture
		this.textNeedsUpdate = true
	 	this.update()
	}.bind(this)

	this.update	= function(){
		if( this.textNeedsUpdate === false )	return
		this.textNeedsUpdate	= false

		// clear texture	
		this.dynamicTexture.clear()
		// update the text
		// TODO make this 0.15 tunable ... how ? in the threex.dynamictexture
		this.dynamicTexture.context.font	= "bold "+(0.15*this.dynamicTexture.canvas.width)+"px Arial";
		this.dynamicTexture.drawTextCooked(this.text, {
			lineHeight	: 0.15,
		})
	}

	this.computeDynamicTexture()
	this.update()
}


THREEx.Callout.prototype = Object.create( THREE.Mesh.prototype );