var THREEx	= THREEx	|| {}

THREEx.BrushPlane	= function(geometry, material){
	// call the inehrited contructor
	THREE.Mesh.call(this, geometry, material);
	this.parameters	= {
		color	: new THREE.Color('white'),
		size	: 64,
	};
}


THREEx.BrushPlane.prototype = Object.create( THREE.Mesh.prototype );

THREEx.BrushPlane.fromCamera	= function(camera){

	//////////////////////////////////////////////////////////////////////////////////
	//		compute plane size to fit viewport size				//
	//////////////////////////////////////////////////////////////////////////////////
	var yfovinradians	= camera.fov * Math.PI / 180
// TODO shouldnt it be the distance to target
// - but target isnt known to camera three.js
// - maybe to put it as optional parameter of the ctor with camera.position.z as default ?
	var distanceToPlane	= camera.position.z
	var height		= 2.0 * distanceToPlane * Math.tan(0.5 * yfovinradians)
	var width		= height * camera.aspect

	//////////////////////////////////////////////////////////////////////////////////
	//		texture creation						//
	//////////////////////////////////////////////////////////////////////////////////
	var canvas	= document.createElement( 'canvas' );
	canvas.width	= 512
	canvas.height	= canvas.width * height/width
	var context	= canvas.getContext( '2d' );
	var texture	= new THREE.Texture(canvas)
	texture.needsUpdate	= true

	//////////////////////////////////////////////////////////////////////////////////
	//		plane creation							//
	//////////////////////////////////////////////////////////////////////////////////
	var geometry	= new THREE.PlaneGeometry(width, height)
	var material	= new THREE.MeshBasicMaterial({
		// color		: 'green',
		map		: texture,
		side		: THREE.DoubleSide,
		transparent	: true,
	})

	//////////////////////////////////////////////////////////////////////////////////
	//		Comment								//
	//////////////////////////////////////////////////////////////////////////////////
	var brushPlane	= new THREEx.BrushPlane(geometry, material)

	brushPlane.lookAt(camera.position)

	return brushPlane
}
