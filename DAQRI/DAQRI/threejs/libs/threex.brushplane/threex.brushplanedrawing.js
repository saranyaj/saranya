var THREEx	= THREEx	|| {}

THREEx.BrushPlaneDrawing	= function(brushPlane, parameters, editor){
	var texture	= brushPlane.material.map

	if(texture.image instanceof HTMLCanvasElement){
		var canvas	= texture.image
	}else{
		console.assert( texture.image.width > 0 )
		console.assert( texture.image.height > 0 )
		var canvas	= document.createElement('canvas')
		canvas.width	= texture.image.width
		canvas.height	= texture.image.height
		var context	= canvas.getContext( '2d' )
		context.drawImage(texture.image, 0, 0)
		texture.image	= canvas
	}

	var context	= canvas.getContext( '2d' )

	//////////////////////////////////////////////////////////////////////////////////
	//		load brush							//
	//////////////////////////////////////////////////////////////////////////////////
	var brushImage	= null
	var color	= new THREE.Color('white')

	this.parameters	= parameters

	this.buildBrush	= function(){
		// create canvas
		var canvas	= document.createElement( 'canvas' );
		canvas.width	= this.parameters.size;
		canvas.height	= this.parameters.size;
		var context	= canvas.getContext( '2d' );
		// create gradient
		var color	= this.parameters.color
		var gradient	= context.createRadialGradient( canvas.width / 2, canvas.height / 2, 0, canvas.width / 2, canvas.height / 2, canvas.width / 2 );
		gradient.addColorStop( 0.1, color.getStyle() );
		gradient.addColorStop( 1.0, 'rgba('+color.r*255+','+color.g*255+','+color.b*255+',0)' );
		// actually fill the canvas
		context.fillStyle	= gradient;
		context.fillRect( 0, 0, canvas.width, canvas.height );
		// update brushImage
		brushImage	= canvas
	}
	this.buildBrush()

	//////////////////////////////////////////////////////////////////////////////////
	//		Draw function							//
	//////////////////////////////////////////////////////////////////////////////////
	this.drawUv	= function(uCoord, vCoord){
		// compute canvas x,y from uv
		var x	= (  uCoord) * canvas.width
		var y	= (1-vCoord) * canvas.height

		// draw bush
		context.save();
		context.globalAlpha = 0.5;
		context.translate(-brushImage.width/2,-brushImage.height/2);
		context.drawImage(brushImage, x, y);
		context.restore();

		// update texture
		texture.needsUpdate	= true

		editor.signals.sceneGraphChanged.dispatch();
	}

	this.clear	= function(){
		context.clearRect(0,0,canvas.width, canvas.height);

		// context.fillStyle = "rgba(255,255,255,0.5)";
		// context.fillRect(0,0,canvas.width, canvas.height);

		texture.needsUpdate	= true
	}

	//////////////////////////////////////////////////////////////////////////////////
	//		projection							//
	//////////////////////////////////////////////////////////////////////////////////

	var drawing	= false
	var renderer	= editor.viewport.renderer
	var domElement	= renderer.domElement
	// domElement.addEventListener('mousedown', onMouseDown)
	// domElement.addEventListener('mouseup', onMouseUp)

	// WARNING WARNING WARNING WARNING WARNING WARNING 
	// adding event listeners to the document will cause a potentially huge memory leak
	// these must be removed in editor.dispose()
	document.addEventListener('keydown', onKeyDown)
	document.addEventListener('keyup', onKeyUp)
	domElement.addEventListener('mousemove', onMouseMove)
console.log('event registering')

	//////////////////////////////////////////////////////////////////////////////////
	//		event handling							//
	//////////////////////////////////////////////////////////////////////////////////
	function onMouseDown(event){
		// if selected object isnt a THREEx.BrushPlane, do nothing
		if( editor.selected !== brushPlane )	return
		drawing	= true
		// draw at the mouse position
		onMouseEventDraw(event)		
	}
	function onMouseUp(event){
		// if selected object isnt our THREEx.BrushPlane, do nothing
		if( editor.selected !== brushPlane )	return
		drawing	= false
	}	
	function onMouseMove(event){
		// if selected object isnt our THREEx.BrushPlane, do nothing
		if( editor.selected !== brushPlane )	return
		// if not drawing, return now
		if( drawing === false )	return;
		// draw at the mouse position
		onMouseEventDraw(event)
	}

	//////////////////////////////////////////////////////////////////////////////////
	//	TEMPORARY TRICK to avoid handling dependancy in viewport controls	//
	//////////////////////////////////////////////////////////////////////////////////
	function onKeyDown(event){		
		console.log('down', window.event)
		if( event.altKey === true )	return
		// if selected object isnt a THREEx.BrushPlane, do nothing
		if( editor.selected !== brushPlane )	return
		drawing	= true
		// disabled because the mouseposition is unknown in a keyboardEvent
		// draw at the mouse position
		// onMouseEventDraw(event)
	}
	function onKeyUp(event){
		if( event.altKey === true )	return
		// if selected object isnt a THREEx.BrushPlane, do nothing
		if( editor.selected !== brushPlane )	return
		drawing	= false
	}

	//////////////////////////////////////////////////////////////////////////////////
	//		mouse coordinate projection					//
	//////////////////////////////////////////////////////////////////////////////////
	var projector	= new THREE.Projector();
	var onMouseEventDraw	= function(event){
		console.assert(drawing === true)
		// get mouse position in viewport - from threex.domevents
		var mouse	= _getRelativeMouseXY(event)

		// find the intersection location if any
		var vector	= new THREE.Vector3( mouse.x, mouse.y, 0.5 );
		var camera	= editor.viewport.camera
		var ray         = projector.pickingRay( vector, camera );
		var intersects  = ray.intersectObjects( [brushPlane] );
		if( intersects.length === 0 )	return
		var intersect	= intersects[0]
		
		// convert world coordinate into local UV 
		var worldCoord	= intersect.point.clone()
		var drawCoord	= brushPlane.worldToLocal(worldCoord)
		drawCoord.x	= 0.5 + (drawCoord.x / brushPlane.geometry.parameters.width )
		drawCoord.y	= 0.5 + (drawCoord.y / brushPlane.geometry.parameters.height)

		this.drawUv(drawCoord.x,drawCoord.y)
	}.bind(this)
	

	//////////////////////////////////////////////////////////////////////////////////
	//		destroy function						//
	//////////////////////////////////////////////////////////////////////////////////
	this.destroy	= function(){
		// remove all registered event listener
		// domElement.removeEventListener('mousedown', onMouseDown)
		// domElement.removeEventListener('mouseup', onMouseUp)
		document.removeEventListener('keydown', onKeyDown)
		document.removeEventListener('keyup', onKeyUp)
		domElement.removeEventListener('mousemove', onMouseMove)
	}


	//////////////////////////////////////////////////////////////////////////////////
	//		Comment								//
	//////////////////////////////////////////////////////////////////////////////////
	
	// take from threex.domevent
	function _getRelativeMouseXY(domEvent){
		var element = domEvent.target || domEvent.srcElement;
		if (element.nodeType === 3) {
			element = element.parentNode; // Safari fix -- see http://www.quirksmode.org/js/events_properties.html
		}
		
		//get the real position of an element relative to the page starting point (0, 0)
		//credits go to brainjam on answering http://stackoverflow.com/questions/5755312/getting-mouse-position-relative-to-content-area-of-an-element
		var elPosition	= { x : 0 , y : 0};
		var tmpElement	= element;
		//store padding
		var style	= getComputedStyle(tmpElement, null);
		elPosition.y += parseInt(style.getPropertyValue("padding-top"), 10);
		elPosition.x += parseInt(style.getPropertyValue("padding-left"), 10);
		//add positions
		do {
			elPosition.x	+= tmpElement.offsetLeft;
			elPosition.y	+= tmpElement.offsetTop;
			style		= getComputedStyle(tmpElement, null);

			elPosition.x	+= parseInt(style.getPropertyValue("border-left-width"), 10);
			elPosition.y	+= parseInt(style.getPropertyValue("border-top-width"), 10);
		} while(tmpElement = tmpElement.offsetParent);
		
		var elDimension	= {
			width	: (element === window) ? window.innerWidth	: element.offsetWidth,
			height	: (element === window) ? window.innerHeight	: element.offsetHeight
		};
		
		return {
			x : +((domEvent.pageX - elPosition.x) / elDimension.width ) * 2 - 1,
			y : -((domEvent.pageY - elPosition.y) / elDimension.height) * 2 + 1
		};
	};
}

THREEx.BrushPlaneDrawing.baseUrl	= '../'
