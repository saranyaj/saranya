var THREEx	= THREEx	|| {}


THREEx.ImageObject	= function(){

	// build image element
	var image	= document.createElement('img');
	this.image	= image
	// create the texture
	var texture	= new THREE.Texture( image );


	var geometry	= new THREE.PlaneGeometry(1, 1)
	var material	= new THREE.MeshBasicMaterial({
		side	: THREE.DoubleSide,
		map	: texture,
	})

	// call the inehrited contructor
	THREE.Mesh.call(this, geometry, material)

	//add an listener on loaded metadata
	image.addEventListener('load', function(){
		var object	= this

		delete object.__webglInit; // TODO: Remove hack (WebGLRenderer refactoring)

		object.geometry.dispose();

		// compute aspect 
		var aspect	= image.width/image.height
		var geometry	= new THREE.PlaneGeometry(aspect, 1)
		this.geometry	= geometry

		texture.needsUpdate	= true;

	}.bind(this), false);


	this.load	= function(url){
		image.src	= url;
	}
}


THREEx.ImageObject.prototype = Object.create( THREE.Mesh.prototype )


