var THREEx  = THREEx  || {}


THREEx.TouchParams  = function ( ) {

  this.buttonType = null
  this.redirectType = null;
  this.redirectChapter = -1;
  this.redirectURL = '';

}

THREEx.TouchParams.fromJson = function ( json ) {

  var touchParams = new THREEx.TouchParams( );

  touchParams.buttonType = json.buttonType;
  touchParams.redirectType = json.redirectType;
  touchParams.redirectChapter = json.redirectChapter;
  touchParams.redirectURL = json.redirectURL;

  return touchParams;
}


THREEx.TouchParams.prototype = {

  constructor: THREEx.TouchParams,

  toJson: function ( ) { 

    var json = {

      buttonType: this.buttonType,
      redirectType: this.redirectType,
      redirectChapter: this.redirectChapter,
      redirectURL: this.redirectURL

    };

    return json;
  }

}
