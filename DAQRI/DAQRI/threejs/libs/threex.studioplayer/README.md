# threex.studioplayer

It provides a player to studio.
It is able to 'play' the scene you edit in studio.

All the movies, the htmlmixer, the dynamic camera are all supposed to be handled
by the player, they are to be interactive.

This code is primarly intended to be run on mobile. so compatibility with
current mobile OS, IOS8 and Android, is a requirement.

## Show dont tell
* [examples/basic.html]() provide a basic example of the player studioplayer.
* [examples/behavior.html]() provide a example of the player studioplayer using
[threex.studiobehavior]()
* [examples/windowedPlayer2.html]() is a standalone player with all features, able to run
the scene saved by studio itself. It is the one which is the base to play on mobile.
