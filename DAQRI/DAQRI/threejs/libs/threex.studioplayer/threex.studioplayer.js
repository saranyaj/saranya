var THREEx	= THREEx	|| {}

THREEx.StudioPlayer	= function(){
	var player	= this

	player.signals = {
		onInit	: new SIGNALS.Signal(),
		onStart	: new SIGNALS.Signal(),
		onRender: new SIGNALS.Signal(),
		onStop	: new SIGNALS.Signal(),
	}

	player.isPaused	= true

	if (window.daqri.environment == 'test') {
		player.renderer = new THREE.CanvasRenderer();
	}else{		
		player.renderer	= new THREE.WebGLRenderer({
			antialias	: true,
			alpha		: true,
		});
	}

	// player.renderer.setClearColor(new THREE.Color('lightgrey'), 1)

	player.scene		= new THREE.Scene
	player.defaultScene	= new THREE.Scene

	player.camera	= new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.01, 1000);
	player.camera.position.z = 100;

	player.renderingEnabled	= true
	player.controlsEnabled	= true
}

THREEx.StudioPlayer.prototype.render = function() {
	var player	= this
	var renderer	= player.renderer
	var scene	= player.scene
	var camera	= player.camera

	if( player.renderingEnabled === false )	return

	player.signals.onRender.dispatch()

	// resize on each frame
	var boundingRect= renderer.domElement.getBoundingClientRect();
	renderer.setSize( boundingRect.width, boundingRect.height )
	camera.aspect	= boundingRect.width / boundingRect.height
	camera.updateProjectionMatrix()

	// actually render the scene
	player.renderer.render( player.scene, player.camera )
}

//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.StudioPlayer.prototype.setDefaultScene = function() {
	var exporter	= new THREE.ObjectExporter();
	var output	= exporter.parse( this.defaultScene );

	var loader	= new THREE.ObjectLoader();
	var storedScene	= loader.parse( output );

	this.setScene(storedScene)
}

THREEx.StudioPlayer.prototype.clearScene = function() {
	while( this.scene.children.length > 0 ){
		var firstChild	= this.scene.children[0]
		this.scene.remove(firstChild)
	}
}

THREEx.StudioPlayer.prototype.setScene = function(scene) {
	var runner	= this
	scene.children.slice(0).forEach(function(child){
		console.log('add', child)
		runner.scene.add(child)
	})
}

//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.StudioPlayer.prototype.isRunning = function() {
	return this.isPaused ? false : true
};
THREEx.StudioPlayer.prototype.start = function() {
	console.assert(this.isRunning() === false)
	this.isPaused	= false
	this.signals.onStart.dispatch()
};
THREEx.StudioPlayer.prototype.stop = function() {
	console.assert(this.isRunning() === true)
	this.isPaused	= true
	this.signals.onStop.dispatch()
};


THREEx.StudioPlayer.prototype.init = function() {
	this.signals.onInit.dispatch()
};
