THREEx.StudioPlayer.prototype.addBehaviorsComponent	= function(){
	var player		= this;

        var behaviorContext     = new THREEx.BehaviorContext()

	var renderer	= player.renderer
	var scene	= player.scene
	var camera	= player.camera
        behaviorContext.setScene(scene).setCamera(camera).setDomElement(renderer.domElement)
	behaviorContext.init()
        player.behaviorContext  = behaviorContext

        // honor behaviorObject.init() when player.init()
	player.signals.onInit.add(function(){
		var scene	= player.scene
		scene.traverse(function(object3d){
			var behaviorObject	= THREEx.BehaviorObject3d.fromObject3d(object3d)
			if( behaviorObject === null )	return
			behaviorObject.init()
		})
	})

        // honor behaviorObject.start() when player.start()
	player.signals.onStart.add(function(){
		var scene	= player.scene
		console.log("scene", player.scene);
		scene.traverse(function(object3d){
			var behaviorObject	= THREEx.BehaviorObject3d.fromObject3d(object3d)
			if( behaviorObject === null )	return
			behaviorObject.start()
		})
	})

        // honor behaviorObject.stop() when player.stop()
	player.signals.onStop.add(function(){
		var scene	= player.scene
		scene.traverse(function(object3d){
			var behaviorObject	= THREEx.BehaviorObject3d.fromObject3d(object3d)
			if( behaviorObject === null )	return
			behaviorObject.stop()
		})
	})
}
