var THREEx  = THREEx  || {}


THREEx.AudioObject  = function ( name, experienceID ) {

  THREE.Object3D.call( this );

  this.name = name;
  this.experienceID = experienceID;
  this.serialisable = false; // should not be written to scene file, because it is saved as an experience
}

THREEx.AudioObject.prototype = Object.create( THREE.Object3D.prototype );
