var THREEx	= THREEx	|| {}

THREEx.BehaviorObject3d	= function(){
	this.context	= null
	this.object3d	= null
	this.scripts	= []

	var SIGNALS	= signals;
	this.signals	= {
		click	: new SIGNALS.Signal(),
	}
}


THREEx.BehaviorObject3d.prototype.setContext = function(context) {
	console.assert( context instanceof THREEx.BehaviorContext )
	this.context	= context
	return this
}

//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////
THREEx.BehaviorObject3d.fromObject3d = function(object3d) {
	if( object3d.behaviors === undefined )	return null
	return object3d.behaviors
}

/**
 * Test if this object3d has behaviors
 */
THREEx.BehaviorObject3d.isInObject3d = function(object3d) {
	var behaviors	= THREEx.BehaviorObject3d.fromObject3d(object3d)
	var hasBehaviors= behaviors !== null ? true : false
	return hasBehaviors
}

THREEx.BehaviorObject3d.prototype.setObject3d = function(object3d) {
	console.assert( object3d instanceof THREE.Object3D )

	// detach it if needed
	this.detach()

	if( object3d === null )	return

	object3d.behaviors	= this

	// set the new one
	this.object3d	= object3d

	return this
}

THREEx.BehaviorObject3d.prototype.detach = function(){
	if( this.object3d === null )	return

	this.object3d.behaviors	= null
	this.object3d	= null
	
	return this;
}

//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.BehaviorObject3d.prototype.init = function() {
	console.log('init BehaviorObject3d')
	this.scripts.forEach(function(script){
		script.init()
	})
};

THREEx.BehaviorObject3d.prototype.start = function() {
	this.scripts.forEach(function(script){
		script.start()
	})
};


THREEx.BehaviorObject3d.prototype.stop = function() {
	this.scripts.forEach(function(script){
		script.stop()
	})
};


//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.BehaviorObject3d.prototype.findScriptByName = function(name){
	for(var i = 0; i < this.scripts.length; i++){
		var script	= this.scripts[i]
		if( script.name === name )	return script
	}
	return null
}


THREEx.BehaviorObject3d.prototype.findScriptByUUID = function(uuid){
	for(var i = 0; i < this.scripts.length; i++){
		var script	= this.scripts[i]
		if( script.uuid === uuid )	return script
	}
	return null
}

THREEx.BehaviorObject3d.prototype.addScript = function(script){
	this.scripts.push(script)
}

THREEx.BehaviorObject3d.prototype.removeScript = function(script){
	var index	= this.scripts.indexOf(script)
	if( index === -1 )	return
	this.scripts.splice(index, 1)
}
