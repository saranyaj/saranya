var THREEx	= THREEx	|| {}

THREEx.BehaviorObject3dExporter	= function(){}

THREEx.BehaviorObject3dExporter.prototype.parse = function(behaviors) {
	var output	= {}

	output.scripts	= []
	behaviors.scripts.forEach(function(script){
		var data	= {}
		data.name	= script.name
		data.type	= script.type
		if( script.parameters && script.parameters.length > 0 ){
			data.parameters	= JSON.parse(JSON.stringify(script.parameters))
		}

		if( script.listenedEvents ){
			data.listenedEvents	= script.listenedEvents.slice()
		}

		output.scripts.push(data)
	})


	return output
};
