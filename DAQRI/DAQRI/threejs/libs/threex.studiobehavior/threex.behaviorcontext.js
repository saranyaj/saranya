var THREEx	= THREEx	|| {}

THREEx.BehaviorContext	= function(){
	this.scene	= null
	this.camera	= null
	this.domElement	= null
}

/**
 * microevents.js - https://github.com/jeromeetienne/microevent.js
*/
// THREEx.BehaviorContext.MicroeventMixin	= function(destObj){
// 	destObj.addEventListener	= function(event, fct){
// 		if(this._events === undefined) 	this._events	= {};
// 		this._events[event] = this._events[event]	|| [];
// 		this._events[event].push(fct);
// 	};
// 	destObj.removeEventListener	= function(event, fct){
// 		if(this._events === undefined) 	this._events	= {};
// 		if( event in this._events === false  )	return;
// 		this._events[event].splice(this._events[event].indexOf(fct), 1);
// 	};
// 	destObj.dispatchEvent	= function(event /* , args... */){
// 		console.log('dispatchEvent', arguments)
// 		if( this._events === undefined ) 	this._events	= {};
// 		if( this._events[event] === undefined )	return;
// 		var tmpArray	= this._events[event].slice(); 
// 		for(var i = 0; i < tmpArray.length; i++){
// 			var result	= tmpArray[i].apply(this, Array.prototype.slice.call(arguments, 1))
// 			if( result !== undefined )	return result;
// 		}
// 		return undefined;
// 	};
// 	return destObj;
// };


// THREEx.BehaviorContext.MicroeventMixin( THREEx.BehaviorContext.prototype )



//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////
THREEx.BehaviorContext.prototype.setScene = function(scene) {
	this.scene	= scene
	return this
};

THREEx.BehaviorContext.prototype.setCamera = function(camera) {
	this.camera	= camera
	return this
};

THREEx.BehaviorContext.prototype.setDomElement = function(domElement) {
	this.domElement	= domElement
	return this;
};

//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////
THREEx.BehaviorContext.prototype.init = function() {
	var context	= this
// debugger
	// sanity check
	console.assert(context.domElement !== null)
	console.assert(context.camera !== null)
	console.assert(context.domElement !== null)

	// TODO init mouse event
	// init hammer event too

	context.domElement.addEventListener('click', function(event){
		console.log('CLICK')
		dispatchEvent('click', event)
	})

	//////////////////////////////////////////////////////////////////////////////////
	//		Comment								//
	//////////////////////////////////////////////////////////////////////////////////
	function dispatchEvent(eventName, eventData){
		var intersects	= computeIntersects()
		// console.log('intersects', intersects)
		intersects.forEach(function(intersect){
			var object3d	= intersect.object
			var behavior3d	= THREEx.BehaviorObject3d.fromObject3d(object3d)
			if( behavior3d === null )	return

			behavior3d.signals.click.dispatch()
		})
	}
	function computeIntersects(){
		var camera	= context.camera
		var scene	= context.scene
		var domElement	= context.domElement

		var projector	= new THREE.Projector()
		var raycaster	= new THREE.Raycaster()
		var mouse	= new THREE.Vector2()

		var rect 	= domElement.getBoundingClientRect();
		mouse.x		= (( event.clientX - rect.left)	/ rect.width  * 2 -1);
		mouse.y 	=-(( event.clientY - rect.top )	/ rect.height * 2 -1);

		var vector	= new THREE.Vector3(mouse.x, mouse.y, 1)
		projector.unprojectVector( vector, camera )

		raycaster.set( camera.position, vector.sub( camera.position ).normalize() )

		var intersects	= raycaster.intersectObjects( scene.children )
		console.log('intersects', intersects)	
		return intersects	
	}
};