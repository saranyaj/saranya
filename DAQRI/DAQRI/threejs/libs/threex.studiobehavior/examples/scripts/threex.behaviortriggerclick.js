var THREEx	= THREEx	|| {}

THREEx.BehaviorTriggerClick	= function(){
	THREEx.BehaviorTrigger.call( this );

	this.type	= 'TriggerClick'

	var SIGNALS	= signals;
	this.signals	= {
		ping	: new SIGNALS.Signal(),
	}
}


THREEx.BehaviorTriggerClick.prototype = Object.create( THREEx.BehaviorTrigger.prototype );

//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.BehaviorTriggerClick.prototype.start = function() {
	this.enabled	= true
}


THREEx.BehaviorTriggerClick.prototype.stop = function() {
	this.enabled	= false
}

THREEx.BehaviorTriggerClick.prototype.init = function() {
	var domElement	= document
	var trigger	= this
	var behaviorObject3D	= trigger.behaviors

	behaviorObject3D.signals.click.add(function(){
		console.log('triggerClick received click event')
		trigger.signals.ping.dispatch()
	})
};