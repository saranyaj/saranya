var THREEx	= THREEx	|| {}

THREEx.BehaviorActionVisibleToggle	= function(){
	THREEx.BehaviorAction.call( this );

	this.type	= 'ActionVisibleToggle'
}

THREEx.BehaviorActionVisibleToggle.prototype = Object.create( THREEx.BehaviorAction.prototype );

//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.BehaviorActionVisibleToggle.prototype.dispatch	= function(event){
	var action	= this
	var object3d	= action.behaviors.object3d
// console.log('dispatch', object3d)
	object3d.visible= object3d.visible === true ? false : true
}



