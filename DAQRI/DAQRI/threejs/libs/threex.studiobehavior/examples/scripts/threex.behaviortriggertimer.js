var THREEx	= THREEx	|| {}

THREEx.BehaviorTriggerTimer	= function(){
	THREEx.BehaviorTrigger.call( this );

	this.type	= 'TriggerTimer'

	var SIGNALS	= signals;
	this.signals	= {
		ping	: new SIGNALS.Signal(),
	}

	this.parameters	= {
		delay	: 200,
		repeat	: true,
	}

	this._timeout	= null
}


THREEx.BehaviorTriggerTimer.prototype = Object.create( THREEx.BehaviorTrigger.prototype );

//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////
THREEx.BehaviorTriggerTimer.prototype.start = function() {
	var trigger	= this
	if( trigger.parameters.repeat ){
		trigger._timeout	= setInterval(function(){
			// console.log('triggerTimer setInterval expired')
			trigger.signals.ping.dispatch()
		}, trigger.parameters.delay)		
	}else{
		trigger._timeout	= setTimeout(function(){
			// console.log('triggerTimer setTimeout expired')
			trigger.signals.ping.dispatch()
		}, trigger.parameters.delay)	
	}
}
//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////
THREEx.BehaviorTriggerTimer.prototype.stop = function() {
	var trigger	= this

	if( this._timeout === null )	return

	if( trigger.parameters.repeat === true ){
		clearInterval(this._timeout)
	}else{
		clearTimeout(this._timeout)
	}

	this._timeout	= null
}