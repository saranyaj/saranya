var THREEx	= THREEx	|| {}

THREEx.BehaviorScript	= function(){
	this.type	= ''
	this.name	= ''
	this.behaviors	= null

	// TODO support this.enabled	= false
	// especially for dom event
}

//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////
THREEx.BehaviorScript.prototype.setName = function(name){
	this.name	= name
	return this
}

THREEx.BehaviorScript.prototype.setBehaviors = function(behaviors) {
	this.behaviors	= behaviors
	return this
}


//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.BehaviorScript.prototype.init = function(){
}


THREEx.BehaviorScript.prototype.start = function() {
	// body...
}

THREEx.BehaviorScript.prototype.stop = function() {
	// body...
}
