var THREEx	= THREEx	|| {}

THREEx.BehaviorAction	= function(){
	THREEx.BehaviorScript.call( this );

	this.scriptType		= ''
	this.listenedEvents	= []
}

THREEx.BehaviorAction.prototype = Object.create( THREEx.BehaviorScript.prototype );

//////////////////////////////////////////////////////////////////////////////////
//		Comment								//
//////////////////////////////////////////////////////////////////////////////////

THREEx.BehaviorAction.prototype.bindTo = function(listenedEvent){
	this.listenedEvents.push(listenedEvent)
	return this
}


THREEx.BehaviorAction.prototype.init = function() {
	var action	= this
	var behaviors	= action.behaviors
	var context	= behaviors.context
	this.listenedEvents.forEach(function(listenedEvent){

		var scriptName	= listenedEvent.replace(/\.(.+)$/, '')
		var signalName	= listenedEvent.replace(/^(.+)\./, '')
// console.log('listenedEvent', listenedEvent)
// console.log('scriptName', scriptName)
// console.log('signalName', signalName)
		// connect trigger and actions
		var trigger	= behaviors.findScriptByName(scriptName)
		console.assert(trigger !== null)
		console.assert(trigger.signals[signalName] !== undefined)
		trigger.signals[signalName].add(function(event){
			action.dispatch(event)
		})
	})
};
