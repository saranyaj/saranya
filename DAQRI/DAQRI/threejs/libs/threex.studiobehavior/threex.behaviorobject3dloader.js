var THREEx	= THREEx	|| {}

THREEx.BehaviorObject3dLoader	= function(){}

THREEx.BehaviorObject3dLoader.prototype.parse = function(output) {
	var behaviors	= new THREEx.BehaviorObject3d()

	output.scripts.forEach(function(data){
		if( data.type === 'ActionVisibleToggle' ){
			var script	= new THREEx.BehaviorActionVisibleToggle()
		}else if( data.type === 'TriggerClick' ){
			var script	= new THREEx.BehaviorTriggerClick()
		}else if( data.type === 'TriggerTimer' ){
			var script	= new THREEx.BehaviorTriggerTimer()
		}else	console.assert(false, 'data.type is unknown: ', data.type)

		if( data.name )		script.name		= data.name
		if( data.parameters )	script.parameters	= JSON.parse(JSON.stringify(data.parameters))

		if( data.listenedEvents ){
			data.listenedEvents.forEach(function(listenedEvent){
				script.bindTo(listenedEvent)
			})
		}

		script.setBehaviors(behaviors)

		behaviors.addScript(script)
	})

	return behaviors
};
