var THREEx	= THREEx	|| {}


THREEx.BehaviorDialog	= function(callback){
	// backup the current activeElement to restore it later
	var activeEl	= document.activeElement

	var dialogEl	= document.createElement( 'dialog' );
	dialogEl.setAttribute('id', 'behaviorDialog')

	var formEl	= document.createElement('form')
	formEl.method	= 'dialog'
	dialogEl.appendChild(formEl)

	//////////////////////////////////////////////////////////////////////////////////
	//		nameRow
	//////////////////////////////////////////////////////////////////////////////////

	var nameRow	= document.createElement('div')
	nameRow.classList.add('row')
	formEl.appendChild(nameRow)

	var labelEl	= document.createElement('label')
	labelEl.textContent	= 'Name'
	nameRow.appendChild(labelEl)

	var nameInputEl	= document.createElement('input')
	nameInputEl.setAttribute('required', 'required')
	nameRow.appendChild(nameInputEl)

	//////////////////////////////////////////////////////////////////////////////////
	//		triggerRow
	//////////////////////////////////////////////////////////////////////////////////

	var triggerRow	= document.createElement('div')
	triggerRow.classList.add('row')
	formEl.appendChild(triggerRow)

	var labelEl	= document.createElement('label')
	labelEl.textContent	= 'Trigger'
	triggerRow.appendChild(labelEl)

	var triggerSelectEl	= document.createElement('select')
	triggerRow.appendChild(triggerSelectEl)

	var options	= {
		'Click'		: 'click',
		'Timer'		: 'timer',
	}
	Object.keys(options).forEach(function(value){
		var text	= options[value]

		var optionEl	= document.createElement('option')
		optionEl.value	= value
		optionEl.textContent	= text
		triggerSelectEl.appendChild(optionEl)
	})


	//////////////////////////////////////////////////////////////////////////////////
	//		actionRow
	//////////////////////////////////////////////////////////////////////////////////

	var actionRow	= document.createElement('div')
	actionRow.classList.add('row')
	formEl.appendChild(actionRow)

	var labelEl	= document.createElement('label')
	labelEl.textContent	= 'Action'
	actionRow.appendChild(labelEl)

	var actionSelectEl	= document.createElement('select')
	actionRow.appendChild(actionSelectEl)

	var options	= {
		'VisibleToggle'	: 'Toggle visibility',
	}
	Object.keys(options).forEach(function(optionValue){
		var optionText	= options[optionValue]

		var optionEl	= document.createElement('option')
		optionEl.value	= optionValue
		optionEl.textContent	= optionText
		actionSelectEl.appendChild(optionEl)
	})

	//////////////////////////////////////////////////////////////////////////////////
	//		handle buttons1
	//////////////////////////////////////////////////////////////////////////////////
	
	var buttonOk		= document.createElement('button')
	buttonOk.type       	= 'submit'
	buttonOk.style.float	= 'right'
	buttonOk.value		= 'OK'
	buttonOk.innerText	= 'OK'
	formEl.appendChild(buttonOk)

	var buttonCancel	= document.createElement('button')
	buttonCancel.style.float= 'left'
	buttonCancel.value	= 'cancel'
	buttonCancel.innerText	= 'Cancel'
	formEl.appendChild(buttonCancel)

	// if cancel is clicked, close the dialog without validating it
	buttonCancel.addEventListener('click', function(event){
		dialogEl.close()
		event.stopPropagation()
		event.preventDefault()
	})

	//////////////////////////////////////////////////////////////////////////////////
	//		Comment								//
	//////////////////////////////////////////////////////////////////////////////////

	document.body.appendChild(dialogEl)

	dialogEl.showModal()


	dialogEl.addEventListener('close', function (event) {
		// restore the focus to the activeEl
		activeEl.focus()
		// notify the callback
		if( dialogEl.returnValue === '' || dialogEl.returnValue === 'cancel' ){
			callback(null)
			return
		}

		//////////////////////////////////////////////////////////////////////////////////
		//		Comment								//
		//////////////////////////////////////////////////////////////////////////////////
		// TODO build behaviors based on form data


		var behaviorObject3d	= new THREEx.BehaviorObject3d()

		console.log('triggerselect', triggerSelectEl.value);
		var triggerType	= triggerSelectEl.value
		var trigger	= new THREEx['BehaviorTrigger'+triggerType]
		behaviorObject3d.addScript(trigger)

		console.log('actionselect', actionSelectEl.value);
		var actionType	= actionSelectEl.value
		var action	= new THREEx['BehaviorAction'+actionType]
		behaviorObject3d.addScript(action)

		//////////////////////////////////////////////////////////////////////////////////
		//		Comment								//
		//////////////////////////////////////////////////////////////////////////////////
		callback(behaviorObject3d)
	});

	return dialogEl;
}
