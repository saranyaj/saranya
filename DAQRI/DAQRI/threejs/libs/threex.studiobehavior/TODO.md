# TODO

* renamed THREEx.BehaviorObject3D = THREEx.Object3DBehavior.js

* make a hammer script trigger
* add the UI in the editor itself
* plug the serialisation into the scene save

## How to handle centralisation event handling
* threex.behaviorcontext.js     to handle general event
* threex.behaviorobject3d.js
        * for current threex.behaviors.js

## How to handle drag/drop
* first hammer.js press on the object to drag
* then listen on panmove/panend/pancancel
* if panmove, move the object to 3d position
    - aka a screen position projected on a 3d plane
    - where to put the 3d plane
* if panend, move the object in 3d position
* if pancancel, come back to original position
