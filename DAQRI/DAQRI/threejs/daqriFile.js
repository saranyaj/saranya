var daqriFile = function () {};

daqriFile.DownloadFile = function (url, id, callback) {

	var loader = new THREE.XHRLoader();

	loader.load( url, function ( file ) {
		callback(id, file);				
	} );
};

daqriFile.FileCallback = function (file) {
	console.log(file);
	// stuff happens?
};

daqriFile.ErrorCallback = function (error) {
	console.log(error);
};
	
daqriFile.LoadModel = function ( file, name ) {

	var extension = name.split( '.' ).pop().toLowerCase();
	var geometries;

	switch ( extension ) {

		case 'babylon':
/*
			var reader = new FileReader();
			reader.addEventListener( 'load', function ( event ) {

				var contents = event.target.result;
				var json = JSON.parse( contents );

				var loader = new THREE.BabylonLoader();
				var scene = loader.parse( json );

				editor.setScene( scene );

			}, false );
			reader.readAsText( file );

			break;*/

		case 'ctm':

		/*	var reader = new FileReader();
			reader.addEventListener( 'load', function ( event ) {

				var data = new Uint8Array( event.target.result );

				var stream = new CTM.Stream( data );
				stream.offset = 0;

				var loader = new THREE.CTMLoader();
				loader.createModel( new CTM.File( stream ), function( geometry ) {

					geometry.sourceType = "ctm";
					geometry.sourceFile = file.name;

					var material = new THREE.MeshPhongMaterial();

					var mesh = new THREE.Mesh( geometry, material );
					mesh.name = filename;

					editor.addObject( mesh );
					editor.select( mesh );

				} );

			}, false );
			reader.readAsArrayBuffer( file );

			break;*/

		case 'dae':

			/*var reader = new FileReader();
			reader.addEventListener( 'load', function ( event ) {

				var contents = event.target.result;

				var parser = new DOMParser();
				var xml = parser.parseFromString( contents, 'text/xml' );

				var loader = new THREE.ColladaLoader();
				loader.parse( xml, function ( collada ) {

					collada.scene.name = filename;

					editor.addObject( collada.scene );
					editor.select( collada.scene );

				} );

			}, false );
			reader.readAsText( file );

			break;*/

		case 'js':
		case 'json':

		case '3geo':
		case '3mat':
		case '3obj':
		case '3scn':

			/*var reader = new FileReader();
			reader.addEventListener( 'load', function ( event ) {

				var contents = event.target.result;

				// 2.0

				if ( contents.indexOf( 'postMessage' ) !== -1 ) {

					var blob = new Blob( [ contents ], { type: 'text/javascript' } );
					var url = URL.createObjectURL( blob );

					var worker = new Worker( url );

					worker.onmessage = function ( event ) {

						event.data.metadata = { version: 2 };
						handleJSON( event.data, file, filename );

					};

					worker.postMessage( Date.now() );

					return;

				}

				// >= 3.0

				var data;

				try {

					data = JSON.parse( contents );

				} catch ( error ) {

					alert( error );
					return;

				}

				handleJSON( data, file, filename );

			}, false );
			reader.readAsText( file );

			break;*/

		case 'obj':

			var object = new THREE.OBJLoader().parse( file );
			geometries = object.children				

			break;

		case 'ply':

			/*var reader = new FileReader();
			reader.addEventListener( 'load', function ( event ) {

				var contents = event.target.result;

				console.log( contents );

				var geometry = new THREE.PLYLoader().parse( contents );
				geometry.sourceType = "ply";
				geometry.sourceFile = file.name;

				var material = new THREE.MeshPhongMaterial();

				var mesh = new THREE.Mesh( geometry, material );
				mesh.name = filename;

				editor.addObject( mesh );
				editor.select( mesh );

			}, false );
			reader.readAsText( file );

			break;*/

		case 'stl':

			/*var reader = new FileReader();
			reader.addEventListener( 'load', function ( event ) {

				var contents = event.target.result;

				var geometry = new THREE.STLLoader().parse( contents );
				geometry.sourceType = "stl";
				geometry.sourceFile = file.name;

				var material = new THREE.MeshPhongMaterial();

				var mesh = new THREE.Mesh( geometry, material );
				mesh.name = filename;

				editor.addObject( mesh );
				editor.select( mesh );

			}, false );

			if ( reader.readAsBinaryString !== undefined ) {

				reader.readAsBinaryString( file );

			} else {

				reader.readAsArrayBuffer( file );

			}

			break;*/

		/*
		case 'utf8':

			var reader = new FileReader();
			reader.addEventListener( 'load', function ( event ) {

				var contents = event.target.result;

				var geometry = new THREE.UTF8Loader().parse( contents );
				var material = new THREE.MeshLambertMaterial();

				var mesh = new THREE.Mesh( geometry, material );

				editor.addObject( mesh );
				editor.select( mesh );

			}, false );
			reader.readAsBinaryString( file );

			break;
		*/

		case 'vtk':

			/*var reader = new FileReader();
			reader.addEventListener( 'load', function ( event ) {

				var contents = event.target.result;

				var geometry = new THREE.VTKLoader().parse( contents );
				geometry.sourceType = "vtk";
				geometry.sourceFile = file.name;

				var material = new THREE.MeshPhongMaterial();

				var mesh = new THREE.Mesh( geometry, material );
				mesh.name = filename;

				editor.addObject( mesh );
				editor.select( mesh );

			}, false );
			reader.readAsText( file );

			break;*/

		case 'wrl':

			/*var reader = new FileReader();
			reader.addEventListener( 'load', function ( event ) {

				var contents = event.target.result;

				var result = new THREE.VRMLLoader().parse( contents );

				editor.setScene( result );

			}, false );
			reader.readAsText( file );

			break;*/

		default:

			alert( 'Unsupported file format. daqriFile' );

			break;

	}

	return geometries;
};


daqriFile.LoadTexture = function ( file, callback) {
	var reader = new FileReader();
	reader.addEventListener( 'load', function ( event ) {
		var image = document.createElement( 'img' );
		image.addEventListener( 'load', function( event ) {
			var texture = new THREE.Texture( this ); // this = image
			texture.sourceFile = file.name;
			texture.name = file.name;
			texture.needsUpdate = true;
			callback( texture );			
		}, false );
		image.src = event.target.result;
	}, false );
	reader.readAsDataURL( file );	
};

daqriFile.DownloadTexture = function ( url, id, callback ) {
	var image = document.createElement( 'img' );
	var fileName = url.split("/").pop();
	image.crossOrigin = "Anonymous";
	image.addEventListener( 'load', function() {
		var texture = new THREE.Texture( this ); // this = image
		texture.sourceFile = fileName;
		texture.sourceURL = url
		texture.name = fileName;
		texture.needsUpdate = true;
		callback ( id, texture );
	}, false );	
	image.src = url;
};

daqriFile.DownloadVideo = function ( url, id, callback ) {
	var video = document.createElement( 'video' );
	var fileName = url.split("/").pop();
	video.crossOrigin = "Anonymous";
	// video.addEventListener( 'loadeddata', function() {
	//video.addEventListener( 'load', function() {
	video.addEventListener( 'loadstart', function() {
		log("inside load func for video - " + fileName);
		var rawVideo = video; // this = image
		rawVideo.sourceFile = fileName;
		rawVideo.needsUpdate = true;
		log("calling callback - " + fileName);
		callback ( id, rawVideo );
	}, false );	
	log("video src - " + url);
	video.src = url;
	//video.load();
};