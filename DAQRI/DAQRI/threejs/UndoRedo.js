var UndoRedo	= function(editor){
	var _this	= this
	this.enabled	= true

	//////////////////////////////////////////////////////////////////////////////////
	//		Bind all signals
	//////////////////////////////////////////////////////////////////////////////////
	var signals = editor.signals;
	// bind all signals
	signals.objectAdded.add(onChange);
	signals.objectChanged.add(onChange);
	signals.objectRemoved.add(onChange);
	signals.materialChanged.add(onChange);
	signals.sceneGraphChanged.add(onChange);

	//////////////////////////////////////////////////////////////////////////////////
	//		no more than one saveState per minDelay
	//////////////////////////////////////////////////////////////////////////////////
	// minimum delay between 2 saveState() in milliseconds
	var minDelay	= 200
	var timeout	= null
	var ignoreChange= false
	function onChange(){
		// check if we need to ignore change at the moment
		if( ignoreChange === true )	return
		// honor .enabled
		if( _this.enabled === false )	return

		// clear previous timeout
		// - TODO should it be before ignoreChange ? i dunno.
		clearTimeout( timeout );
		// launch/relaunch the timeout
		timeout = setTimeout(function(){
			// check if we need to ignore change at the moment
			if( ignoreChange === true )	return
			// honor .enabled
			if( _this.enabled === false )	return
			// actually save state
			saveState()
		}, minDelay);
	}

	//////////////////////////////////////////////////////////////////////////////////
	//		Declare variables
	//////////////////////////////////////////////////////////////////////////////////
	// to store all the states for undo
	// - last element is supposed to be the current state
	var undoStack	= []
	this._undoStack	= undoStack	// for debug in javascript console
	// to store all states for redo
	var redoStack	= []
	this._redoStack	= redoStack	// for debug in javascript console
	//////////////////////////////////////////////////////////////////////////////////
	//		exposed functions
	//////////////////////////////////////////////////////////////////////////////////

	/**
	 * perform a redo if available
	 */
	this.undo	= function(){
		// honor .enabled
		if( _this.enabled === false )	return
		// sanity check
		if( undoStack.length <= 1 ){
			console.warn('cant undo')
			return
		}
		// feed redoStack with last state from undoStack
		redoStack.push(undoStack.pop())
		var state	= undoStack[undoStack.length-1]
		restoreState(state)

		signals.sceneGraphChanged.dispatch();
	}

	/**
	 * perform a redo if available
	 */
	this.redo	= function(){
		// honor .enabled
		if( _this.enabled === false )	return
		// sanity check
		if( redoStack.length === 0 ){
			console.warn('cant redo')
			return
		}
		// remove the most recent state in the redoStack
		var state	= redoStack.pop()
		// push it as the most recent state in the undoStack
		undoStack.push(state)
		// actually restore state
		restoreState(state)

		// notify the editor
		signals.sceneGraphChanged.dispatch();
	}
	/**
	 * reset the undoredo stacks
	 */
	this.reset	= function(){
		// reset undoStack
		undoStack.splice(0, undoStack.length)
		// reset redoStack
		redoStack.splice(0, redoStack.length)
		// add initial state
		saveState()
	}

	/**
	 * display some stats - useful to debug
	 */
	function dumpStats(prefix){
		console.log(prefix+': undoStack-length='+undoStack.length+' redoStack-length='+redoStack.length)
	}
	this.dumpStats	= dumpStats

	//////////////////////////////////////////////////////////////////////////////////
	//		saveState
	//////////////////////////////////////////////////////////////////////////////////
	function saveState(){
		// sanity check
		console.assert(ignoreChange === false)
		console.assert(_this.enabled === true)
		// keep only stackMaxLength undoStack - to keep memory in check
		var stackMaxLength	= 25;
		while( undoStack.length > stackMaxLength )	undoStack.shift()
		
		// convert scene into a undoStack
		// TODO i could push is as json string... better for ram ?
		var exporter	= new THREE.ObjectExporter();
		var state	= exporter.parse( editor.scene )

		//////////////////////////////////////////////////////////////////////////////////
		//		round up all number in the state to avoid floating point comparison issue
		//////////////////////////////////////////////////////////////////////////////////
		// round the state to 5
		state	= roundNumberInJson(state, 5)
		/**
		 * round numbers in a JSON object based on precision - useful to avoid 
		 * scary long Floating point Number and their imprecision
		 * 
		 * @param  {Object} data    the json object
		 * @param  {Integer} nDigits Number of significant digits after comma
		 */
		function roundNumberInJson(data, nDigits){
			var precision	= Math.pow(10, nDigits)
			function processData(data){
				// handle Number (the one which does the rounding)
				if(data instanceof Number || typeof(data) === 'number' ){
					// TODO what about only keeping the X more significative digits ?
					// - would that be better ? unsure
					// - decimalAdjust() in https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/round
					data	= Math.round(data*precision)/precision
					return data
				}
				// handle array
				if(data instanceof Array){
					for(var i = 0; i < data.length; i++){
						data[i]	= processData(data[i])
					}
					return data
				}
				// handle object
				if(data instanceof Object){
					Object.keys(data).forEach(function(key){
						data[key]	= processData(data[key])
					})
					return data
				}
				// any other type is unprocessed
				return data
			}

			data	= processData(data)
			return data
		}

		//////////////////////////////////////////////////////////////////////////////////
		//		Avoid Duplicate
		//////////////////////////////////////////////////////////////////////////////////

		// if current state equal to the most recent one, do nothing
		// - avoid useless duplicates
		if( undoStack.length >= 1 ){
			var lastState	= undoStack[undoStack.length-1]
			var currState	= state
			var areEqual	= JSON.stringify(lastState) === JSON.stringify(currState)
			// console.log('saveState. is new state equal to old one', areEqual)
			// current state is equal to previous one. no need to save state
			if( areEqual === true )	return;
		}

		//////////////////////////////////////////////////////////////////////////////////
		//		actually save new state
		//////////////////////////////////////////////////////////////////////////////////
		// add this to the current scene
		undoStack.push(state)
		// reset redoStack
		redoStack.splice(0, redoStack.length)
	}

	//////////////////////////////////////////////////////////////////////////////////
	//		restoreState
	//////////////////////////////////////////////////////////////////////////////////
	function restoreState(state){
		var scene	= editor.scene
		// sanity check
		console.assert(ignoreChange === false)
		console.assert(_this.enabled === true)
		// ignore change while restoring state
		ignoreChange	= true

		// backup the selected object to restore it later
		var selectedUUID= editor.selected ? editor.selected.uuid : null
		editor.deselect()

		// load scene
		var loader	= new THREE.ObjectLoader()
		var newScene	= loader.parse(state)

		// set the new scene
		editor.setScene(newScene)

		// reset all material - thus no issue in when the undo has a light
		scene.traverse( function( object3d ){
			if( object3d.material === undefined )	return
			// mark this material as needsUpdate
			object3d.material.needsUpdate = true;	
			// Handle MeshFaceMaterial special case			
			if ( object3d.material instanceof THREE.MeshFaceMaterial ) {
				for ( var i = 0; i < object3d.material.materials.length; i ++ ) {
					object3d.material.materials[ i ].needsUpdate = true;
				}
			}
		});

		// restore the object selection if possible
		if( selectedUUID )	editor.selectByUuid(selectedUUID)
		
		// no more ignore change while restoring state
		ignoreChange	= false
	}
}

