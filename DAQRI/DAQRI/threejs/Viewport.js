var Viewport = function ( editor ) {

	var signals = editor.signals;

	// create the container
	$('#viewport-container').html("");
	var container = new UI.Panel();
	container.dom	= document.getElementById('viewport-container');

	// jme- export viewport in editor
	editor.viewport	= container

	var info = new UI.Text();
	info.setPosition( 'absolute' );
	info.setRight( '5px' );
	info.setBottom( '5px' );
	info.setFontSize( '12px' );
	info.setColor( '#ffffff' );
	info.setValue( 'objects: 0, vertices: 0, faces: 0' );
	// container.add( info );

	var scene = editor.scene;
	var sceneHelpers = editor.sceneHelpers;

	var objects = [];

        //////////////////////////////////////////////////////////////////////////////////
        //		to support THREEx.GameInstance
        //////////////////////////////////////////////////////////////////////////////////

        container.renderingEnabled	= true
        var controlsEnabled		= true
        Object.defineProperty(container, 'controlsEnabled', {
                enumerable	: true,
                get : function(){
                        return controlsEnabled;
                },
                set : function(value){
                        controlsEnabled	= value;

                        if( controlsEnabled === false ){
                                subViewports.forEach(function(subViewport){
                                        subViewport._displaySize        = 'none'
                                        subViewport.domElement.dataset.displaySize      = 'none'
                                })
                        }else{
                                subViewports.forEach(function(subViewport){
                                        subViewport._displaySize        = 'normal'
                                        subViewport.domElement.dataset.displaySize      = 'normal'
                                })
				// set subViewport1 in doubleBoth as default layout
				subViewport1.setDisplaySize('doubleBoth', subViewports)
                        }
		}
        })

	// helpers
	//////////////////////////////////////////////////////////////////////////////////
	//		init renderer
	//////////////////////////////////////////////////////////////////////////////////

	var clearColor, renderer;

	if ( System.support.webgl === true ) {

		renderer = new THREE.WebGLRenderer({
			antialias: true,
			alpha	: true,
		});

	} else {
		if (window.daqri.environment == 'test') {
			renderer = new THREE.CanvasRenderer();
		} else {
			console.assert(false, 'WEBGL IS REQUIRED')
			alert('WEBGL IS REQUIRED')
		}
	}

	renderer.autoClear	= false;
	renderer.autoUpdateScene= false;
	container.renderer	= renderer	// export renderer

	//////////////////////////////////////////////////////////////////////////////////
	//		selectionBox
	//////////////////////////////////////////////////////////////////////////////////
	var selectionBox = new THREE.BoxHelper();
	selectionBox.material.depthTest = false;
	selectionBox.material.transparent = true;
	selectionBox.visible = false;
	sceneHelpers.add( selectionBox );

	//////////////////////////////////////////////////////////////////////////////////
	//		create sub viewports
	//////////////////////////////////////////////////////////////////////////////////
	var domElementSubViewports	= document.createElement('div')
	domElementSubViewports.id	= 'subViewports'
	container.dom.appendChild( domElementSubViewports );

	var subViewports	= []
	container.subViewports	= subViewports

	/*
	var subViewport1	= createSubViewport('topLeft', 'doubleBoth', new THREE.Vector3(0, 225, 250))
	subViewports.push(subViewport1)

	// view is top
	var subViewport2	= createSubViewport('topRight', 'none', new THREE.Vector3(0, 250, 0))
	subViewports.push(subViewport2)

	// view is front
	var subViewport3	= createSubViewport('bottomLeft', 'none', new THREE.Vector3(0, 0, 250))
	subViewports.push(subViewport3)

	// view is right
	var subViewport4	= createSubViewport('bottomRight', 'none', new THREE.Vector3(250, 0, 0))
	subViewports.push(subViewport4)
	*/

	function createSubViewport(subViewportId, viewportSize, cameraPosition){
		// add the domElement to catch events
		var domElement	= document.createElement('div')
		domElement.id	= 'subViewport'+subViewportId
		domElement.classList.add('subViewport')
		domElementSubViewports.appendChild(domElement)

		// create the subViewport itself
		var subViewport	= new SubViewport(subViewportId, domElement, renderer, viewportSize, cameraPosition)

		//////////////////////////////////////////////////////////////////////////////////
		//		Init contextualMenu
		//////////////////////////////////////////////////////////////////////////////////
		var contextualMenu	= new ContextualMenu(domElement)

		//////////////////////////////////////////////////////////////////////////////////
		//		Fullscreen Button
		//////////////////////////////////////////////////////////////////////////////////
		var sizeButton	= new ViewportButton(domElement, onClickSizeButton);
		sizeButton.title	= 'Change the full size'
		sizeButton.innerHTML	= '<img class="sizeButton"/>'
		sizeButton.classList.add('sizeButton')
		domElement.appendChild(sizeButton)

		function onClickSizeButton(){
			var displaySize		= subViewport.getDisplaySize()
			//console.log('resize',subViewportId,'from', displaySize)
			if( displaySize === 'doubleBoth' ){
				subViewport.setDisplaySize('normal', subViewports)
			}else{
				subViewport.setDisplaySize('doubleBoth', subViewports)
			}
		}

		//////////////////////////////////////////////////////////////////////////////////
		//		Expand Horizontal Button
		//////////////////////////////////////////////////////////////////////////////////
		var doubleWidthButton	= new ViewportButton(domElement, onClickDoubleWidthButton);
		doubleWidthButton.title	= 'Change the horizontal size'
		doubleWidthButton.innerText	= 'double width'
		doubleWidthButton.innerHTML	= '<img class="doubleWidthButton"/>'		
		doubleWidthButton.classList.add('doubleWidthButton')
		domElement.appendChild(doubleWidthButton)

		function onClickDoubleWidthButton(){
			var displaySize		= subViewport.getDisplaySize()
			//console.log('resize',subViewportId,'from', displaySize)
			if( displaySize === 'normal' ){
				subViewport.setDisplaySize('doubleWidth', subViewports)
			}else{
				subViewport.setDisplaySize('normal', subViewports)
			}
		}

		//////////////////////////////////////////////////////////////////////////////////
		//		Expand Vertical Button
		//////////////////////////////////////////////////////////////////////////////////
		var doubleHeightButton	= new ViewportButton(domElement, onClickDoubleHeightButton);
		doubleHeightButton.title	= 'Change the vertical size'
		doubleHeightButton.innerText	= 'double height'
		doubleHeightButton.innerHTML	= '<img class="doubleHeightButton">'
		doubleHeightButton.classList.add('doubleHeightButton')
		domElement.appendChild(doubleHeightButton)

		function onClickDoubleHeightButton(){
			var displaySize		= subViewport.getDisplaySize()
			//console.log('resize',subViewportId,'from', displaySize)
			if( displaySize === 'normal' ){
				subViewport.setDisplaySize('doubleHeight', subViewports)
			}else{
				subViewport.setDisplaySize('normal', subViewports)
			}
		}

		//////////////////////////////////////////////////////////////////////////////////
		//		Comment
		//////////////////////////////////////////////////////////////////////////////////

		if( subViewportId === 'topLeft' ){
			domElement.classList.add('subViewportLeft')
			domElement.classList.add('subViewportTop')
		}else if( subViewportId === 'topRight' ){
			domElement.classList.add('subViewportRight')
			domElement.classList.add('subViewportTop')
		}else if( subViewportId === 'bottomLeft' ){
			domElement.classList.add('subViewportLeft')
			domElement.classList.add('subViewportBottom')
		}else if( subViewportId === 'bottomRight' ){
			domElement.classList.add('subViewportRight')
			domElement.classList.add('subViewportBottom')
		}else	console.assert(false)

		return subViewport	
	}

	//////////////////////////////////////////////////////////////////////////////////
	//		Fog
	//////////////////////////////////////////////////////////////////////////////////
	var oldFogType = "None";
	var oldFogColor = 0xaaaaaa;
	var oldFogNear = 1;
	var oldFogFar = 5000;
	var oldFogDensity = 0.00025;

	//////////////////////////////////////////////////////////////////////////////////
	//		Signals
	//////////////////////////////////////////////////////////////////////////////////

	signals.requestAnimationFrame.add(function(){
		render()
	})

	signals.rendererChanged.add( function ( type ) {
		var parentElement	= renderer.domElement.parent
		parentElement.removeChild( renderer.domElement );

		alert('changing renderer without need')
		renderer = new THREE[ type ]( { antialias: true } );
		renderer.autoClear = false;
		renderer.autoUpdateScene = false;
		renderer.setClearColor(0x222222, 1);
		renderer.setSize( container.dom.offsetWidth, container.dom.offsetHeight );

		parentElement.appendChild( renderer.domElement );

		render();
	} );

	signals.sceneGraphChanged.add( function () {

		render();
		updateInfo();

	} );

	signals.objectSelected.add( function ( object ) {

		selectionBox.visible = false;
		// transformControls.detach();
		subViewports.forEach(function(subViewport){
			var transformControls	= subViewport.transformControls
			transformControls.detach();
		})

		if ( object !== null ) {

			if ( object.geometry !== undefined &&
				 object instanceof THREE.Sprite === false ) {

				selectionBox.update( object );
				selectionBox.visible = true;

			}

			if ( object instanceof THREE.PerspectiveCamera === false ) {

				// transformControls.attach( object );
				subViewports.forEach(function(subViewport){
					var transformControls	= subViewport.transformControls
					transformControls.attach(object);
				})

			}

		}

		render();

	} );

	signals.objectAdded.add( function ( object ) {

		var materialsNeedUpdate = false;

		object.traverse( function ( child ) {

			if ( child instanceof THREE.Light ) materialsNeedUpdate = true;

			objects.push( child );

		} );

		if ( materialsNeedUpdate === true ) updateMaterials();

	} );

	signals.objectChanged.add( function ( object ) {

		subViewports.forEach(function(subViewport){
			var transformControls	= subViewport.transformControls
			transformControls.update();
		})

		// if ( object !== camera  ) {

			if ( object.geometry !== undefined ) {

				selectionBox.update( object );

			}

			if ( editor.helpers[ object.id ] !== undefined ) {

				editor.helpers[ object.id ].update();

			}

			updateInfo();

		// }

		render();

	} );

	signals.objectRemoved.add( function ( object ) {

		var materialsNeedUpdate = false;

		object.traverse( function ( child ) {

			if ( child instanceof THREE.Light ) materialsNeedUpdate = true;

			objects.splice( objects.indexOf( child ), 1 );

		} );

		if ( materialsNeedUpdate === true ) updateMaterials();

	} );

	signals.helperAdded.add( function ( object ) {

		objects.push( object.getObjectByName( 'picker' ) );

	} );

	signals.helperRemoved.add( function ( object ) {

		objects.splice( objects.indexOf( object.getObjectByName( 'picker' ) ), 1 );

	} );

	signals.materialChanged.add( function ( material ) {

		render();

	} );

	signals.fogTypeChanged.add( function ( fogType ) {

		if ( fogType !== oldFogType ) {

			if ( fogType === "None" ) {

				scene.fog = null;

			} else if ( fogType === "Fog" ) {

				scene.fog = new THREE.Fog( oldFogColor, oldFogNear, oldFogFar );

			} else if ( fogType === "FogExp2" ) {

				scene.fog = new THREE.FogExp2( oldFogColor, oldFogDensity );

			}

			updateMaterials();

			oldFogType = fogType;

		}

		render();

	} );

	signals.fogColorChanged.add( function ( fogColor ) {

		oldFogColor = fogColor;

		updateFog( scene );

		render();

	} );

	signals.fogParametersChanged.add( function ( near, far, density ) {

		oldFogNear = near;
		oldFogFar = far;
		oldFogDensity = density;

		updateFog( scene );

		render();

	} );

	signals.windowResize.add( function () {
		var width	= $(window).outerWidth(true)  - 550;
		var height	= $(window).outerHeight(true) - 293;

		renderer.setSize( width, height );
		// renderer.setSize( container.dom.offsetWidth, container.dom.offsetHeight );

		render();
	} );

	signals.playAnimations.add( function (animations) {
		
		function animate() {

			requestAnimationFrame( animate );
			
			for ( var i = 0; i < animations.length ; i ++ ) {

				animations[i].update(0.016);

			} 

			render();
		}

		animate();

	} );

	//////////////////////////////////////////////////////////////////////////////////
	//		Comment
	//////////////////////////////////////////////////////////////////////////////////

	domElementSubViewports.appendChild( renderer.domElement );
	animate();

	function updateInfo() {

		var objects = 0;
		var vertices = 0;
		var faces = 0;

		scene.traverse( function ( object ) {

			if ( object instanceof THREE.Mesh ) {

				objects ++;

				var geometry = object.geometry;

				if ( geometry instanceof THREE.Geometry ) {

					vertices += geometry.vertices.length;
					faces += geometry.faces.length;

				} else if ( geometry instanceof THREE.BufferGeometry ) {

					vertices += geometry.attributes.position.array.length / 3;

					if ( geometry.attributes.index !== undefined ) {

						faces += geometry.attributes.index.array.length / 3;

					} else {

						faces += geometry.attributes.position.array.length / 9;

					}

				}

			}

		} );

		info.setValue( 'objects: ' + objects + ', vertices: ' + vertices + ', faces: ' + faces );

	}

	function updateMaterials() {

		editor.scene.traverse( function ( node ) {

			if ( node.material ) {

				node.material.needsUpdate = true;

				if ( node.material instanceof THREE.MeshFaceMaterial ) {

					for ( var i = 0; i < node.material.materials.length; i ++ ) {

						node.material.materials[ i ].needsUpdate = true;

					}

				}

			}

		} );

	}

	function updateFog( root ) {

		if ( root.fog ) {

			root.fog.color.setHex( oldFogColor );

			if ( root.fog.near !== undefined ) root.fog.near = oldFogNear;
			if ( root.fog.far !== undefined ) root.fog.far = oldFogFar;
			if ( root.fog.density !== undefined ) root.fog.density = oldFogDensity;

		}

	}

	function animate() {

		requestAnimationFrame( animate );

	}

	function render(){
		// honor .renderingEnabled
		if( container.renderingEnabled  === false )	return

		// jme- added preRender signal
		signals.preRender.dispatch();

		sceneHelpers.updateMatrixWorld();
		scene.updateMatrixWorld();

		subViewports.forEach(function(subViewport){
			subViewport.cameraMenu.update()
		})

		subViewports.forEach(function(subViewport){
			subViewport.render(scene, sceneHelpers)
		})
	}

	container.removeAllEventListeners = function() {

		isAnimating = false
		subViewports.forEach(function(subViewport){
			subViewport.removeAllEventListeners()
		})
	}

	container.cameraPositions = function() {
		cameraPositions = {}
		subViewports.forEach(function(subViewport){
			cameraPositions[subViewport.subViewportId] = subViewport.camera.position
		})
		return cameraPositions
	}

	return container;


//////////////////////////////////////////////////////////////////////////////////
//		Viewport.SubViewport
//////////////////////////////////////////////////////////////////////////////////

/**
 * [SubViewport description]
 * @param {[type]} subViewportId [description]
 * @param {[type]} domElement    [description]
 * @param {[type]} renderer      [description]
 */
 function SubViewport(subViewportId, domElement, renderer, viewportSize, cameraPosition){
	this.domElement		= domElement
	this.subViewportId	= subViewportId

	var sceneSubViewport = new THREE.Scene();

	//////////////////////////////////////////////////////////////////////////////////
	//		compute attachements
	//////////////////////////////////////////////////////////////////////////////////
	if( subViewportId.match(/^top/) !== null ){
		this.vertAttach	= 'top'
	}else if( subViewportId.match(/^bottom/) !== null ){
		this.vertAttach	= 'bottom'
	}else	console.assert(false)

	if( subViewportId.match(/Right$/) !== null ){
		this.horiAttach	= 'right'
	}else if( subViewportId.match(/Left$/) !== null ){
		this.horiAttach	= 'left'
	}else	console.assert(false)

	// console.log('subViewportId', subViewportId, this.horiAttach, this.vertAttach)

	//////////////////////////////////////////////////////////////////////////////////
	//		Camera
	//////////////////////////////////////////////////////////////////////////////////

	// init camera
	var boundingRect= domElement.getBoundingClientRect();
	// var camera	= new THREE.PerspectiveCamera(50, boundingRect.width/boundingRect.height, 1, 8000)
	var camera	= new THREE.PerspectiveCamera(20, boundingRect.width/boundingRect.height, 1, 8000 );
	camera.position.set(0,1,3).multiplyScalar(200)
	this.camera	= camera

	// set initial camera position from editor.config
	// camera.position.fromArray( editor.config.getKey( 'camera_'+subViewportId ).position );
	// camera.lookAt( new THREE.Vector3().fromArray( editor.config.getKey( 'camera_'+subViewportId ).target ) );

	camera.position = cameraPosition;
	camera.lookAt( new THREE.Vector3( 0, 0 ,0 ) );

	//////////////////////////////////////////////////////////////////////////////////
	//		Create Canvas
	//////////////////////////////////////////////////////////////////////////////////
	this.render	= function(scene, sceneHelpers){
		var canvasRect	= renderer.domElement.getBoundingClientRect();
		var scissorRect	= domElement.getBoundingClientRect();
		// compute scissor dimension based on scissorRect in canvasRect
		var left	= scissorRect.left  - canvasRect.left
		var bottom	= canvasRect.bottom - scissorRect.bottom
		var width	= scissorRect.width
		var height	= scissorRect.height

		// update camera aspect
		camera.aspect = width / height;
		camera.updateProjectionMatrix();

		// setup scissor
		renderer.setViewport( left, bottom, width, height );
		renderer.setScissor(  left, bottom, width, height );
		renderer.enableScissorTest( true );
		// clear the buffers and render the scene
		renderer.setClearColor(0x222222, 1);
		renderer.clear();
		renderer.render( scene, camera )
		renderer.render( sceneHelpers, camera )		
		renderer.render( sceneSubViewport, camera )		
	}


	//////////////////////////////////////////////////////////////////////////////////
	//		transformControls
	//////////////////////////////////////////////////////////////////////////////////
	var transformControls = new THREE.TransformControls( camera, domElement );
	this.transformControls= transformControls

	var onChangeTransformControls	= function () {
		editorControls.enabled = true;
		if ( transformControls.axis !== null ) {
			editorControls.enabled = false;
		}

		if ( editor.selected !== null ) {
			signals.objectChanged.dispatch( editor.selected );
		}
	}
	transformControls.addEventListener( 'change', onChangeTransformControls)
	sceneSubViewport.add( transformControls );

	//////////////////////////////////////////////////////////////////////////////////
	//		Signals Handling
	//////////////////////////////////////////////////////////////////////////////////
	signals.transformModeChanged.add( function ( mode ) {
		transformControls.setMode( mode );
	} );

	signals.snapChanged.add( function ( dist ) {
		transformControls.setSnap( dist );
	} );

	signals.spaceChanged.add( function ( space ) {
		transformControls.setSpace( space );
	});

	signals.windowResize.add( function () {
		var boundingRect= domElement.getBoundingClientRect();
		camera.aspect = boundingRect.width / boundingRect.height;
		camera.updateProjectionMatrix();
	} );

	var saveTimeout;
	signals.cameraChanged.add( function () {
		// save camera position on timeout
		clearTimeout( saveTimeout );
		saveTimeout = setTimeout( function () {
			editor.config.setKey( 'camera_'+subViewportId, {
				position: camera.position.toArray(),
				target	: editorControls.center.toArray()
			});
		}, 1000 );
		// render with the changed camera
		render();
	} );

	//////////////////////////////////////////////////////////////////////////////////
	//		Object Picking
	//////////////////////////////////////////////////////////////////////////////////

	// object picking
	var raycaster = new THREE.Raycaster();
	var projector = new THREE.Projector();

	// events
	var getIntersects = function ( event, object ) {
		// get coordinates
		var rect = domElement.getBoundingClientRect();
		var x = ( event.clientX - rect.left ) / rect.width;
		var y = ( event.clientY - rect.top ) / rect.height;
		var vector = new THREE.Vector3( ( x ) * 2 - 1, - ( y ) * 2 + 1, 0.5 );
		// unproject vector
		projector.unprojectVector( vector, camera );
		raycaster.set( camera.position, vector.sub( camera.position ).normalize() );

		if ( object instanceof Array ) {

			return raycaster.intersectObjects( object );

		}

		return raycaster.intersectObject( object );

	};

	var onMouseDownPosition	= new THREE.Vector2();
	var onMouseUpPosition	= new THREE.Vector2();

	var onMouseDown = function ( event ) {
		event.preventDefault();

		var rect = domElement.getBoundingClientRect();
		var x = (event.clientX - rect.left) / rect.width;
		var y = (event.clientY - rect.top) / rect.height;
		onMouseDownPosition.set( x, y );

		document.addEventListener( 'mouseup', onMouseUp, false );
	};

	var onMouseUp = function ( event ) {
		var rect = domElement.getBoundingClientRect();
		x = (event.clientX - rect.left) / rect.width;
		y = (event.clientY - rect.top) / rect.height;
		onMouseUpPosition.set( x, y );

		// TODO this === 0 make it super fragile... why not a special IF ON THE SAME object
		// - similar to the web click is ok IIF down/up are on the same element
		if( onMouseDownPosition.distanceTo( onMouseUpPosition ) == 0 ) {

			var intersects = getIntersects( event, objects );

			if ( intersects.length > 0 ) {

				var object = intersects[ 0 ].object;

				if ( object.userData.object !== undefined ) {

					// helper

					editor.select( object.userData.object );

				}else if ( object.userData.selectableUUID !== undefined ) {

					// jme- if ```object``` is just another part of a bigger object
					editor.selectByUuid( object.userData.selectableUUID );

				} else {

					editor.select( object );

				}

			} else {

				editor.select( null );

			}

			render();

		}

		document.removeEventListener( 'mouseup', onMouseUp );
	};

	var onDoubleClick = function ( event ) {
		var intersects = getIntersects( event, objects );
		if ( intersects.length > 0 && intersects[ 0 ].object === editor.selected ) {
			editorControls.focus( editor.selected );
		}
	};

	domElement.addEventListener( 'mousedown', onMouseDown, false );
	domElement.addEventListener( 'dblclick', onDoubleClick, false );

	//////////////////////////////////////////////////////////////////////////////////
	//		Editor Controls
	//////////////////////////////////////////////////////////////////////////////////
	// controls need to be added *after* main logic,
	// otherwise controls.enabled doesn't work.

	var editorControls = new THREE.EditorControls( camera, domElement );
	editorControls.center.fromArray( editor.config.getKey( 'camera_'+subViewportId ).target )
	var onChangeEditorControlchange	= function () {
		transformControls.update();
		signals.cameraChanged.dispatch( camera );
	}
	editorControls.addEventListener('change', onChangeEditorControlchange);


	//////////////////////////////////////////////////////////////////////////////////
	//		Handle grid
	//////////////////////////////////////////////////////////////////////////////////

	var grid = Grid( 10 );
	
	for( var i = 0; i < grid.length; i++) {
		sceneSubViewport.add(grid[i])
	}
	

	// handle varying grid step
	var currentGridStep	= 5;
	function gridOnEditorControlsChange(){
		var distance	= editorControls.center.clone().distanceTo( camera.position );
		var gridStep;

		if( distance < 10 ){
			gridStep = 1;
		} else if( distance < 50 ){
			gridStep = 10;
		} else if (distance < 750){
			gridStep = 100;
		} else {
			gridStep = 1000;
		}

		// if no change, do nothing
		if ( gridStep === currentGridStep )	return

		currentGridStep = gridStep
		for( var i = 0; i < grid.length; i++) {
			sceneSubViewport.remove(grid[i]);
		}
		grid = Grid( gridStep )
		for( var i = 0; i < grid.length; i++) {
			sceneSubViewport.add(grid[i])
		}
	}
	editorControls.addEventListener( 'change', gridOnEditorControlsChange)
	gridOnEditorControlsChange()

	var topbarElement = document.createElement('div')
	topbarElement.classList.add('topbar')
	domElement.appendChild(topbarElement)				

	//////////////////////////////////////////////////////////////////////////////////
	//		init camera menu
	//////////////////////////////////////////////////////////////////////////////////

	// init camera menu
	this.cameraMenu	= new CameraMenu(domElement, editorControls, camera, cameraPosition)

	//////////////////////////////////////////////////////////////////////////////////
	//		handle displaySize
	//////////////////////////////////////////////////////////////////////////////////

	this._displaySize = viewportSize
	domElement.dataset.displaySize	= this._displaySize

	this.getDisplaySize	= function(){
		return this._displaySize
	}
	this.setDisplaySize	= function(displaySize, subViewports){
		// console.log('********************************')
		// console.log('setDisplaySize', subViewportId, 'from', this._displaySize,'to',displaySize)
		// console.dir(subViewports)

		if( displaySize === 'doubleBoth' ){
			subViewports.forEach(function(subViewport){
				subViewport._displaySize	= 'none'
			})
		}else if( displaySize === 'doubleWidth' ){
			var hasDoubleHeight	= false
			subViewports.forEach(function(subViewport){
				if( subViewport._displaySize === 'doubleHeight')	hasDoubleHeight = true
			})
			if( hasDoubleHeight ){
				subViewports.forEach(function(subViewport){
					subViewport._displaySize	= 'normal'
				})				
			}
			subViewports.forEach(function(subViewport){
				if( subViewport.vertAttach !== this.vertAttach )	return
				subViewport._displaySize	= 'none'
			}.bind(this))
		}else if( displaySize === 'doubleHeight' ){
			var hasDoubleWidth	= false
			subViewports.forEach(function(subViewport){
				if( subViewport._displaySize === 'doubleWidth')	hasDoubleWidth = true
			})
			if( hasDoubleWidth ){
				subViewports.forEach(function(subViewport){
					subViewport._displaySize	= 'normal'
				})				
			}

			subViewports.forEach(function(subViewport){
				if( subViewport.horiAttach !== this.horiAttach )	return
				subViewport._displaySize	= 'none'
			}.bind(this))
		}else if( displaySize === 'normal' ){
			if( this._displaySize === 'doubleWidth'){
				subViewports.forEach(function(subViewport){
					if( subViewport.vertAttach !== this.vertAttach )	return
					subViewport._displaySize	= 'normal'
				}.bind(this))				
			}else if( this._displaySize === 'doubleHeight'){
				subViewports.forEach(function(subViewport){
					if( subViewport.horiAttach !== this.horiAttach )	return
					subViewport._displaySize	= 'normal'
				}.bind(this))				
			}else{
				subViewports.forEach(function(subViewport){
					subViewport._displaySize	= 'normal'
				})				
			}
		}else{
			debugger;
			console.assert(false)
		}

		this._displaySize	= displaySize

		// syncCssDisplaySize
		subViewports.forEach(function(subViewport){
			subViewport.domElement.dataset.displaySize = subViewport.getDisplaySize()
		})
	}

	//////////////////////////////////////////////////////////////////////////////////
	//		removeAllEventListeners
	//////////////////////////////////////////////////////////////////////////////////
	this.removeAllEventListeners = function() {
		transformControls.removeAllEventListeners( );
		transformControls.removeEventListener( 'change', onChangeTransformControls )

		editorControls.removeAllEventListeners();
		editorControls.removeEventListener( 'change', gridOnEditorControlsChange)
		editorControls.removeEventListener( 'change', onChangeEditorControlchange);

		domElement.removeEventListener( 'mousedown', onMouseDown, false );
		domElement.removeEventListener( 'dblclick', onDoubleClick, false );

		document.removeEventListener( 'mouseup', onMouseUp, false );

		this.cameraMenu.removeAllEventListeners()
	}
}


//////////////////////////////////////////////////////////////////////////////////
//		Viewport.CameraMenu
//////////////////////////////////////////////////////////////////////////////////
function CameraMenu(domElement, editorControls, camera){
	var onDestroyFcts	= []
	var menuElement		= document.createElement('div')
	menuElement.classList.add('cameraMenu')

	domElement.appendChild(menuElement)

	var buttonElement	= document.createElement('div')
	buttonElement.classList.add('triggerButton')

	var cameraImage	= document.createElement('img')
	cameraImage.src = '/png/camera.png'
	buttonElement.appendChild(cameraImage)

	var cameraLabel = document.createElement('span')
	cameraLabel.innerText	= 'Camera'
	buttonElement.appendChild(cameraLabel)

	menuElement.appendChild(buttonElement)

	var cameraLock = document.createElement('div')
	cameraLock.classList.add('cameraLock') 
	menuElement.appendChild(cameraLock)

	var isCameraLocked = false
	cameraLock.addEventListener('mouseup', function(event){
		isCameraLocked = !isCameraLocked
		if (isCameraLocked)
			cameraLock.classList.add('locked')
		else
			cameraLock.classList.remove('locked')

		event.stopPropagation()
		event.preventDefault()							
	})


	var onChangeEditorControls = function () {
		cameraLabel.innerText	= 'Camera'
	}
	editorControls.addEventListener( 'change', onChangeEditorControls );
	onDestroyFcts.push(function(){
		editorControls.removeEventListener( onChangeEditorControls )
	})

	var optionsElement	= document.createElement('div')
	optionsElement.classList.add('menuOptions')
	optionsElement.style.display	= 'none'
	menuElement.appendChild(optionsElement)

	var menuListElement	= document.createElement('ul')
	optionsElement.appendChild(menuListElement)

	var options	= ['perspective', 'front', 'back', 'left', 'right', 'top', 'bottom']
	options.forEach(function(option){
		var menuLineElement	= document.createElement('li')
		menuLineElement.dataset.action	= option
		menuLineElement.innerText	= option.charAt(0).toUpperCase() + option.slice(1);
		menuListElement.appendChild(menuLineElement)
		menuLineElement.addEventListener('mousedown', function(event){
			optionsElement.style.display = 'none'
			// buttonElement.innerText	= menuLineElement.innerText
			onOptionClick(option, menuLineElement)
			event.stopPropagation()
			event.preventDefault()							
		})
	})

	// handle menu open/close
	buttonElement.addEventListener('mousedown', function(event){
		if (!isCameraLocked)
		{
			if( optionsElement.style.display === '' ){
				optionsElement.style.display = 'none'
			}else{
				optionsElement.style.display = ''
			}
		}
		event.stopPropagation()
		event.preventDefault()							
	}, false)

	//////////////////////////////////////////////////////////////////////////////////
	//		Camera Perspective
	//////////////////////////////////////////////////////////////////////////////////
	function onOptionClick(option){
		//console.log('option', option, buttonElement)
		// set button value		
		cameraLabel.innerText	= option.charAt(0).toUpperCase() + option.slice(1);

		var deltaUnit = new THREE.Vector3()
		if( option === 'perspective' )	deltaUnit.set(+1,+1,+1).normalize()
		else if( option === 'front' )	deltaUnit.set( 0, 0,+1)
		else if( option === 'back' )	deltaUnit.set( 0, 0,-1)
		else if( option === 'right' )	deltaUnit.set(+1, 0, 0)
		else if( option === 'left' )	deltaUnit.set(-1, 0, 0)
		else if( option === 'top' )		deltaUnit.set( 0.05,+1, 0.0)
		else if( option === 'bottom' )	deltaUnit.set( 0.05,-1, 0)
		else	console.assert(false)
		
		setCameraPosition(deltaUnit)
	}

	//////////////////////////////////////////////////////////////////////////////////
	//		Comment								//
	//////////////////////////////////////////////////////////////////////////////////
	var tweenControls	= new THREEx.TweenControls(camera)
	// customize the function to tween
	tweenControls.tweenFunction	= function(k){
		return 1 - ( --k * k * k * k );	// Quartic.Out
	}


	this.update	= function(){
		tweenControls.update()

		// update editorControls.center with current target
		camera.lookAt(editorControls.center)
	}
	//////////////////////////////////////////////////////////////////////////////////
	//		Utils function
	//////////////////////////////////////////////////////////////////////////////////
	function setCameraPosition(deltaUnit){
		if(deltaUnit == null)
			return

		// target is selected object or scene.postion
		var target	= editor.selected || editor.scene

		// measure current distance to target
		var distance	= target.position.clone().sub(camera.position).length()

		// create temporary camera to compute tweenControls
		var finalCamera	= camera.clone()

		// set camera.position from target and deltaUnit
		var delta	= deltaUnit.clone().multiplyScalar(distance)
		var finalPosition= target.position.clone().add(delta)

		// Set the tweenControls
		tweenControls.setTarget(finalPosition)

		// update editorControls.center with current target
		editorControls.center.copy(target.position)

		// dispatch event
		signals.cameraChanged.dispatch( camera );
	}

	//////////////////////////////////////////////////////////////////////////////////
	//		removeAllEventListeners
	//////////////////////////////////////////////////////////////////////////////////
	this.removeAllEventListeners = function() {
		onDestroyFcts.forEach(function(onDestroy){
			onDestroy()
		})
	}
}

//////////////////////////////////////////////////////////////////////////////////
//		Viewport.ViewportButton
//////////////////////////////////////////////////////////////////////////////////
function ViewportButton(domElement, onButtonClick){
	var buttonElement	= document.createElement('div')
	buttonElement.classList.add('viewportButton')


	// handle menu open/close
	buttonElement.addEventListener('mousedown', function(event){
		onButtonClick()
		event.stopPropagation()
		event.preventDefault()							
	}, false)

	return buttonElement
}

//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//		Viewport.ContextualMenu
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////
function ContextualMenu(domElement){

	// define menu Options
	var menuOptions	= {
		'translate'		: 'Translate',
		'rotate'		: 'Rotate',
		'scale'			: 'Scale',
		'toggleWireframe'	: 'Toggle Wireframe',
		'toggleSnapToGrid'	: 'Toggle Snap to Grid',
		'clone'			: 'Clone',
		'delete'		: 'Delete',
	}

	//////////////////////////////////////////////////////////////////////////////////
	//		Build Menu
	//////////////////////////////////////////////////////////////////////////////////

	var menuElement	= document.createElement('div')
	menuElement.style.display	= 'none'
	menuElement.setAttribute('id', 'designer3jsContextMenu')
	domElement.appendChild(menuElement)
	var ulElement	= document.createElement('ul')
	menuElement.appendChild(ulElement)
	Object.keys(menuOptions).forEach(function(action){
		var liElement	= document.createElement('li')
		ulElement.appendChild(liElement)
		liElement.innerText	= menuOptions[action]
		liElement.dataset.action= action
		liElement.setAttribute('class', action);
	})

	//////////////////////////////////////////////////////////////////////////////////
	//		Bind events to make it appear/disappear
	//////////////////////////////////////////////////////////////////////////////////
	var onMouseDownPosition	= new THREE.Vector2();
	function onMouseDown(event){
		// if not the right mouse button, return now
		if( event.button !== 2 )	return;
		// update onMouseDownPosition
		var boundingRect	= domElement.getBoundingClientRect();
		onMouseDownPosition.x	= event.clientX - boundingRect.left
		onMouseDownPosition.y	= event.clientY - boundingRect.top
		// bind mouseup
		domElement.addEventListener( 'mouseup', onMouseUp );
	}
	function onMouseUp(event){
		// if not the right mouse button, return now
		if( event.button !== 2 )	return;
		// compute onMouseUpPosition
		var onMouseUpPosition	= new THREE.Vector2();
		var boundingRect	= domElement.getBoundingClientRect();
		onMouseUpPosition.x	= event.clientX - boundingRect.left
		onMouseUpPosition.y	= event.clientY - boundingRect.top
		// test if the mouse moved
		if( onMouseDownPosition.distanceTo( onMouseUpPosition ) !== 0 )	return;

		if( menuElement.style.display === 'block' ){
			menuElement.style.display	= 'none';
		}else{
			menuElement.style.display	= 'block';
			menuElement.style.left	= (onMouseUpPosition.x-80)+'px';
			menuElement.style.top	= (onMouseUpPosition.y-10)+'px';
		}
		// stop the event here
		event.preventDefault()
		event.stopPropagation()
		// upbind the event
		domElement.removeEventListener( 'mouseup', onMouseUp );
	}
	domElement.addEventListener( 'contextmenu', function(event){ event.preventDefault(); }, false );
	domElement.addEventListener( 'mousedown', onMouseDown);

	//////////////////////////////////////////////////////////////////////////////////
	//		Bind events for menu options
	//////////////////////////////////////////////////////////////////////////////////
	// listen to click on each line of the menu
	var elements	= menuElement.querySelectorAll('li');
	[].slice.call(elements).forEach(function(liElement){
		// console.log('li', liElement, liElement.dataset.action)
		liElement.addEventListener('mousedown', function(event){
			if( event.button !== 0 )	return;
			// hide menu
			menuElement.style.display	= 'none';
			// get the action for this line
			var action	= liElement.dataset.action
			// handle event
			onMenuOptionClick(action)
			// stop the event here
			event.preventDefault()
			event.stopPropagation()
		})
	})

	//////////////////////////////////////////////////////////////////////////////////
	//		onMenuOptionClick
	//////////////////////////////////////////////////////////////////////////////////

	function onMenuOptionClick(action){
		var signals	= editor.signals;
		// console.log('do action', action)
		if( action === 'translate' ){
			if( editor.selected === null )	return
			signals.transformModeChanged.dispatch( 'translate' )
		}else if( action === 'rotate' ){
			if( editor.selected === null )	return
			signals.transformModeChanged.dispatch( 'rotate' )
		}else if( action === 'scale' ){
			if( editor.selected === null )	return
			signals.transformModeChanged.dispatch( 'scale' )
		}else if( action === 'toggleWireframe' ){
			if( editor.selected === null )	return
			if( editor.selected.parent === undefined ) return; // avoid cloning the camera or scene
			var object 	= editor.selected
			var material	= object.material
			console.assert(object.material)
			material.wireframe	= material.wireframe === false ? true : false
			signals.materialChanged.dispatch( material );
		}else if( action === 'toggleSnapToGrid' ){
			var gridWidth	= 25;	// FIXME- hardcoded
			var hadSnap	= editor.viewport.transformControls.snap !== null ? true : false
			signals.snapChanged.dispatch( hadSnap ? gridWidth : null );
		}else if( action === 'clone' ){
			if( editor.selected === null )	return
			if( editor.selected.parent === undefined ) return; // avoid cloning the camera or scene
			var object = editor.selected;
			object = object.clone();
			editor.addObject( object );
			editor.select( object );
		}else if( action === 'delete' ){
			if( editor.selected === null )	return
			var parent = editor.selected.parent;
			editor.removeObject( editor.selected );
			editor.select( parent );
		}else	console.assert(false, 'action '+action+' ')
	}
	// return the just-built menu
	return menuElement
}

}
