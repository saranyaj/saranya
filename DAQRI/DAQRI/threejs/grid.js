
//////////////////////////////////////////////////////////////////////////////////
//		Modified version of GridHelper from three.js
//////////////////////////////////////////////////////////////////////////////////

 	// edges #1460CE @ 93$
	// border 10px #1460CE @ 35%
	// lines 1px #1460CE 55%
	// major lines 2px 284985
	// base 1460CE 10% - the plane
	// create a plane geometry, set its color to 1460CE and its opacity to 10%. Then draw the lines on top of it?

Grid = function ( step ) {
	var size = step * 3
	var lineColor = new THREE.Color(0x1460CE)
	var boldLineColor = new THREE.Color(0x3662B2)

	// Background plane
	var geometryPlane = new THREE.PlaneGeometry(size*2, size*2, 1, 1)
	var mat = new THREE.MeshBasicMaterial()
	mat.color = lineColor
	mat.transparent = true
	mat.opacity = 0.10
	var mesh = new THREE.Mesh(geometryPlane, mat)
	mesh.serialisable = false
	mesh.cannotBeEdited = true
	mesh.position = new THREE.Vector3(0,0,0)
	mesh.setRotationFromAxisAngle( new THREE.Vector3( 1, 0, 0 ), -Math.PI/2 )

	var substep = step/10
	var meshes = new Array()
	if(step >= 1)
	{
		meshes.push(new MakeGrid(substep, size, 0.55, lineColor, step))
	}

	meshes.push(new MakeGrid(step, size, 1, boldLineColor, 0))

	meshes[0].children.push(mesh)
    
    // Create borders with boxes
    var narrow = 0.01 * step
    var offset = size - narrow / 2
    size = size * 2
    var box = MakeBox (size, narrow, narrow, boldLineColor)
    box.position = new THREE.Vector3(0,-(narrow/2), offset)
	meshes[0].children.push(box)

	box = MakeBox (size, narrow, narrow, boldLineColor)
    box.position = new THREE.Vector3(0,-(narrow/2), -offset)
	meshes[0].children.push(box)

	box = MakeBox (narrow, narrow, size, boldLineColor)
    box.position = new THREE.Vector3(offset,-(narrow/2), 0)
	meshes[0].children.push(box)

	box = MakeBox (narrow, narrow, size, boldLineColor)
    box.position = new THREE.Vector3(-offset,-(narrow/2), 0)
	meshes[0].children.push(box)

	return meshes
}

MakeBox = function (width, height, depth, color) {
	var boxGeo = new THREE.BoxGeometry(width, height, depth, 1, 1, 1)
	var boxMat = new THREE.MeshBasicMaterial()
	boxMat.color = color
	boxMat.transparent = true
	boxMat.opacity = 0.35
	var mesh = new THREE.Mesh(boxGeo, boxMat)
	mesh.serialisable = false
	mesh.cannotBeEdited = true
	return mesh
}

MakeGrid = function ( step, size, opacity, color, skip ) {

	var geometry = new THREE.Geometry();
	var material = new THREE.LineBasicMaterial( { vertexColors: THREE.VertexColors } );

	material.opacity = opacity
	if(skip !== 0) {
		material.transparent = true
	}

	for ( var i = - size; i <= size; i += step ) {
		if(skip != 0 && i % skip == 0){
			continue;
		}
		geometry.vertices.push(
			new THREE.Vector3( - size, 0, i ), new THREE.Vector3( size, 0, i ),
			new THREE.Vector3( i, 0, - size ), new THREE.Vector3( i, 0, size )
		);

		geometry.colors.push( color, color, color, color);
	}

	THREE.Line.call( this, geometry, material, THREE.LinePieces );
};

MakeGrid.prototype = Object.create( THREE.Line.prototype );

MakeGrid.prototype.setColors = function( colorCenterLine, colorGrid ) {

	this.color1.set( colorCenterLine );
	this.color2.set( colorGrid );

	this.geometry.colorsNeedUpdate = true;

}