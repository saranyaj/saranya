//
//  ProjectListViewController.h
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/22/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "ProjectDataTypes.h"

@class DAQRIWebRequestManager;


@interface DetailViewController : UIViewController

//Project/Steps/Task Title & Description Title
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UITextView *descriptionLabel;

//Steps Title and count number
@property (strong, nonatomic) IBOutlet UILabel *stepsTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *stepsCountLabel;

//Task Title and count number
@property (strong, nonatomic) IBOutlet UILabel *tasksTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *taskCountLabel;

//pointOfOrigin Title and name
@property (strong, nonatomic) IBOutlet UILabel *pointOfOriginTitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *pointOfOriginImageName;

//Assets type label
@property (strong, nonatomic) IBOutlet UILabel *documentTypeLabel;

//Views
@property (strong, nonatomic) IBOutlet UIView *projectTitleAndDescriptionSplitView;
@property (strong, nonatomic) IBOutlet UIView *splitviewBetweenDescriptionandPointOfOrigin;
@property (strong, nonatomic) IBOutlet UIView *stepsTaskView;
@property (strong, nonatomic) IBOutlet UIView *pointOfOriginView;

//Button click
@property (strong, nonatomic) IBOutlet UIButton *nextPageButton;
@property (strong, nonatomic) IBOutlet UIButton *refreshButton;

//Assets tableview
@property (strong, nonatomic) IBOutlet UITableView *tableView;


@property (nonatomic,retain)  NSMutableArray *tableViewDataSource;
@property (nonatomic, strong) DAQRIWebRequestManager *sharedManger;
@property (nonatomic, strong) TaskData *ltaskData;
@end
