//
//  SpinnerView.h
//  rc
//
//  Created by Saranya Jayaseelan on 5/7/14.
//  Copyright (c) 2014 ReliableCoders. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpinnerView : UIView{
    UIActivityIndicatorView *spinner;
}
@property(nonatomic,retain)UIActivityIndicatorView *spinner;
-(void)startAnimation;
-(void)stopAnimation;
@end
