//
//  DAQRIWebGLSubClassViewController.m
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/30/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import "DAQRIWebGLSubClassViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <ImageIO/CGImageProperties.h>


@interface DAQRIWebGLSubClassViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    
    int dynamicHeight;

}
@property(nonatomic,strong) UIImageView             *vImagePreview;
@property(nonatomic,retain) AVCaptureStillImageOutput *stillImageOutput;
@property(nonatomic,retain) AVCaptureSession *session;
@end



@implementation DAQRIWebGLSubClassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addingButtonOverlayView];
    // Do any additional setup after loading the view.
}

- (void)awakeFromNib {
    [super awakeFromNib];
}


-(void)addingButtonOverlayView{
    
    if(!_previousTaskButton){
        _previousTaskButton= [[UIButton alloc] initWithFrame:CGRectMake(16, 16, 186, 27)];
        [_previousTaskButton setImage:[UIImage imageNamed:@"bttn_previous_task1"]  forState:UIControlStateNormal];
        [_previousTaskButton addTarget:self action:@selector(previousTaskPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if(!_nextTaskButton){
        _nextTaskButton= [[UIButton alloc] initWithFrame:CGRectMake(16, 700, 186, 27)];
        [_nextTaskButton setImage:[UIImage imageNamed:@"bttn_next_task1"]  forState:UIControlStateNormal];
        [_nextTaskButton addTarget:self action:@selector(nextTaskPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if(!_cancelTaskButton){
        _cancelTaskButton= [[UIButton alloc] initWithFrame:CGRectMake(785, 22, 186, 27)];
        [_cancelTaskButton setImage:[UIImage imageNamed:@"bttn_cancel_task1"]  forState:UIControlStateNormal];
        [_cancelTaskButton addTarget:self action:@selector(cancelTaskPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"input_type =  %@",_ltaskData.input_type);
    NSLog(@"input_label =  %@",_ltaskData.input_label);
    NSLog(@"input_options = %@",_ltaskData.input_options);
    [self addingInputViews];


}

-(void)addingInputViews{
    
    [self.inputView removeFromSuperview];
    
    if([_ltaskData.input_type isEqualToString:@"measurement"]){
        [self loadMeaseuremntView];
        
    }else  if([_ltaskData.input_type isEqualToString:@"pass_fail"]){
        [self loadBoolView];
        
    }else  if([_ltaskData.input_type isEqualToString:@"media"]){
        if([_ltaskData.input_options[@"type"] isEqualToString:@"photo"]){
           //Take Photo
            [self takePhoto];
            
        }else{
          // take video
            [self takeVideo];
        }
        
    }else  if([_ltaskData.input_type isEqualToString:@"mult_choice"]){
        [self multipleChoice];

    }

}

-(void)multipleChoice{
    
    
    [self.inputView removeFromSuperview];
    self.inputView = [[UIView alloc] initWithFrame:CGRectZero];
    self.inputView.layer.backgroundColor = [[UIColor colorWithWhite:0.0f alpha:0.5f] CGColor];
    
    int y = 0;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 8, 418, 26)];
    [titleLabel setText:_ltaskData.input_label];
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel setFont:[UIFont fontWithName:PROXIMANOVA_BOLD size:18]];
    [self.inputView addSubview:titleLabel];
    
    y = titleLabel.frame.origin.y + titleLabel.frame.size.height;
    
    
    NSArray *array = _ltaskData.input_options[@"choices"];
    for(int i = 0; i < [array count] ; i++){
        UIButton *choiceButton = [[UIButton alloc] initWithFrame:CGRectMake(21, y, 130, 45)];
        [choiceButton setImage:[UIImage imageNamed:@"rb_notselected1"] forState:UIControlStateNormal];
        [choiceButton setTitle:[NSString stringWithFormat:@"  %@",[array objectAtIndex:i][@"text"]]  forState:UIControlStateNormal];
        [choiceButton addTarget:self action:@selector(radioButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.inputView addSubview:choiceButton];
        choiceButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        choiceButton.tag = i;
        
        y = choiceButton.frame.origin.y + choiceButton.frame.size.height;

    }
    
    UIButton *acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(21, y, 300, 64)];
    [acceptButton setImage:[UIImage imageNamed:@"bttn_accept1"] forState:UIControlStateNormal];
    [acceptButton addTarget:self action:@selector(acceptButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.inputView addSubview:acceptButton];
    
    y = acceptButton.frame.origin.y + acceptButton.frame.size.height;
    
    dynamicHeight = y;
    
}

-(void)takePhoto{
    
    [self.inputView removeFromSuperview];
    
    self.inputView = [[UIView alloc] initWithFrame:CGRectZero];
    self.inputView.layer.backgroundColor = [[UIColor colorWithWhite:0.0f alpha:0.5f] CGColor];
    
    UIButton *toggleButton = [[UIButton alloc] initWithFrame:CGRectMake(21, 19, 130, 45)];
    [toggleButton setImage:[UIImage imageNamed:@"tggle_photo"] forState:UIControlStateNormal];
    [toggleButton addTarget:self action:@selector(acceptButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.inputView addSubview:toggleButton];

    UIButton *photoButton = [[UIButton alloc] initWithFrame:CGRectMake(166, 19, 60, 46)];
    [photoButton setImage:[UIImage imageNamed:@"rb_notselected"] forState:UIControlStateNormal];
   // [photoButton addTarget:self action:@selector(takePhotoButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.inputView addSubview:photoButton];
    
}

-(void)takeVideo{
   
    [self.inputView removeFromSuperview];
    
    self.inputView = [[UIView alloc] initWithFrame:CGRectZero];
    self.inputView.layer.backgroundColor = [[UIColor colorWithWhite:0.0f alpha:0.5f] CGColor];
    
    UIButton *toggleButton = [[UIButton alloc] initWithFrame:CGRectMake(21, 19, 130, 45)];
    [toggleButton setImage:[UIImage imageNamed:@"tggle_video"] forState:UIControlStateNormal];
    [toggleButton addTarget:self action:@selector(acceptButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.inputView addSubview:toggleButton];
    
    UIButton *photoButton = [[UIButton alloc] initWithFrame:CGRectMake(166, 19, 60, 46)];
    [photoButton setImage:[UIImage imageNamed:@"rb_notselected"] forState:UIControlStateNormal];
    // [photoButton addTarget:self action:@selector(takePhotoButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.inputView addSubview:photoButton];
}

-(void)loadMeaseuremntView{
   
    [self.inputView removeFromSuperview];
    
    self.inputView = [[UIView alloc] initWithFrame:CGRectZero];
    self.inputView.layer.backgroundColor = [[UIColor colorWithWhite:0.0f alpha:0.5f] CGColor];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 8, 236, 26)];
    [titleLabel setText:_ltaskData.input_label];
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel setFont:[UIFont fontWithName:PROXIMANOVA_BOLD size:18]];
    [self.inputView addSubview:titleLabel];
    
    UITextField *inputOptionLabel = [[UITextField alloc] initWithFrame:CGRectMake(20, 45, 77, 64)];
    inputOptionLabel.placeholder = _ltaskData.input_options[@"units"];
    inputOptionLabel.userInteractionEnabled = YES;
    inputOptionLabel.textColor = [UIColor whiteColor];
    inputOptionLabel.textAlignment = NSTextAlignmentCenter;
    inputOptionLabel.layer.borderColor = [UIColor orangeColor].CGColor;
    inputOptionLabel.layer.borderWidth=1.0;
    [inputOptionLabel setFont:[UIFont fontWithName:PROXIMANOVA_BOLD size:19]];
    [self.inputView addSubview:inputOptionLabel];
    
    UIButton *acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(110, 45, 300, 64)];
    [acceptButton setImage:[UIImage imageNamed:@"bttn_accept1"] forState:UIControlStateNormal];
    [acceptButton addTarget:self action:@selector(acceptButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.inputView addSubview:acceptButton];
}

-(void)loadBoolView{
    
    [self.inputView removeFromSuperview];
    
    self.inputView = [[UIView alloc] initWithFrame:CGRectZero];
    self.inputView.layer.backgroundColor = [[UIColor colorWithWhite:0.0f alpha:0.5f] CGColor];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 8, 236, 26)];
    [titleLabel setText:_ltaskData.input_label];
    titleLabel.textColor = [UIColor whiteColor];
    [titleLabel setFont:[UIFont fontWithName:PROXIMANOVA_BOLD size:18]];
    [self.inputView addSubview:titleLabel];
    
    UILabel *trueLabel = [[UILabel alloc] initWithFrame:CGRectMake(63, 51, 90, 26)];
    [trueLabel setText:_ltaskData.input_options[@"option_a"]];
    trueLabel.textColor = [UIColor whiteColor];
    [trueLabel setFont:[UIFont fontWithName:PROXIMANOVA_BOLD size:18]];
    [self.inputView addSubview:trueLabel];
    
    UILabel *falseLabel = [[UILabel alloc] initWithFrame:CGRectMake(63, 85, 90, 26)];
    [falseLabel setText:_ltaskData.input_options[@"option_b"]];
    falseLabel.textColor = [UIColor whiteColor];
    [falseLabel setFont:[UIFont fontWithName:PROXIMANOVA_BOLD size:18]];
    [self.inputView addSubview:falseLabel];
    
    UIButton *trueLabelCheckBoxButton = [[UIButton alloc] initWithFrame:CGRectMake(22, 50, 33, 33)];
    trueLabelCheckBoxButton.tag = 0;
    [trueLabelCheckBoxButton setImage:[UIImage imageNamed:@"cbx_unchecked1"] forState:UIControlStateNormal];
    [trueLabelCheckBoxButton addTarget:self action:@selector(checkboxButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.inputView addSubview:trueLabelCheckBoxButton];
    
    UIButton *falseLabelCheckBoxButton = [[UIButton alloc] initWithFrame:CGRectMake(22, 87, 33, 22)];
    falseLabelCheckBoxButton.tag = 1;
    [falseLabelCheckBoxButton setImage:[UIImage imageNamed:@"cbx_unchecked1"] forState:UIControlStateNormal];
    [falseLabelCheckBoxButton addTarget:self action:@selector(checkboxButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.inputView addSubview:falseLabelCheckBoxButton];

    
    UIButton *acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(135, 47, 300, 64)];
    [acceptButton setImage:[UIImage imageNamed:@"bttn_accept1"] forState:UIControlStateNormal];
    [acceptButton addTarget:self action:@selector(acceptButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.inputView addSubview:acceptButton];
}

-(void)loadCameraPictureView{
    
}

#pragma mark webview delegates

- (void)webViewDidStartLoad:(UIWebView *)webView{
    self.childWebView = webView;
    
    [webView addSubview:_previousTaskButton];
    [webView addSubview:_nextTaskButton];
    [webView addSubview:_cancelTaskButton];
    [webView addSubview:_inputView];
    
    CGRect  frame = webView.frame;
    CGFloat inputViewWidth;
    CGFloat ingap    = -50;
    int x,y;
    
    
    if([_ltaskData.input_type isEqualToString:@"measurement"]){
        
         inputViewWidth = 418;
         x = frame.size.width  - inputViewWidth + ingap;
         y = 600;
        
        [self.inputView setFrame:CGRectMake(x, y, inputViewWidth, 145)];

    }else  if([_ltaskData.input_type isEqualToString:@"pass_fail"]){
        
        inputViewWidth = 461;
        x = frame.size.width  - inputViewWidth + ingap;
        y = 600;
        
        [self.inputView setFrame:CGRectMake(x, y, inputViewWidth, 145)];
        
    }else  if([_ltaskData.input_type isEqualToString:@"media"]){
        
        inputViewWidth = 260;
        x = self.view.frame.size.width/2  - inputViewWidth/2 + ingap;
        y = 600;
        
        [self.inputView setFrame:CGRectMake(x, y, inputViewWidth, 84)];
        
    }else  if([_ltaskData.input_type isEqualToString:@"mult_choice"]){
        y = dynamicHeight;
        [self.inputView setFrame:CGRectMake(477, self.view.frame.size.height - y - 10, 418, y)];
    }
}


- (void) webViewDidFinishLoad:(UIWebView *)webView {
    [super webViewDidFinishLoad:webView];
}

# pragma mark button actions

-(IBAction)previousTaskPressed:(id)sender{
    NSLog(@"previousTaskPressed");
}

-(IBAction)nextTaskPressed:(id)sender{
    NSLog(@"nextTaskPressed");
}

-(IBAction)cancelTaskPressed:(id)sender{
    NSLog(@"cancelTaskPressed");
}

-(IBAction)acceptButtonPressed:(id)sender{
    NSLog(@"acceptButtonPressed");
}

-(void)checkboxButtonPressed:(id)sender{
    NSLog(@"checkboxButtonPressed");
}
-(void)radioButtonPressed:(id)sender{
    NSLog(@"radioButtonPressed");
}

-(void)takePhotoButtonPressed:(id)sender{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.modalPresentationStyle = UIModalPresentationCurrentContext;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.allowsEditing = YES;
    self.imagePickerController = picker;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
    //[self.view addSubview:self.imagePickerController.view];
    
   /* AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in self.stillImageOutput.connections)
    {
        for (AVCaptureInputPort *port in [connection inputPorts])
        {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] )
            {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection)
        {
            break;
        }
    }
    
    NSLog(@"about to request a capture from: %@", self.stillImageOutput);
    [self.stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error)
     {
        // CFDictionaryRef exifAttachments = CMGetAttachment( imageSampleBuffer, kCGImagePropertyExifDictionary, NULL);
         if (error)
         {
             // Do something with the attachments.
             NSLog(@"attachements");
         } else {
             NSLog(@"no attachments");
         }
         
         NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
         UIImage *image = [[UIImage alloc] initWithData:imageData];
         
        // self.vImagePreview.image = image;
         
         UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
     }];*/
    
//    UIGraphicsBeginImageContext(self.view.bounds.size);
//    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
//    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    UIImageWriteToSavedPhotosAlbum(viewImage, nil, nil, nil);
//    
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Photo taken" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//    [alert show];
    
}

-(void)CameraSettingsInView{
    
    self.session = [[AVCaptureSession alloc] init];
    self.session.sessionPreset = AVCaptureSessionPresetMedium;
    
    CALayer *viewLayer = self.vImagePreview.layer;
    NSLog(@"viewLayer = %@", viewLayer);
    
    AVCaptureVideoPreviewLayer *captureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
    
    captureVideoPreviewLayer.frame = self.vImagePreview.bounds;
   // [self.vImagePreview.layer addSublayer:captureVideoPreviewLayer];
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    NSError *error = nil;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    if (!input) {
        // Handle the error appropriately.
        NSLog(@"ERROR: trying to open camera: %@", error);
    }
    [self.session addInput:input];
    
    self.stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
    [self.stillImageOutput setOutputSettings:outputSettings];
    [self.session addOutput:self.stillImageOutput];
    
    [self.session startRunning];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    NSLog(@"didFinishPickingMediaWithInfo");    
    [picker dismissViewControllerAnimated:YES completion:nil];

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
