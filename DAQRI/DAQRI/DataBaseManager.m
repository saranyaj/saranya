//
//  DataBaseManager.m
//  DAQRI
//
//  Created by Saranya Jayaseelan on 1/29/15.
//  Copyright (c) 2015 rc. All rights reserved.
//

#import "DataBaseManager.h"

@implementation DataBaseManager

+ (DataBaseManager *) shareManager {
    
    static DataBaseManager *shareManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareManager = [[self alloc] init];
    });
    return shareManager;
}

- (instancetype)init
{
    if (self = [super init]) {
        [self initializeDatabase];
        [self createEditableCopyOfDatabaseIfNeeded];
    }
    return self;
}


- (void)createEditableCopyOfDatabaseIfNeeded {
    
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];//path access
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:DAQRI_DATABASE_NAME];
    success = [fileManager fileExistsAtPath:writableDBPath];
    ////////NSLog(@"%@",paths);
    if (success) return;
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DAQRI_DATABASE_NAME];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}

- (void)initializeDatabase {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:DAQRI_DATABASE_NAME];
    NSLog(@"path = %@",path);
    
    if (sqlite3_open([path UTF8String], & database) == SQLITE_OK) {
        NSLog(@"Ok");
    } else {
        sqlite3_close(database);
        NSAssert1(0, @"Failed to open database with message '%s'.", sqlite3_errmsg(database));
    }
}
# pragma mark read table from project/steps/list

-(NSMutableArray *)readFromProjectTable
{
  
    NSMutableArray *projectArray = [NSMutableArray array];
    
    const char *sqlStatement = "select * from Projects";
    
    sqlite3_stmt *compiledStatement;
    
    if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
       
        // Loop through the results and add them to the feeds array
        //sqlite3_bind_text(compiledStatement,1,[category UTF8String], -1,SQLITE_TRANSIENT);
        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            
           ProjectData *projData = [[ProjectData alloc] init];
            
            // Read the data from the result row
            projData.projectId = [NSNumber numberWithInt:sqlite3_column_int(compiledStatement, 0)];
            projData.projectName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 1)];
            projData.projDescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
            projData.no_of_Steps = sqlite3_column_int(compiledStatement, 4);
            
            NSData *retrieveData = [[NSData alloc] initWithBytes:sqlite3_column_blob(compiledStatement, 5) length:sqlite3_column_bytes(compiledStatement, 5)];
            projData.projDocAssets = [NSKeyedUnarchiver unarchiveObjectWithData:retrieveData];
            
            [projectArray addObject:projData];
            
        }
    }
    sqlite3_finalize(compiledStatement);
    return projectArray;
    
}

-(NSMutableArray *)readFromStepTable
{
    
    NSMutableArray *stepArray = [NSMutableArray array];
    
    const char *sqlStatement = "select * from Steps";
    
    sqlite3_stmt *compiledStatement;
    
    if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
        
        // Loop through the results and add them to the feeds array
        //sqlite3_bind_text(compiledStatement,1,[category UTF8String], -1,SQLITE_TRANSIENT);
        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            
            StepData *stepData = [[StepData alloc] init];
            
            // Read the data from the result row
            stepData.projectId = [NSNumber numberWithInt:sqlite3_column_int(compiledStatement, 0)];
            stepData.stepsId = [NSNumber numberWithInt:sqlite3_column_int(compiledStatement, 1)];

            stepData.stepName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
            stepData.stepDescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
            stepData.no_of_tasks = sqlite3_column_int(compiledStatement, 4);
            
            NSData *retrieveData = [[NSData alloc] initWithBytes:sqlite3_column_blob(compiledStatement, 5) length:sqlite3_column_bytes(compiledStatement, 5)];
            stepData.stepDocAssets = [NSKeyedUnarchiver unarchiveObjectWithData:retrieveData];
            
            [stepArray addObject:stepData];
            
        }
    }
    sqlite3_finalize(compiledStatement);
    return stepArray;
    
}

-(NSMutableArray *)readFromTaskTable
{
    
    NSMutableArray *taskArray = [NSMutableArray array];
    
    const char *sqlStatement = "select * from Task";
    
    sqlite3_stmt *compiledStatement;
    
    if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
        
        // Loop through the results and add them to the feeds array
        //sqlite3_bind_text(compiledStatement,1,[category UTF8String], -1,SQLITE_TRANSIENT);
        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            
            TaskData *taskData = [[TaskData alloc] init];
            
            // Read the data from the result row
            taskData.projectId = [NSNumber numberWithInt:sqlite3_column_int(compiledStatement, 0)];
            taskData.stepsId = [NSNumber numberWithInt:sqlite3_column_int(compiledStatement, 1)];
            taskData.taskId = [NSNumber numberWithInt:sqlite3_column_int(compiledStatement, 2)];
            
            taskData.taskName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
            taskData.taskDescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
            
            taskData.input_type = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
            taskData.input_label = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
            
            NSData *retrieveData = [[NSData alloc] initWithBytes:sqlite3_column_blob(compiledStatement, 7) length:sqlite3_column_bytes(compiledStatement, 7)];
            taskData.input_options = [NSKeyedUnarchiver unarchiveObjectWithData:retrieveData];
            
            retrieveData = [[NSData alloc] initWithBytes:sqlite3_column_blob(compiledStatement, 8) length:sqlite3_column_bytes(compiledStatement, 8)];
            taskData.taskDocAssets = [NSKeyedUnarchiver unarchiveObjectWithData:retrieveData];
          
            [taskArray addObject:taskData];
            
        }
    }
    sqlite3_finalize(compiledStatement);
    return taskArray;
    
}


-(NSMutableArray *)readDocAssetsFromTaskTableUsingStepId:(int)stepId
{
    
    NSMutableArray *taskArray = [NSMutableArray array];
    
    NSString *sqlString = [NSString stringWithFormat:@"select DocAssets from Task where Step_id = %d",stepId];
    const char *sqlStatement = [sqlString UTF8String];
    
    sqlite3_stmt *compiledStatement;
    
    if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
        
        // Loop through the results and add them to the feeds array
        //sqlite3_bind_text(compiledStatement,1,[category UTF8String], -1,SQLITE_TRANSIENT);
        while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
            
           /* TaskData *taskData = [[TaskData alloc] init];
            
            // Read the data from the result row
            taskData.projectId = [NSNumber numberWithInt:sqlite3_column_int(compiledStatement, 0)];
            taskData.stepsId = [NSNumber numberWithInt:sqlite3_column_int(compiledStatement, 1)];
            taskData.taskId = [NSNumber numberWithInt:sqlite3_column_int(compiledStatement, 2)];
            
            taskData.taskName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
            taskData.taskDescription = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 4)];
            
            taskData.input_type = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 5)];
            taskData.input_label = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 6)];
            
            NSData *retrieveData = [[NSData alloc] initWithBytes:sqlite3_column_blob(compiledStatement, 7) length:sqlite3_column_bytes(compiledStatement, 7)];
            taskData.input_options = [NSKeyedUnarchiver unarchiveObjectWithData:retrieveData];*/
            
            NSData *retrieveData = [[NSData alloc] initWithBytes:sqlite3_column_blob(compiledStatement, 8) length:sqlite3_column_bytes(compiledStatement, 8)];
            NSMutableArray *docAssets = [NSKeyedUnarchiver unarchiveObjectWithData:retrieveData];
            
            [taskArray addObject:docAssets];
            
        }
    }
    sqlite3_finalize(compiledStatement);
    return taskArray;
    
}


# pragma mark create table for project/steps/list

-(void)insertProjectDetails:(ProjectData *)projectData
{
        @try{
            
            char *sqlName = "create table if not exists Projects(Project_id INTEGER PRIMARY KEY  NOT NULL  UNIQUE,Project_name VARCHAR,Project_description VARCHAR,Project_total_steps VARCHAR,Project_completed_steps INTEGER,DocAssets BLOB)";
            sqlite3_stmt *insertStatment;
            if (sqlite3_prepare_v2(database, sqlName, -1, &insertStatment, NULL) != SQLITE_OK)
            {
                NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
            }
            
            sqlite3_bind_int(insertStatment,  1, [projectData.projectId integerValue]);
            
            if(![projectData.projectName isKindOfClass: [NSNull class]])
            sqlite3_bind_text(insertStatment, 2, [projectData.projectName UTF8String], -1, SQLITE_TRANSIENT);
            
            if(![projectData.projDescription isKindOfClass: [NSNull class]])
            sqlite3_bind_text(insertStatment, 3, [projectData.projDescription UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_int(insertStatment,  4, projectData.steps.count);
            sqlite3_bind_int(insertStatment,  5, 0);
            
            if(![projectData.projDocAssets isKindOfClass: [NSNull class]]){
            NSData *theDictionaryData = [NSKeyedArchiver archivedDataWithRootObject:projectData.projDocAssets];
            sqlite3_bind_blob(insertStatment, 6, (__bridge const void *)(theDictionaryData), -1, SQLITE_TRANSIENT);
            }
            
            int success11 = sqlite3_step(insertStatment);
            printf("%d",success11);
            sqlite3_reset(insertStatment);
            if (success11 == SQLITE_ERROR)
            {
                NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
            }
            else
            {
                /* UIAlertView *alert = [[UIAlertView alloc]
                 initWithTitle:@"Added To step table"
                 message:@""
                 delegate:nil
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:nil];
                 [alert show];*/
                NSLog(@"Added To Project table");
            }
            sqlite3_finalize(insertStatment);
        }
    
        @catch (NSException *ex) {
            @throw ex;
        }
   }

-(void)insertStepsDetails:(StepData *)stepData
{
    @try{
        
        char *sqlName = "create table if not exists Steps (Project_id INTEGER NOT NULL , Steps_id INTEGER PRIMARY KEY  NOT NULL  UNIQUE , Steps_name VARCHAR,Steps_description VARCHAR, Steps_total_task INTEGER, Steps_Completed_task INTEGER, DocAssets BLOB)";
        
        sqlite3_stmt *insertStatment;
        if (sqlite3_prepare_v2(database, sqlName, -1, &insertStatment, NULL) != SQLITE_OK)
        {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
        
        
        sqlite3_bind_int(insertStatment,  1, [stepData.projectId integerValue]);
        sqlite3_bind_int(insertStatment,  2, [stepData.stepsId integerValue]);
        
        if(![stepData.stepName isKindOfClass: [NSNull class]])
        sqlite3_bind_text(insertStatment, 3, [stepData.stepName UTF8String], -1, SQLITE_TRANSIENT);
        
        if(![stepData.stepDescription isKindOfClass: [NSNull class]])
        sqlite3_bind_text(insertStatment, 4, [stepData.stepDescription UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_int(insertStatment,  5, stepData.tasks.count);
        sqlite3_bind_int(insertStatment,  6, 0);
        
        if(![stepData.stepDocAssets isKindOfClass: [NSNull class]]){
        NSData *theDictionaryData = [NSKeyedArchiver archivedDataWithRootObject:stepData.stepDocAssets];
        sqlite3_bind_blob(insertStatment, 7, (__bridge const void *)(theDictionaryData), -1, SQLITE_TRANSIENT);
        }

        
        int success11 = sqlite3_step(insertStatment);
        printf("%d",success11);
        sqlite3_reset(insertStatment);
        if (success11 == SQLITE_ERROR)
        {
            NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
        }
        else
        {
           /* UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Added To step table"
                                  message:@""
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];*/
            NSLog(@"Added To step table");
        }
        sqlite3_finalize(insertStatment);
    }
    
    @catch (NSException *ex) {
        @throw ex;
    }
}

-(void)insertTaskDetails:(TaskData *)taskData
{
    @try{
        
        char *sqlName = "create table if not exists Task(Project_id INTEGER, Step_id INTEGER, Task_id INTEGER PRIMARY KEY  NOT NULL  UNIQUE , Task_name VARCHAR,Task_description VARCHAR, Task_input_type VARCHAR, Task_input_labe VARCHAR, Task_input_optionss BLOB, DocAssets BLOB)";
        sqlite3_stmt *insertStatment;
        if (sqlite3_prepare_v2(database, sqlName, -1, &insertStatment, NULL) != SQLITE_OK)
        {
            NSAssert1(0, @"Error: failed to prepare statement with message '%s'.", sqlite3_errmsg(database));
        }
        
        sqlite3_bind_int(insertStatment,  1, [taskData.projectId integerValue]);
        sqlite3_bind_int(insertStatment,  2, [taskData.taskId integerValue]);
        sqlite3_bind_int(insertStatment,  3, [taskData.stepsId integerValue]);
        
        if(![taskData.taskName isKindOfClass: [NSNull class]])
        sqlite3_bind_text(insertStatment, 4, [taskData.taskName UTF8String], -1, SQLITE_TRANSIENT);
        
        if(![taskData.taskDescription isKindOfClass: [NSNull class]])
        sqlite3_bind_text(insertStatment, 5, [taskData.taskDescription UTF8String], -1, SQLITE_TRANSIENT);
        
        if(![taskData.input_type isKindOfClass: [NSNull class]])
        sqlite3_bind_text(insertStatment, 6, [taskData.input_type UTF8String], -1, SQLITE_TRANSIENT);
        
        if(![taskData.input_label isKindOfClass: [NSNull class]])
        sqlite3_bind_text(insertStatment, 7, [taskData.input_label UTF8String], -1, SQLITE_TRANSIENT);
       
        
        if(![taskData.input_options isKindOfClass: [NSNull class]]){
        NSData *theDictionaryData = [NSKeyedArchiver archivedDataWithRootObject:taskData.input_options];
        sqlite3_bind_blob(insertStatment, 8, (__bridge const void *)(theDictionaryData), -1, SQLITE_TRANSIENT);
        }

        if(![taskData.taskDocAssets isKindOfClass: [NSNull class]]){
        NSData *theDictionaryData = [NSKeyedArchiver archivedDataWithRootObject:taskData.taskDocAssets];
        sqlite3_bind_blob(insertStatment, 9, (__bridge const void *)(theDictionaryData), -1, SQLITE_TRANSIENT);
        }
        
        int success11 = sqlite3_step(insertStatment);
        printf("%d",success11);
        sqlite3_reset(insertStatment);
        if (success11 == SQLITE_ERROR)
        {
            NSAssert1(0, @"Error: failed to insert into the database with message '%s'.", sqlite3_errmsg(database));
        }
        else
        {
           /* UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Added To Project table"
                                  message:@""
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
            [alert show];*/
            
            NSLog(@"Added To task table");

        }
        sqlite3_finalize(insertStatment);
    }
    
    @catch (NSException *ex) {
        @throw ex;
    }
}


@end
