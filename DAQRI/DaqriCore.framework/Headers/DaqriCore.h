//
//  DaqriCore.h
//  DaqriCore
//
//  Created by Dusten Sobotta on 11/24/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

//! Project version number for DaqriCore.
FOUNDATION_EXPORT double DaqriCoreVersionNumber;

//! Project version string for DaqriCore.
FOUNDATION_EXPORT const unsigned char DaqriCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DaqriCore/PublicHeader.h>
