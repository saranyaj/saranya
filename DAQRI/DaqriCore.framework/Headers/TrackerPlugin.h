//
//  TrackerPlugin.h
//  DaqriCore
//
//  Created by Dusten Sobotta on 12/4/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#ifndef __DaqriCore__TrackerPlugin__
#define __DaqriCore__TrackerPlugin__

#include <stdio.h>
#include <string>
#include <vector>

#if PLATFORM_IOS
    #include <OpenGLES/ES2/gl.h>
#else
    #include <GLES2/gl2.h>
#endif


#include "Types.h"

using std::string;
using std::vector;

namespace Daqri
{
	//Stores all persistent configuration for a given tracker
	struct TrackerPluginConfig
	{
		TrackerPluginConfig()
			: useLocalDataSet(false),
			  localDataSetPath(""),
			  useCloudRecognition(false),
			  cloudAccessKey(""),
			  cloudSecretKey(""),
			  maxTrackableTargets(1)
		{
			
		}
		
		bool	useLocalDataSet;
		string	localDataSetPath;
		
		bool	useCloudRecognition;
		string	cloudAccessKey;
		string	cloudSecretKey;
		
		uint8_t	maxTrackableTargets;
	};

	//Encapsulates a tracker implementation and its lifecycle hooks
	class TrackerPlugin
	{
	public:
		
		TrackerPlugin() = default;
		
		//Returns a human-readable name for this plugin.  Used for logging purposes only.
		virtual const char* GetName() { return "TrackerPlugin"; }
		
		//Lifecycle
		virtual bool Load() = 0;			//Inits the plugin
		virtual bool Start() = 0;			//Enables the plugin and starts video passthrough
		virtual bool StartTracking() = 0;	//Starts tracking/image recognition/cloud recognition
		virtual bool StopTracking() = 0;	//Stops tracking/image recognition/cloud recognition
		virtual bool Stop() = 0;			//Disables the plugin and stops video passthrough
		virtual bool UnLoad() = 0;			//De-Inits the plugin


		//Re-configures the rendering parameters to accomodate a surface with the supplied width, height, and orientation.
		//Triggers a rebuild of the projection matrix and viewport; affects the orientation of trackables pose data.
		virtual void SetSurfaceSizeAndRotation(const int width, const int height, const Orientation orientation) = 0;

		//Renders the video background.  If tracking is enabled, updates the trackables vector.
		virtual void Update(const GLuint frameBuffer, const GLuint renderBuffer) = 0;
		
		
		//Returns a projection matrix that aligns with the tracker's understanding of the world.
		virtual Matrix44F GetProjectionMatrix() const = 0;
		
		//Returns a viewport with position offsets that account for the physical camera, orientation, and surface aspect ratio.
		virtual Viewport2D GetViewport() const = 0;
		
		//Returns a vector of currently tracked objects.  Pose transforms always respect the most recent surface and orientation settings.
		virtual const vector<Trackable>& GetTrackables() const = 0;
		
		//The persistent configuration data for this TrackerPlugin.  Applied automatically throughout plugin lifecycle.
		TrackerPluginConfig Config;
		
	private:
		//Most tracker libraries are implemented with several singletons.
		//Copy and Assignement are explicitly deleted to prevent unintentional bad behavior.
		TrackerPlugin(TrackerPlugin const&) = delete;
		TrackerPlugin& operator=(const TrackerPlugin&) = delete;
	};
} // end namesapce Daqri

#endif /* defined(__DaqriCore__TrackerPlugin__) */
