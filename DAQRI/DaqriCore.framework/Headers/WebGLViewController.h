//
//  ViewController.h
//  Daqri4DLibraryExample
//
//  Created by Dusten Sobotta on 11/14/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EAGLView.h"

#import "ContentManagerIOS.h"
#include "Types.h"

using namespace Daqri;

//An OpenGLES/UIWebView hybrid.  Manages most TrackerManager lifecycle hooks automatically.
//Coordinates async loading of WebGL content.  Synchronizes rendering/state updates between
//background feed and pose data--which is propogated to WebGL
@interface DAQRI_WebGLViewController : UIViewController<UIWebViewDelegate, UpdatePoseProtocol>
{

}

@property (nonatomic, retain) NSMutableArray* videoURLs;

//Loads HTML/WebGL content from a local file on-disk.
- (void)setWebContent:(NSString*)inFilePath;

//Loads HTML/WebGL content from a file at a remote location, with the specified URL path
- (void)setRemoteWebContent:(NSString*)remoteURL;

//Applies Industrial 4D Studio Scene JSON to the current HTML/WebGL scene.
- (void)setSceneJSON:(NSString*)inScenePath;

//Clears all WebGL content from the scene
- (void)clearScene;

//Specifies which remote Audio/Video URLs in HTML/WebGL content will be loaded from local cache instead.
- (void)setRemoteVideoURLs:(NSMutableArray*) videoURLs;

//Notify when the app will pause, to ensure that OpenGL is rendering is paused at the correct time.
- (void)appWillPause;

//Notify when the app will un-pause, to ensure that OpenGL rendering is resumed at the correct time.
- (void)appWillResume;

//Callback from javascript with a json object
- (void) javascriptJSONCallback:(id) jsonObj;

//Callback from javascript with key/value pair (as a string)
- (void) javascriptEventCallbackWithKey:(NSString*)key AndValue:(NSString*) value;

@end