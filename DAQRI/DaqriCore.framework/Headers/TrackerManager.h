//
//  TrackerManager.h
//  DaqriCore
//
//  Created by Dusten Sobotta on 12/6/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#ifndef __DaqriCore__TrackerManager__
#define __DaqriCore__TrackerManager__

#include <stdio.h>

#include <string>
#include <vector>

#if PLATFORM_IOS
    #include <OpenGLES/ES2/gl.h>
#else
    #include <GLES2/gl2.h>
#endif

#include "Types.h"

using std::string;
using std::vector;

namespace Daqri
{

class TrackerPlugin;

//A C++ front-end for all interaction with TrackerPlugins.  Manages TrackerPlugin states and lifecycles.
class TrackerManager
{
public:
	//Returns a reference to the TrackerManager singleton.
	static TrackerManager& GetInstance();
	
	//Stops and unloads the current TrackerPlugin, then loads the supplied one.  Returns true if successful.
	bool EnablePlugin(TrackerPlugin* trackerPlugin);
	
	//Returns the current/loaded TrackerPlugin
	const TrackerPlugin* GetCurrentTracker();

	//Starts rendering the camera feed with the current TrackerPlugin.
	//If tracking is enabled, this also starts tracking on the current TrackerPlugin.
	bool StartCamera();
	
	//Stops rendering the camera feed with the current TrackerPlugin.
	//If tracking is enabled, this also stops tracking on the current TrackerPlugin.
	bool StopCamera();
	
	//Enables tracking on the current plugin (and any subsequently loaded TrackerPlugins) when the camera is active.
	bool EnableTracking();
	
	//Disables tracking on the current plugin (and any subsequently loaded TrackerPlugins).
	bool DisableTracking();
	
	//Returns whether tracking is enabled.
	//NOTE: This does not refect the internal state of the current TrackerPlugin;
	//rather, it reflects whether tracking is automatically enabled/disabled in StartCamera/StopCamera
	bool IsTrackingEnabled() const;

	//Sets the desired OpenGL surface width, height, and orientation.
	//These parameters are applied to the current TrackerPlugin immediately, and to all subsequently-loaded TrackerPlugins
	void SetSurfaceSizeAndRotation(const int width, const int height, const Orientation orientation);
	
	
	//renders the video background, and updates the vector of trackables in the scene
	void Update(const GLuint frameBuffer, const GLuint renderBuffer);
	
	//Returns a projection matrix that aligns with the current TrackerPlugin's understanding of the world.
	Matrix44F GetProjectionMatrix() const;
	
	//Returns a viewport with position offsets that account for the physical camera, orientation, and surface aspect ratio.
	Viewport2D GetViewport() const;
	
	//Returns a vector of the current TrackerPlugin's tracked objects.
	//Pose transforms always respect the most recent surface and orientation settings.
	const vector<Trackable>& GetTrackables() const;

private:
	TrackerManager();
	TrackerManager(TrackerManager const&) = delete;
	TrackerManager& operator=(const TrackerManager&) = delete;


	TrackerPlugin*	currentPlugin;
	int				viewWidth;
	int				viewHeight;
	Orientation		viewOrientation;
	
	bool			cameraActive;
	bool			trackingEnabled;
};
	
	
} //end namespace Daqri

#endif /* defined(__DaqriCore__TrackerManager__) */
