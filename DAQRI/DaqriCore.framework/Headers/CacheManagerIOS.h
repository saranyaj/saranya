//
//  CacheManager.h
//  DaqriCore
//
//  Created by Apar Suri on 12/1/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#ifndef DaqriCore_CacheManager_h
#define DaqriCore_CacheManager_h
#import "TMCache.h"

//This is the cacheManager interface. It is a wrapper over TMCache.
@interface DAQRI_CacheManager : NSObject {
    
}
//maintains the rootPath for the cache on device
@property (nonatomic, retain) NSString* rootPath;

//init method which only sets the cache name. Parameters -
    //NSString name - name of the cache, default being DaqriCache
- (id) initWithCacheName:(NSString*) name;

//init method which only sets the root path. Parameters -
    //NSString path - root path for the cache, default being the Caches folder given by NSCachesDirectory
- (id) initWithRootPath: (NSString*) path;

//init method which sets the name and rootPath
    //NSString name - name of the cache
    //NSString path - root path for the cache
- (id) initWithCacheName:(NSString*) name rootPath:(NSString*) path;

//init method which sets the name, rootPath and the byte limit for the cache
    //NSString name - name of the cache
    //NSString path - root path for the cache
    //NSUInteger byteLimit - specify the byte limit for the cache; there is no limit by default
- (id) initWithCacheName:(NSString*) name rootPath:(NSString*) path byteLimit:(NSUInteger) byteLimit;

//init method which sets the name, rootPath, byte limit and the age limit
    //NSString name - name of the cache
    //NSString path - root path for the cache
    //NSUInteger byteLimit - specify the byte limit for the cache.
    //NSTimeInterval ageLimit - specify the time limit after which the cache is no longer valid; there is no expiration time by default
- (id) initWithCacheName:(NSString*) name rootPath:(NSString*) path byteLimit:(NSUInteger) byteLimit ageLimit:(NSTimeInterval) ageLimit;

//This method sets the object with the specified key. The paramaters are -
    //NSString* key - this is the key for cached object
    //id<NSCoding> object - this is object being cached. Make sure to make it NSCoding compliant. To see an example check DAQRI_DownloadData
- (void) setToCacheWithKey:(NSString*) key AndObject:(id<NSCoding>)object;

//This method get the data from the cache with the specified key. The parameters are -
    //return id<NSCoding> - returns the object from the cache with the specified key. Note that it gets NSCoding compliant object only. If the data is not present for the specific key or if it is not NSCoding compliant, the cache will return nil.
    //NSString* key - get the object with the specific key.
- (id<NSCoding>) getFromCacheWithKey:(NSString*) key;

//This removes the object from cache with specified key. The parameter is -
    //NSString* key - the key which specifies the object to be removed from cache
- (void) removeFromCache:(NSString*) key;

//This method clears the complete cache. Note that it puts the data in the TrashURL and possibly clears it.
- (void) clearCache;

@end

#endif
