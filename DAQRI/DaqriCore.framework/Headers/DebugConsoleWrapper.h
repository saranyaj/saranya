//
//  DebugConsoleWrapper.h
//  DaqriCore
//
//  Created by Apar Suri on 11/26/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#ifndef __DaqriCore__DebugConsoleWrapper__
#define __DaqriCore__DebugConsoleWrapper__

#include <stdio.h>
namespace Daqri
{
    void DebugConsoleInitialize();
    void LogConsole(const char *);
}

#endif /* defined(__DaqriCore__DebugConsoleWrapper__) */
