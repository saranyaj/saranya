//
//  LogIOS.h
//  DaqriCore
//
//  Created by Dusten Sobotta on 12/1/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#ifndef DaqriCore_LogIOS_h
#define DaqriCore_LogIOS_h

#include <cstdarg>

namespace Daqri
{
	
struct LogIOSImpl
{
	void LogVerbose(const char* format, va_list args);
	void LogDebug(const char* format, va_list args);
	void LogWarning(const char* format, va_list args);
	void LogError(const char* format, va_list args);
};
	
} //end namespace Daqri

#endif
