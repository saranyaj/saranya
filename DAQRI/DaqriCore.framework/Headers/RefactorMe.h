//
//  RefactorMe.h
//  DaqriCore
//
//  Created by Dusten Sobotta on 12/22/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#ifndef DaqriCore_RefactorMe_h
#define DaqriCore_RefactorMe_h

namespace Daqri
{
	namespace RefactorMe
	{
		void OnRender();
		void SetRenderFrameQCAREnabled(const bool enabled);
	} //end namespace RefactorMe
} //end namespace Daqri

#endif
