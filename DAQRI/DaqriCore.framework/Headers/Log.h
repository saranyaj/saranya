//
//  Log.h
//  DaqriCore
//
//  Created by Dusten Sobotta on 11/24/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#ifndef __DaqriCore__Log__
#define __DaqriCore__Log__

#include <stdio.h>
#include <string>

using std::string;

namespace Daqri
{
class Log
{

public:
	static void Verbose(const char* format, ...);
	static void Debug(const char* format, ...);
	static void Warning(const char* format, ...);
	static void Error(const char* format, ...);
}; //end class Log
	
} //end namespace Daqri

#endif /* defined(__DaqriCore__Log__) */
