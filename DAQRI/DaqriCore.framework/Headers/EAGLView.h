//
//  UIView_EAGLView.h
//  Daqri4DLibraryExample
//
//  Created by Dusten Sobotta on 11/19/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

@protocol UIGLViewProtocol
// iOS: Called by QCAR to render the current frame
- (void)renderFrameQCAR;
@end

@protocol UpdatePoseProtocol
- (void)updatePose;
@end

#import <UIKit/UIKit.h>

//A OpenGL ES 2.0 view responsible for rendering the background feed and driving pose updates for the current
//TrackerPlugin.  There should be no need to interact with this class directly or subclass it.
//DAQRI_WebGLViewController manages all interaction with OpenGL ES through an instance of this class.
@interface DAQRI_EAGLView : UIView <UIGLViewProtocol>
{

}

//initialization must be kicked off from the main thread.
- (id)initWithFrame:(CGRect)frame;

//Sets the active pose listener (a DAQRI_WebGLViewController), which will recieve all trackable/pose updates.
- (void)setPoseListener:(NSObject<UpdatePoseProtocol>*)inPoseListener;

//wraps up any pending GLES commands.
//Ensures that render loop has been stopped before the app potentially enters the background.
- (void)finishOpenGLESCommands;

//frees easily-recreated GLES resources.
- (void)freeOpenGLESResources;


//pauses rendering of background feed
- (void)pause;

//resumes rendering of background feed
- (void)resume;

//- (void) setOffTargetTrackingMode:(BOOL) enabled;
@end