//
//  DAQRI_TrackerManager.h
//  DaqriCore
//
//  Created by Dusten Sobotta on 12/7/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "Types.h"

#include <OpenGLES/ES2/gl.h>
#include <UIKit/UIKit.h>

namespace Daqri
{
	class TrackerPlugin;
}

using namespace Daqri;

//An Objective-C front-end for all interaction with TrackerPlugins.  Manages TrackerPlugin states and lifecycles.
@interface DAQRI_TrackerManager : NSObject

//Stops and unloads the current TrackerPlugin, then loads the supplied one.
+ (void) enablePlugin:(TrackerPlugin*)trackerPlugin;

//Returns the current/loaded TrackerPlugin
+ (const TrackerPlugin*) getCurrentTracker;

//Starts rendering the camera feed with the current TrackerPlugin.
//If tracking is enabled, this also starts tracking on the current TrackerPlugin.
+ (bool) startCamera;

//Stops rendering the camera feed with the current TrackerPlugin.
//If tracking is enabled, this also stops tracking on the current TrackerPlugin.
+ (bool) stopCamera;

//Enables tracking on the current plugin (and any subsequently loaded TrackerPlugins) when the camera is active.
+ (bool) enableTracking;

//Disables tracking on the current plugin (and any subsequently loaded TrackerPlugins).
+ (bool) disableTracking;

//Returns whether tracking is enabled.
//NOTE: This does not refect the internal state of the current TrackerPlugin;
//rather, it reflects whether tracking is automatically enabled/disabled in StartCamera/StopCamera
+ (bool) isTrackingEnabled;

//Sets the desired OpenGL surface width, height, and orientation.
//These parameters are applied to the current TrackerPlugin immediately, and to all subsequently-loaded TrackerPlugins
+ (void) setSurfaceWidth:(int)width
				  height:(int)height
			 orientation:(UIInterfaceOrientation)orientation;

//renders the video background, and updates the vector of trackables in the scene
+ (void) updateWithFrameBuffer:(GLuint)framebuffer andRenderBuffer:(GLuint) renderbuffer;

//Returns a projection matrix that aligns with the current TrackerPlugin's understanding of the world.
+ (Matrix44F) getProjectionMatrix;

//Returns a viewport with position offsets that account for the physical camera, orientation, and surface aspect ratio.
+ (Viewport2D) getViewport;

//Returns a vector of the current TrackerPlugin's tracked objects.
//Pose transforms always respect the most recent surface and orientation settings.
+ (NSArray*) getTrackables;

@end
