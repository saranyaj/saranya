//
//  Types.h
//  DaqriCore
//
//  Created by Dusten Sobotta on 11/25/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#ifndef DaqriCore_Types_h
#define DaqriCore_Types_h

#include <functional>

namespace Daqri
{

enum class Orientation
{
	ROTATE_90 = 0,
	ROTATE_180 = 1,
	ROTATE_270 = 2,
	ROTATE_0 = 3
};

struct Matrix44F
{
	Matrix44F() = default;
	float data[4*4];
};

struct Trackable
{
	Trackable() = default;
	Trackable(const int inID, const char* inName, const Matrix44F& inTransform)
	:	id(inID),
	name(inName),
	transform(inTransform)
	{
		
	}
	
	int			id;
	const char*	name;
	Matrix44F	transform;
};

struct Viewport2D
{
	int posX;
	int posY;
	int sizeX;
	int sizeY;
};
    
struct Calibration
{
    float width, height;
    float focalLengthX, focalLengthY;
    float principalPointX, principalPointY;
    float fovDegrees;
};

}
#endif
