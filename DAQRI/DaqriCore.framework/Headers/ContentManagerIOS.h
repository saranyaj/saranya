//
//  ContentManager.h
//  DaqriCore
//
//  Created by Apar Suri on 12/1/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#ifndef DaqriCore_ContentManager_h
#define DaqriCore_ContentManager_h
#import "CacheManagerIOS.h"
#import "DownloadManager.h"

//The content manager is the basic implementation of the smart downloader
//Make sure to set all the base URLs correctly by calling - (void)setGlobalsWithBaseURL:enterpriseJSONURL:projectBaseURL:taskBaseURL:taskBaseURL;
//Currently it has the ability to
// - Download/Cache single file
// - Force Download single file (even if the data is cached)
// - Getting content for a scene (which is associated with a specific task)
// - Getting content for a complete enterprise
// - Getting a project list from target json
// - locking/unlocking assets in cache which allows them to not get cleared
@interface DAQRI_ContentManager : NSObject {
}
//singleton object
+(id) sharedManager;

//normal cache object, used for most caching puporses
@property (nonatomic, retain) DAQRI_CacheManager* cache;
//bundled cache is used for caching the bundled data; Currently disabled.
@property (nonatomic, retain) DAQRI_CacheManager* bundledCache;
//Dictionary which saves the callback info about the asset being downloaded
@property (nonatomic, retain) NSMutableDictionary* contentCallbacks;
//Assets which are locked are not cleared by the cache
@property (nonatomic, retain) NSMutableDictionary* lockedAssets;
//Assets which are unlocked are cleared from the cache if cache clear is called
@property (nonatomic, retain) NSMutableDictionary* unlockedAssets;
//This dictionary maintains if all the project downloads have completed
@property (nonatomic, retain) NSMutableDictionary* projectDownloadStatus;
//baseURL is the base URL for the enterprise. This needs to be set by the application
@property (nonatomic, retain) NSString* baseURL;
//This is the enterprise JSON URL which is generally based off of the baseURL
@property (nonatomic, retain) NSString* enterpriseJSONURL;
//This is the project base URL which is again based off of the baseURL. The project id is generally appended as the lastpathcomponent to get the project URL
@property (nonatomic, retain) NSString* projectBaseURL;
//This is the task base URL which is again based off of the baseURL. The task id is generally appended as the lastpathcomponent to get the project URL
@property (nonatomic, retain) NSString* taskBaseURL;
//@property (nonatomic, retain) NSMutableDictionary* targetData;

//This method forces the download for a specific URL, even if it's in cache. The parameters are -
    //NSString* urlString - the URL for which the the forced data is called
    //id receiver - the receiver object which get the callback whether the download failed or succeeded
    //SEL success - the success selector on the receiver object is called when the download completes and succeeds. Note that before it is called, the object is saved into the cache. The selector has DAQRI_DownloadData* as parameter
    //SEL failure - the failure selector on the receiver object when the download fails. The selector has DAQRI_DownloadData* as parameter
- (void) getForceDataFromURL:(NSString*) urlString withReceiver:(id) receiver SuccessCallback:(SEL) success AndFailureCallback:(SEL) failure;

//This method downloads data from a specific URL, if the data is in cache, it fetches from the cache and returns it.The parameters are -
    //NSString* urlString - the URL for which the the forced data is called
    //id receiver - the receiver object which get the callback whether the download failed or succeeded
    //SEL success - the success selector on the receiver object is called when the download completes and succeeds. Note that before it is called, the object is saved into the cache. The selector has DAQRI_DownloadData* as parameter
    //SEL failure - the failure selector on the receiver object when the download fails. The selector has DAQRI_DownloadData* as parameter
- (void) getDataFromURL:(NSString*) urlString withReceiver:(id) receiver SuccessCallback:(SEL) success AndFailureCallback:(SEL) failure;

//- (void) getLocalDataFromPath: (NSString*) path withReceiver:(id) object SuccessCallback:(SEL) success AndFailureCallback:(SEL) failure;
//- (void) setContentForTarget:(NSString *)target withPath:(NSString*) path Reciever:(id) receiver SuccessCallback:(SEL) success AndFailureCallback:(SEL) failure;
//- (void) getContentForTarget:(NSString*) urlString withPath:(NSString*) path withReceiver:(id) object SuccessCallback:(SEL) success AndFailureCallback:(SEL) failure;

//This method gets all the content for a scene. A scene is generally associated with a task. The paramaters to the method are -
    //id sceneData - this is the JSON which is associated with the scene/task.
    //id receiver - the receiver object which get the callback whether all the content for the scene failed or succeeded
    //SEL success - the success selector on the receiver object is called when the content for the scene completes and succeeds. Note that before it is called, all the data is saved into the cache. The selector has DAQRI_SceneParsedData* as parameter
    //SEL failure - the failure selector on the receiver object when the scene content download fails. The selector has DAQRI_DownloadData* as parameter
- (void) getContentForScene:(id)sceneData Receiver:(id) receiver SuccessCallback:(SEL) success AndFailureCallback:(SEL) failure;

//This method gets all the data associated with a particular enterprise. The parameters are -
    //id enterpriseData - this is the enterprise JSON which is associated with a particular enterprise
    //id receiver - the receiver object which get the callback whether all the content for the enterprise failed or succeeded
    //SEL success - the success selector on the receiver object is called when the content for the enterprise completes and succeeds. Note that before it is called, all the data is saved into the cache. The selector has DAQRI_ProjectContent* as parameter
    //SEL failure - the failure selector on the receiver object when the scene content download fails. The selector has DAQRI_DownloadData* as parameter
- (void) getContentForProjects:(id)enterpriseData Receiver:(id) receiver SuccessCallback:(SEL) success AndFailureCallback:(SEL) failure;

//This method gets the list of all the projects in an enterprise. The parameters are -
    //NSData* enterpriseData - this is the enterprise JSON which is associated with a particular enterprise
//The following parameters are optional when calling from an application perspective since the callback may not be necessary. It is used for ContentManager because it can get the list, populate some internal data structures and start the download for the projects. The implementation of this can be seen in getContentForProjects which calls this method and populates the data structures for asynchronous download of project data for each of the projects in an enterprise
    //id receiver - the receiver object which get the callback whether all the content for the enterprise failed or succeeded
    //SEL success - the success selector on the receiver object is called when the content for the enterprise completes and succeeds. Note that before it is called, all the data is saved into the cache. The selector has DAQRI_ProjectContent* as parameter
    //SEL failure - the failure selector on the receiver object when the scene content download fails. The selector has DAQRI_DownloadData* as parameter
- (NSMutableArray*) getProjectListFromJSON:(NSData*) enterpriseData receiver:(id) receiver success:(SEL) success failure:(SEL) failure;

//gets the bundled cache path location on device
- (NSString*) getBundledCachePath;

//gets the bundled cache URL location on device
- (NSURL*) getBundledCacheURL;

//This method clears of the cache of the objects which are unlocked. It also cleans up all the files in the tmp folder where the downloads are done. (not folders in tmp folder are cleared because partial downloads are in the Incomplete folder and there is also the TMCache trash folder in the tmp directory)
- (void) clearCache;

//This method locks the assets in cache so that they dont get cleared when the cache is cleared
- (void) lockAssetsInCache:(NSArray*) arrayOfAssets;

//This method unlocks the assets so that they DO get cleared when the cache is cleared
- (void) unlockAssetsInCache:(NSArray*) arrayOfAssets;

//This method sets the urls for the enterprise/projects/tasks.
//THIS NEEDS TO GET CALLED WITH THE CORRECT URLS FROM THE APP
//The parameters are -
    //NSString* baseURL - the base URL for the app like - https://i4dsdemo.daqri.com/mobile/v1/i4ds/
    //NSString* enterpriseJSONURL - the enterprise JSON URL (which is used for loading the enterprise) like - https://i4dsdemo.daqri.com/mobile/v1/i4ds/enterprise/14?sort=created_at
    //NSString* projectBaseURL - the project Base URL (which is appended with the project id to get the project JSON URL) like - http://staging1.daqri.com/mobile/v1/i4ds/project/
    //NSString* taskBaseURL - the task base URL (which is appended with the task id to get the task JSON URL) like - https://i4dsdemo.daqri.com/mobile/v1/i4ds/task/
- (void) setGlobalsWithBaseURL:(NSString*) baseURL enterpriseJSONURL:(NSString*) enterpriseJSONURL projectBaseURL:(NSString*) projectBaseURL taskBaseURL:(NSString*) taskBaseURL;

@end

//The DAQRI_ContentCallbackData contains the information about the data callback when the data gets downloaded
//This class is generally not required app side
@interface DAQRI_ContentCallbackData : NSObject {
    SEL success;
    SEL failure;
}
@property (nonatomic, retain) NSString* key;
@property (nonatomic, retain) id receiver;

-(void) setSuccessSelector:(SEL) succ;
-(SEL) getSuccessSelector;
-(void) setFailureSelector:(SEL) fail;
-(SEL) getFailureSelector;
@end

//The DAQRI_URLProtocolIntercept is the class which derives from NSURLProtocol. This intercepts any URL requests and sends any cached data if we have it. If not, the request goes through normally and eventually gets cached after it gets downloaded
@interface DAQRI_URLProtocolIntercept : NSURLProtocol
@end

//@interface TargetContent : NSObject {
//    SEL receiverSuccess;
//    SEL receiverFailure;
//    SEL success;
//    SEL failure;
//}
//- (id)initWithName:(NSString*) targetName withReceiver:(id) receiver Success:(SEL)succ failure:(SEL)fail;
//@property (nonatomic, retain) id receiver;
//@property (nonatomic, retain) NSString* targetName;
//@property (nonatomic, retain) NSMutableDictionary* targetDownloads;
//@property (nonatomic, retain) DAQRI_CacheManager* targetCache;
//
//- (void) copyTargetDataToCache;
//- (void) targetRemoteDataDownloaded:(DownloadMediaData*) obj;
//- (void) targetLocalDataDownloaded:(DownloadMediaData*) obj;
//- (void) checkIfAllDownloadsCompleted;
//- (SEL) getSuccessSelector;
//- (SEL) getFailureSelector;
//
//@end

//The DAQRI_SceneParsedData interface contains the data which is used for loading a scene/task.
//This data structure is used in success callback for getSceneContent. The usage can be seen in WebGLViewController.mm -- sceneDataDownloaded
@interface DAQRI_SceneParsedData : NSObject

//sceneJson contains the scene json which needs to be forwarded to the threejs framework (generally to scene.html)
@property (nonatomic, retain) NSString* sceneJSON;
//assetJson is the list of assets (in JSON format) which needs to be forwarded to the threejs framework
@property (nonatomic, retain) NSString* assetJSON;

@end

//The DAQRI_SceneContent has the scene content which is used when downloading scene/task data.
//This maintains when all the scene assets are downloads, once they are, the callback on the receiver is called
@interface DAQRI_SceneContent : NSObject {
    //the callback selector for success to the receiver when ALL the scene assets are downloaded
    SEL receiverSuccess;
    //the callback selector for failure to the receiver when any of the assets for the scene fails
    SEL receiverFailure;
    //the callback selector when any asset gets downloaded
    SEL success;
    //the callback selector when any of assets fail to download
    SEL failure;
}
//parseAndReplaceSceneAssets method is a hacky method which replaces the occurence of video/audio files in the assets which are present in cache from http/https to localdata://
//This is needed as a workaround because ios requests for video/audio do not go through NSURLProtocol so we cannot replace cached data in the response for these file types. Note that this method is not fully implemented for audio formats
- (void) parseAndReplaceSceneAssets;

//This is the scene JSON
@property (nonatomic, retain) NSData* sceneJSONData;
//This is the receiver object which receives the callback when all the scene data gets downloaded successfully or if any of the downloads fail
@property (nonatomic, retain) id receiver;
//This is the dictionary of scene assets downloads which maintains if the download for the asset if completed or not
@property (nonatomic, retain) NSMutableDictionary* sceneDownloads;
//This is data which is sent back to the receiver when all the assets are downloaded and succeed.
@property (nonatomic, retain) DAQRI_SceneParsedData* sceneData;

//This is the init method for DAQRI_SceneContent.The parameters are -
    //NSData* sceneJSONData - this is the scene(/task) JSON for the particular scene
    //id receiver - this is the receiver which receives the callback when the download for all the assets for the scene succeed or fail
    //SEL succ - success callback selector when the download for all the assets for the scene succeed. This is called on the receiver
    //SEL fail - failure callback selector when the download fails for any of the assets for the scene. This is also called on the receiver
- (id)initWithSceneJSONData:(NSData*) sceneJSONData Receiver:(id) receiver Success:(SEL)succ failure:(SEL)fail;

//This method checks if all the downloads for the scene have completed
- (void) checkIfAllDownloadsCompleted;

//- (void) sceneRemoteDataDownloaded:(DownloadMediaData*) obj;

//This method gets the selector for success for each asset (not for when all the assets have succeeded)
- (SEL) getSuccessSelector;
//This method gets the selector for failure for each asset.
- (SEL) getFailureSelector;
@end

//DAQRI_ProjectContent has the project content when the project data is getting downloaded. It maintains the status of all the project assets and has a callback when all the assets pertaining to the project are completed or if any of them fail. The usage can be scene in ProjectDataVC.mm -- enterpriseDownloadCompleted
@interface DAQRI_ProjectContent : NSObject {
    //The selector which gets called on the receiver when the download for assets of the enteprise succeed. The parameter for this selector is DAQRI_ProjectContent
    SEL receiverSuccess;
    //The selector which gets called on the receiver when any of the downloads for the enterprise fail.
    SEL receiverFailure;
    //This selector gets called when the project JSON download succeeds
    SEL projectJsonSuccess;
    //This selector gets called when the project JSON download fails
    SEL projectJsonFailure;
}
//This contains the project JSON
@property (nonatomic, retain) NSData* projectJSONData;
//This is the receiver on which the success and failure callbacks are called when all the assets pertaining to the enterprise succeed or if any of them fail
@property (nonatomic, retain) id receiver;
//This dictionary maintains the status of list of all the downloads for the project
@property (nonatomic, retain) NSMutableDictionary* projectDownloads;
//This is the project id from the project JSON
@property (nonatomic, retain) NSNumber* projectId;
//This is the enterprise id from the project JSON
@property (nonatomic, retain) NSNumber* enterpriseId;
//This is the project name from the project JSON
@property (nonatomic, retain) NSString* projectName;

//init method for DAQRI_ProjectContent. The parameters are -
    //id receiver - this is the receiver object which gets called when all the downloads for the project are completed or if any of them fail
    //SEL succ - this is the success selector which gets called on the receiver object when all the downloads succeed pertaining to the enterprise
    //SEL fail - this is the failure selector which gets called on the receiver object when any of the downloads for the enterprise fail
- (id) initWithReceiver:(id) receiver success:(SEL) succ failure:(SEL) fail;
//This is the method which checks when all the downloads pertaining to the enterprise have completed
- (void) checkIfAllDownloadsCompleted;
//This returns the selector for download success of the project JSON
- (SEL) getProjectJsonSuccessSelector;
//This returns the selector for download failure of project JSON
- (SEL) getProjectJsonFailureSelector;

@end

#endif
