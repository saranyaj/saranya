//
//  DAQRI_DebugConsoleLog.h
//  DaqriCore
//
//  Created by Apar Suri on 11/25/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//
#import <UIKit/UIKit.h>

#ifndef DaqriCore_DebugConsoleLog_h
#define DaqriCore_DebugConsoleLog_h

#define  LOGCONSOLE(PR)               {NSLog(@"%@",PR);dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{ [[DAQRI_DebugConsoleLog sharedManager] LogDebug:PR] ;  }); }

@interface DAQRI_DebugConsoleLog : NSObject {

}
+ (id)sharedManager;
- (void) initialize;
- (UITextView*) textView;
- (void) LogConsole:(NSString*) logMessage;
- (void) LogDebug:(NSString*) logMessage;
- (void) writetoUI;
@end

#endif
