//
//  DownloadManager.h
//  DaqriCore
//
//  Created by Apar Suri on 11/25/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//
#import "AFNetworking.h"
#import <UIKit/UIKit.h>

#ifndef DaqriCore_DownloadManager_h
#define DaqriCore_DownloadManager_h

//This is the data which is returned when the data is downloaded. It is NSCoding compliant so that it can be cached
@interface DAQRI_DownloadData : NSObject <NSCoding>

//This property specifies the url of the data
@property (nonatomic, retain) NSString* urlString;
//This property specifies the responseData which was returned as HTTP response
@property (nonatomic, retain) NSMutableDictionary* reponseData;
//This property specifies the file data for the downloaded content.
@property (nonatomic, retain) NSData* fileData;
//This property specifies the error if the download failed (in which case fileData will be empty)
@property (nonatomic, retain) NSError *error;
//This property specifies the file size
@property (nonatomic, retain) NSNumber* fileSizeNumber;
//This property specifies the file path in the tmp directory (download folder)
@property (nonatomic, retain) NSString* filePath;
//This property specifies the reponse. This may get deprecated since there is problem witl deserializing it
@property (nonatomic, retain) NSHTTPURLResponse* response;
//This property specifies the statusCode of the http response
@property (nonatomic) int stausCode;
@end

//This interface is used for saving the call back info about the DownloadData when it fails because of network unavailabilty
@interface DAQRI_DownloadCallbackData : NSObject {
    //the selector which gets called on success; parameter for this selector is DAQRI_DownloadData
    SEL success;
    //the selector which gets called on failure; parameter for this selector is DAQRI_DownloadData
    SEL failure;
    //the flag which specifies if the download should be overwritten. If this flag is not true, and the file already exists in the download folder, the download just returns without calling success or failure. This may cause issues in downloads getting hung. This should ideally never happen because it means the downloader has this data and the cache does not.
    BOOL shouldOverwrite;
}

//this property saves the url string for the download data
@property (nonatomic, retain) NSString* urlString;
//the receiver object on which the success/failure selectors are called based on whether the download failed or succeeded
@property (nonatomic, retain) id receiver;
//this property saves the operation so that it can be removed from the download manager dictionary of operations once the download completes or fails.
@property (nonatomic, retain) AFHTTPRequestOperation* operation;

//sets the success selector with the SEL parameter
-(void) setSuccessSelector:(SEL) succ;

//returns the success selector
-(SEL) getSuccessSelector;

//sets the failure selectors with the SEL parameter
-(void) setFailureSelector:(SEL) fail;

//returns the failure selector
-(SEL) getFailureSelector;

//sets the should overwrite flag
-(void) setShouldOverwrite:(BOOL) shouldOverwrite;

//gets the should overwite flag
-(BOOL) getShouldOverwrite;
@end

@interface DAQRI_DownloadManager : NSObject
//this is the singleton shared object. Note that there is network reachability code inside the sharedManager which keeps monitoring for network connectivity.
+(id) sharedManager;

//This is the operations dictionary with url as the key and value being the operation. If the download fails, based on the return code (-1009, -1005 for network unavailability), it may get added to failed and resumed later
@property (nonatomic, retain) NSMutableDictionary* operations;
//This is the failed operations dictionary with url as the key and value being the operation. This is used when a download fails because of network issues and gets retried when the network connection comes back (used in reTryFailedDownloads)
@property (nonatomic, retain) NSMutableDictionary* failedOperations;

//This is the method used to download a url. The parameters are -
    //NSString urlString - the url string which needs to be downloaded
    //id receiver - the object which receives the callback selector, either success or failure based on whether the download failed or succeeded
    //SEL success - the success callback which gets called on the receiver. The parameter for the callback is DAQRI_DownloadData
    //SEL failure - the failure callback which gets called on the receiver if the download fails. The parameter for the callback is DAQRI_DownloadData. Note that if the download fails because of -1009 or 1005, it DOES NOT get call failure and instead gets added to failed downloads dictionary to be retried when the network connectivity is again established
-(void) startDownloadWithURL:(NSString *)urlString object:(id)receiver SuccessCallback:(SEL)success FailureCallbak:(SEL)failure;

//This is the method used to download a url. This method removes the operation if it's already present in failed and/or downloads dictionary and then calls the downloadWithURL method. The parameters are -
    //NSString urlString - the url string which needs to be downloaded
    //id receiver - the object which receives the callback selector, either success or failure based on whether the download failed or succeeded
    //SEL success - the success callback which gets called on the receiver. The parameter for the callback is DAQRI_DownloadData
    //SEL failure - the failure callback which gets called on the receiver if the download fails. The parameter for the callback is DAQRI_DownloadData. Note that if the download fails because of -1009 or 1005, it DOES NOT get call failure and instead gets added to failed downloads dictionary to be retried when the network connectivity is again established
    //BOOL shouldOverwrite - it specifies whether the to be downloaded should be overwritten or not. If this flag is not true, and the file already exists in the download folder, the download just returns without calling success or failure. This may cause issues in downloads getting hung. This should ideally never happen because it means the downloader has this data and the cache does not.
-(void) startDownloadWithURL:(NSString *)urlString object:(id)receiver SuccessCallback:(SEL)success FailureCallbak:(SEL)failure AndOverwrite:(BOOL) shouldOverwrite;

//This is the method used to download a url. This method just gets called from the startDownloadWithURL method and has all the downloading logic along with the success/failure/progress blocks. One thing to note is that if a file comes back with 500 code, it is for some reason considered successful by AFNetworking. This method handles that and still sends back a failure. The parameters are -
    //NSString urlString - the url string which needs to be downloaded
    //id receiver - the object which receives the callback selector, either success or failure based on whether the download failed or succeeded
    //SEL success - the success callback which gets called on the receiver. The parameter for the callback is DAQRI_DownloadData
    //SEL failure - the failure callback which gets called on the receiver if the download fails. The parameter for the callback is DAQRI_DownloadData. Note that if the download fails because of -1009 or 1005, it DOES NOT get call failure and instead gets added to failed downloads dictionary to be retried when the network connectivity is again established
    //BOOL shouldOverwrite - it specifies whether the to be downloaded should be overwritten or not. If this flag is not true, and the file already exists in the download folder, the download just returns without calling success or failure. This may cause issues in downloads getting hung. This should ideally never happen because it means the downloader has this data and the cache does not.
-(void) downloadWithURL:(NSString*)urlString object:(id)receiver success:(SEL) success failure:(SEL)failure AndOverwrite:(BOOL) shouldOverwrite;

//This method is called for pausing download with the specified url string
-(void) pauseDownload:(NSString*) urlString;

//This method resumes the download specified by the url string parameter which was previously paused
-(void) resumeDownload:(NSString*) urlString;

//This method cancels the download specified by the url string.
-(void) cancelDownload:(NSString*) urlString;

//This method checks if the local cached content is new data or old data based on the remote URL ASYNCHONOUSLY. Note that currently this method only checks against content-lenght. The parameters are -
    //NSString* urlString - the remote URL string
    //DAQRI_DownloadData* cachedData - the local cached content
    //id receiver - the receiver object which gets the selector call back
    //SEL selector - the selector call back which gets called on the receiver. It has a BOOL parameter.
-(void) doesRemoteURLHaveNewDataAsync:(NSString*) urlString withLocalData:(DAQRI_DownloadData*) cachedData receiver:(id) receiver selector:(SEL) selector;

//This method checks if the local cached content is new data or old data based on the remote URL SYNCHONOUSLY. Note that currently this method only checks against content-lenght. The parameters are -
    //NSString* urlString - the remote URL string
    //DAQRI_DownloadData* cachedData - the local cached content
    //id receiver - the receiver object which gets the selector call back
    //SEL selector - the selector call back which gets called on the receiver. It has a BOOL parameter.
-(BOOL) doesRemoteURLHaveNewDataSync:(NSString*) urlString withLocalData:(DAQRI_DownloadData*) cachedData;

//This method is called when the network is available again. If there were any failed downloads when the network went down, they are retried via this method.
-(void) reTryFailedDownloads;

//This method clears all the files in the tmp folder. Note that it DOES NOT delete any folders or the content inside them. This means it does not delete the Incomplete folder (since they may be downloads currently happening) and the trash folder for TMCache.
-(void) clearTmp;

@end

#endif
