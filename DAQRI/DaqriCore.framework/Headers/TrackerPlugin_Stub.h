//
//  TrackerPlugin_Stub.h
//  DaqriCore
//
//  Created by Dusten Sobotta on 12/6/14.
//  Copyright (c) 2014 Daqri. All rights reserved.
//

#ifndef DaqriCore_TrackerPlugin_Stub_h
#define DaqriCore_TrackerPlugin_Stub_h

#include "TrackerPlugin.h"

namespace Daqri
{

class TrackerPlugin_Stub : public TrackerPlugin
{
public:
	TrackerPlugin_Stub() = default;
	~TrackerPlugin_Stub() = default;
	
	const char* GetName() override { return "TrackerPlugin_Stub"; }
	
	bool Load() override;
	bool Start() override;
	bool StartTracking() override;
	bool StopTracking() override;
	bool Stop() override;
	bool UnLoad() override;
	
	void SetSurfaceSizeAndRotation(const int width, const int height, const Orientation orientation) override;
	
	void Update(const GLuint frameBuffer, const GLuint renderBuffer) override;
	
	Matrix44F					GetProjectionMatrix() const override;
	Viewport2D					GetViewport() const override;
	const vector<Trackable>&	GetTrackables() const override;
};


} //end namespace Daqri
#endif
